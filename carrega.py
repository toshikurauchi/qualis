import csv

def carrega(arquivo):
    '''
    Carrega os dados no arquivo csv em uma lista de dicionários
    '''
    with open(arquivo, 'r') as arquivo_csv:
        leitor = csv.DictReader(arquivo_csv, delimiter=',')
        return [entrada for entrada in leitor], leitor.fieldnames

def carrega_conferencias():
    return carrega('dados/2012-ranking.qualis.conferencias.csv')

def carrega_periodicos():
    return carrega('dados/classificacoes_publicadas_ciencia_da_computacao_2017_1496941692902.csv')

if __name__=='__main__':
    conferencias, campos = carrega_conferencias()
    for conferencia in conferencias[:5]:
        print(conferencia)
    print(campos)
    
    periodicos, campos = carrega_periodicos()
    for periodico in periodicos[:5]:
        print(periodico)
    print(campos)