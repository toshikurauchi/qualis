from datetime import datetime
import csv
import os
from collections import namedtuple


EntradaCFP = namedtuple('EntradaCFP', 'when,where,deadline,when_sort,deadline_sort')


class Cache(object):
    def __init__(self):
        dt = datetime.now()
        self.arquivo = '%d%d%dcache.csv' % (dt.year, dt.month, dt.day)
        self.cache = dict()

    def _carrega(self):
        if not os.path.isfile(self.arquivo):
            return
        with open(self.arquivo, 'r') as arquivo_csv:
            leitor = csv.reader(arquivo_csv, delimiter=',')
            self.cache = {dado[0]: EntradaCFP(*dado[1:]) for dado in leitor}
    
    def __getitem__(self, item):
        if item in self.cache:
            return self.cache[item]

        return None
    
    def __setitem__(self, item, value):
        self.cache[item] = value

    def __contains__(self, item):
        return item in self.cache

    def __enter__(self):
        self._carrega()
        return self
    
    def __exit__(self, type, value, traceback):
        with open(self.arquivo, 'w') as arquivo_csv:
            escritor = csv.writer(arquivo_csv, delimiter=',')
            for sigla in self.cache:
                escritor.writerow([sigla, *self.cache[sigla]])
