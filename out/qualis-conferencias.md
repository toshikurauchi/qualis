+++
date = "2017-06-27T16:04:29"
draft = false

tags = ["misc", "qualis"]
title = "Qualis Conferências"
math = true
summary = ""

highlight = true

+++


A tabela a seguir apresenta o Qualis de conferências de 2012. Os dados foram 
    obtidos na página do [Instituto de Computação da Unicamp](http://ic.unicamp.br/pos/qualis). As datas e locais foram extraídos
    da página [WikiCFP](http://wikicfp.com/cfp/) [última atualização em 27-06-2017].

Sigla | Nome | Índice-H | Estrato | Data | Local | Deadline | data_sort | data_limite_sort
---|---|---|---|---|---|---|---|---
3DIM | International Conference on 3-D Digital Imaging and Modeling | 29 | B1 |  |  |  | z | z
3DUI | IEEE Symposium on 3D User Interfaces | 18 | B2 |  |  |  | z | z
3PGCIC | International Conference on P2P Parallel Grid Cloud and Internet Computing | 4 | B4 | Nov 8, 2017 - Nov 10, 2017 | Barcelona, Spain | Jul 15, 2017 | 2017110820171110 | 20170715
AAAI | Conference on Artificial Intelligence | 93 | A1 | Feb 4, 2017 - Feb 9, 2017 | San Francisco, California, USA | Sep 14, 2016 (Sep 9, 2016) | 2017020420170209 | 20160914
AAECC | Applied algebra Algebraic algorithms and Error Correcting Codes | 17 | B2 |  |  |  | z | z
AAIM | Algorithmic Applications in Management | 14 | B3 |  |  |  | z | z
AAIP | Workshop on Approaches and Applications of Inductive Programming | 3 | B5 |  |  |  | z | z
AAMAS | International Joint Conference on Autonomous Agents and Multiagents Systems | 84 | A1 | May 8, 2017 - May 12, 2017 | Sao Paulo, Brazil | Nov 15, 2016 (Nov 11, 2016) | 2017050820170512 | 20161115
ABiALS | Anticipatory Behavior in Adaptive Learning Systems | 14 | B3 |  |  |  | z | z
ABS | Agent-based Simulation Workshop | 5 | B4 | Jun 26, 2017 - Jun 29, 2017 | Qingdao, Shandong, China | TBD | 2017062620170629 | z
ABZ | Abstract State Machines Alloy B and Z | 18 | B2 |  |  |  | z | z
AC | IADIS International Conference on Applied Computing | 9 | B4 | Sep 20, 2017 - Sep 22, 2017 | Buenos Aires, Argentina | Jun 30, 2017 | 2017092020170922 | 20170630
ACC | American Control Conference | 76 | A1 | May 24, 2017 - May 26, 2017 | Seattle, WA | Sep 19, 2016 | 2017052420170526 | 20160919
ACE | Australasian Conference on Computing Education | 21 | B2 | May 8, 2017 - May 9, 2017 | Singapore | Mar 10, 2017 | 2017050820170509 | 20170310
ACE_A | International Conference on Advances in Computer Entertainment Technology | 30 | B1 |  |  |  | z | z
ACHI | International Conference on Advances in Computer-Human Interaction | 11 | B3 | Mar 19, 2017 - Mar 23, 2017 | Nice, France | Dec 1, 2016 | 2017031920170323 | 20161201
ACiD | Algorithms and Complexity in Durham | 6 | B4 |  |  |  | z | z
ACII | International Conference on Affective Computing and Intelligent Interaction | 24 | B1 | Oct 23, 2017 - Oct 26, 2017 | San Antonio, TX, USA | May 2, 2017 | 2017102320171026 | 20170502
ACIIDS | Asian Conference on Intelligent Information and Database Systems | 7 | B4 | Apr 3, 2017 - Apr 5, 2017 | Kanazawa, Japan | Oct 1, 2016 | 2017040320170405 | 20161001
ACIL | Advances in Computational Intelligence and Learning | 9 | B4 |  |  |  | z | z
ACIS-ICIS | International Conference on Computer and Information Science | 15 | B3 | Oct 25, 2017 - Oct 28, 2017 | Shanghai, China | Jun 30, 2017 | 2017102520171028 | 20170630
ACIVS | Advanced Concepts for Intelligent Vision Systems Conference | 18 | B2 | Sep 18, 2017 - Sep 21, 2017 | Antwerp, Belgium | May 26, 2017 | 2017091820170921 | 20170526
ACL | International Conference of the Association of Computational Linguistics | 110 | A1 | Jul 30, 2017 - Aug 4, 2017 | Vancouver, Canada | Feb 6, 2017 | 2017073020170804 | 20170206
ACL2 | Workshop on the ACL2 Theorem Prover and Its Applications | 12 | B3 |  |  |  | z | z
ACMC | Australasian Computer Music Conference | 9 | B4 |  |  |  | z | z
ACNS | International Conference on Applied Cryptography and Network Security | 34 | B1 | Jul 10, 2017 - Jul 12, 2017 | Kanazawa, Japan | Feb 3, 2017 | 2017071020170712 | 20170203
ACOM | International Workshop on Assessment of Contemporary Modularization Techniques | 3 | B5 |  |  |  | z | z
ACRI | International Conference on Cellular Automata for Research and Industry | 20 | B2 |  |  |  | z | z
ACSAC | Annual Computer Security Applications Conference | 63 | A1 | Dec 4, 2017 - Dec 8, 2017 | San Juan, Puerto Rico, USA | Jun 1, 2017 | 2017120420171208 | 20170601
ACSD | International Conference on Application of Concurrency to System Design | 23 | B1 | Jun 26, 2017 - Jun 30, 2017 | Zaragoza, Spain | Jan 20, 2017 (Jan 13, 2017) | 2017062620170630 | 20170120
Ada-Europe | International Conference on Reliable Software Technologies | 18 | B2 | Jun 12, 2017 - Jun 16, 2017 | Vienna, Austria | Jan 22, 2017 | 2017061220170616 | 20170122
ADBIS | Symposium on Advances in Database and Information Systems | 24 | B1 | Sep 24, 2017 - Sep 27, 2017 | Nicosia, Cyprus | Apr 28, 2017 | 2017092420170927 | 20170428
ADC | Australasian Database Conference | 32 | B1 | Sep 25, 2017 - Sep 28, 2017 | Brisbane, Queensland, Australia | May 26, 2017 (May 19, 2017) | 2017092520170928 | 20170526
AdCom | International Conference on Advanced Computing and Communications | 12 | B3 |  |  |  | z | z
ADHOC-NOW | International Conference on AD-HOC Networks and Wireless | 22 | B1 | Sep 20, 2017 - Sep 22, 2017 | Messina, Italy | May 8, 2017 | 2017092020170922 | 20170508
ADHOCNETS | International Conference on Ad Hoc Networks | 2 | B5 |  |  |  | z | z
ADMA | International Conference on Advanced Data Mining and Applications | 18 | B2 | Nov 5, 2017 - Nov 6, 2017 | Singapore | Jul 10, 2017 | 2017110520171106 | 20170710
ADMI | International Workshop on Agents and Data Mining Interaction | 3 | B5 | May 8, 2017 - May 8, 2017 | Sao Paulo, Brazil | Feb 5, 2017 | 2017050820170508 | 20170205
ADPRL | IEEE International Symposium on Approximate Dynamic Programming and Reinforcement Learning | 13 | B3 |  |  |  | z | z
ADS | Agent-Directed Simulation Symposium | 3 | B5 | Apr 3, 2017 - Apr 5, 2017 | Kanazawa, Japan | Oct 1, 2016 | 2017040320170405 | 20161001
ADT | Algorithmic Decision Theory | 7 | B4 |  |  |  | z | z
AEM | International Conference on Applied and Engineering Mathematics | 6 | B4 |  |  |  | z | z
AES-Brasil | Audio Engineering Society - Brasil | 2 | B5 | Jun 22, 2017 - Jun 24, 2017 | Erlangen, Germany | Jan 22, 2017 | 2017062220170624 | 20170122
AFRICACRYPT | International Conference on Cryptology in Africa | 15 | B3 |  |  |  | z | z
AGI | Artificial General Intelligence | 9 | B4 | Aug 15, 2017 - Aug 18, 2017 | Melbourne, Australia | Apr 25, 2017 | 2017081520170818 | 20170425
Agile | Agile Development Conference | 26 | B1 | Sep 3, 2017 - Sep 7, 2017 | Prague, Czech Republic | May 10, 2017 | 2017090320170907 | 20170510
AGILE_A | AGILE Conference on Geographic Information Science | 10 | B3 |  |  |  | z | z
AGO | International Conference on Advances In Global Optimization: Methods and Applications | 2 | B5 |  |  |  | z | z
AGS | Agents for Games and Simulations | 6 | B4 | Apr 21, 2017 - Apr 21, 2017 | Paris, France | Mar 24, 2017 (Jan 14, 2017) | 2017042120170421 | 20170324
AI | Canadian Conference on Artificial Intelligence | 24 | B1 | Aug 19, 2017 - Aug 25, 2017 | Melbourne, Australia | Feb 19, 2017 | 2017081920170825 | 20170219
AI*IA | Congress of the Italian Association for Artificial Intelligence | 15 | B3 | Nov 14, 2017 - Nov 17, 2017 | Bari | Jun 4, 2017 | 2017111420171117 | 20170604
AIA | Artificial Intelligence and Applications Conference | 6 | B4 |  |  |  | z | z
AIAI | IFIP International Conference on Artificial Intelligence Applications and Innovations | 10 | B4 |  |  |  | z | z
AICCSA | ACS/IEEE International Conference on Computer Systems and Applications | 23 | B1 | Oct 30, 2017 - Nov 3, 2017 | Hammamet, Tunisia | May 30, 2017 | 2017103020171103 | 20170530
AICT | Advanced International Conference on Telecommunications | 22 | B1 | Jun 25, 2017 - Jun 29, 2017 | Venice, Italy | Feb 5, 2017 | 2017062520170629 | 20170205
AIED | International Conference on Artificial Intelligence in Education | 29 | B1 | Jun 26, 2017 - Jun 30, 2017 | Wuhan, China | Jan 31, 2017 | 2017062620170630 | 20170131
AIED_A | Workshop on Motivation and Affect in Educational Software | 3 | B5 |  |  |  | z | z
AIIDE | Artificial Intelligence and Interactive Digital Entertainment | 19 | B2 |  |  |  | z | z
AIME | Conference on Artificial Intelligence in Medicine in Europe | 19 | B2 | Jun 21, 2017 - Jun 24, 2017 | Vienna | Feb 1, 2017 (Jan 15, 2017) | 2017062120170624 | 20170201
AiML | Advances in Modal Logic | 15 | B3 |  |  |  | z | z
AIMS | Autonomous Infrastructure Management and Security | 9 | B4 | Jun 25, 2017 - Jun 30, 2017 | Honolulu, Hawaii, USA | May 2, 2017 | 2017062520170630 | 20170502
AIMSA | Artificial Intelligence: Methodology Systems Applications | 16 | B2 | Mar 8, 2017 - Mar 9, 2017 | Bangalore India | Jan 31, 2017 | 2017030820170309 | 20170131
AINA | International Conference on Advanced Information Networking and Applications | 39 | A2 |  |  |  | z | z
AIRS | Asia Information Retrieval Symposium | 14 | B3 |  |  |  | z | z
AIS | SIGED International Conference on Informatics Education and Research | 2 | B5 | Aug 26, 2017 - Aug 27, 2017 | Dubai, UAE | Jun 25, 2017 | 2017082620170827 | 20170625
AISAT | International Conference on Artificial Intelligence in Science and Technology | 2 | B5 |  |  |  | z | z
AISC | International Conference on Artificial Intelligence and Symbolic Computing | 13 | B3 | Jan 2, 2017 - Jan 3, 2017 | Zurich, Switzerland | Dec 10, 2016 | 2017010220170103 | 20161210
AISec | AI & Security Workshop | 9 | B4 |  |  |  | z | z
AISM | Australian Information Security Management Conference | 8 | B4 |  |  |  | z | z
ALAMAS | European Symposium on Adaptive Learning Agents and Multi-Agent Systems | 3 | B5 |  |  |  | z | z
ALENEX | Workshop on Algorithm Engineering and Experiments | 27 | B1 | Jan 17, 2017 - Jan 18, 2017 | Barcelona, Spain | Aug 15, 2016 (Aug 10, 2016) | 2017011720170118 | 20160815
ALG | Workshop on Algorithm Engineering | 22 | B1 |  |  |  | z | z
AlgOR | International conference on Algorithmic Operations Research and Operations Research Case Competition | 8 | B4 | Jul 24, 2017 - Jul 27, 2017 | Washington D.C., USA | Feb 9, 2017 | 2017072420170727 | 20170209
ALGOSENSORS | Algorithmic Aspects of Wireless Sensor Networks | 15 | B3 | Sep 7, 2017 - Sep 8, 2017 | Vienna, Austria | TBD | 2017090720170908 | z
ALIFE | IEEE Symposium on Artificial Life | 6 | B4 |  |  |  | z | z
ALIFE_A | International Conference on the Simulation and Synthesis of Living Systems | 19 | B2 |  |  |  | z | z
ALIO/EURO | Conference on Combinatorial Optimization | 3 | B5 | Aug 30, 2017 - Sep 1, 2017 | Spain, Santiago de Compostela | Feb 9, 2017 (Feb 2, 2017) | 2017083020170901 | 20170209
ALPIT | International Conference on Advanced Language Processing and Web Information Technology | 7 | B4 |  |  |  | z | z
ALT | International Conferences on Algorithmic Learning Theory | 24 | B1 |  |  |  | z | z
ALTA | Australasian Language Technology Association Workshop | 5 | B4 |  |  |  | z | z
AMAI | International Symposium on Artificial Intelligence and Mathematics | 15 | B3 |  |  |  | z | z
AMAS-BT | Workshop on Architectural and Microarchitectural Support for Binary Translation | 1 | B5 |  |  |  | z | z
AMAST | International Conference on Algebraic Methodology and Software Technology | 27 | B1 |  |  |  | z | z
AMCIS | Americas Conference on Information Systems | 32 | B1 |  |  |  | z | z
AMCS | International Conference on Algorithmic Mathematics and Computer Science | 2 | B5 | N/A | N/A | May 1, 2017 | z | 20170501
AMIA | AMIA Annual Symposium | 39 | A2 | Nov 4, 2017 - Nov 8, 2017 | Washington, DC | TBD | 2017110420171108 | z
AMINING | Active Mining | 6 | B4 |  |  |  | z | z
AMS | Autonome Mobile Systeme | 9 | B4 |  |  |  | z | z
AMSTA | Agent and Multi-Agent Systems: Technologies and Applications | 13 | B3 | Jun 21, 2017 - Jun 23, 2017 | Vilamoura, Algarve, Portugal | Feb 15, 2017 | 2017062120170623 | 20170215
AMTA | Conference of the Association for Machine Translation in the Americas | 4 | B4 |  |  |  | z | z
AMW | Alberto Mendelzon International Workshop on Foundations of Data Management | 5 | B4 |  |  |  | z | z
ANALCO | Workshop on Analytic Algorithms and Combinatorics | 8 | B4 |  |  |  | z | z
ANCS | Symposium On Architecture For Networking And Communications Systems | 25 | B1 | May 18, 2017 - May 19, 2017 | Beijing, China | Jan 21, 2017 (Jan 7, 2017) | 2017051820170519 | 20170121
ANDESCON | IEEE Andean Region International Conference | 4 | B4 |  |  |  | z | z
ANIREM | Workshop on Agents Norms and Institutions for Regulated Multiagent Systems | 1 | B5 |  |  |  | z | z
ANNPR | IAPR International Workshop on Artificial Neural Networks in Pattern | 8 | B4 |  |  |  | z | z
ANPPOM | Congresso da Associação Nacional de Pesquisa e Pós-Graduação em Música | 1 | B5 |  |  |  | z | z
ANSS | Annual Simulation Symposium | 28 | B1 |  |  |  | z | z
ANTS | International Symposium on Algorithmic Number Theory | 27 | B1 | Dec 17, 2017 - Dec 20, 2017 | Bhubaneswar, Odisha, India | Jul 4, 2017 | 2017121720171220 | 20170704
ANTS_A | International Workshop on Ant Colony Optimization and Swarm Intelligence | 10 | B4 |  |  |  | z | z
ANTS_C | International Symposium on Advanced Networks and Telecommunication Systems | 6 | B4 |  |  |  | z | z
AOIS | Agent-Oriented Information Systems Workshop | 8 | B3 |  |  |  | z | z
AOM | Aspect-Oriented Modeling Workshop | 19 | B2 |  |  |  | z | z
AOSD | International Conference on Aspect-Oriented Software Development | 55 | A1 |  |  |  | z | z
AOSE | Workshop on Agent-Oriented Software Engineering | 7 | B3 |  |  |  | z | z
APCIP | Asia-Pacific Conference on Information Processing | 6 | B4 |  |  |  | z | z
APEID | UNESCO-APEID International Conference | 3 | B5 |  |  |  | z | z
APGV | Symposium on Applied Perception in Graphics and Visualization | 19 | B2 |  |  |  | z | z
APLAS | Asian Symposium on Programming Languages and Systems | 28 | B1 | Nov 27, 2017 - Nov 29, 2017 | Suzhou, China | Jun 16, 2017 (Jun 13, 2017) | 2017112720171129 | 20170616
APPROX | International Workshop on Approximation Algorithms for Combinatorial Optimization Problems | 31 | B1 |  |  |  | z | z
APSCC | IEEE Asia-Pacific Services Computing Conference | 15 | B3 |  |  |  | z | z
APTC | Asia-Pacific Trusted Infrastructure Technologies Conference | 1 | B5 |  |  |  | z | z
APWEB | International Asia-Pacific Web Conference | 20 | B2 | Jul 7, 2017 - Jul 9, 2017 | Beijing | Feb 17, 2017 (Feb 10, 2017) | 2017070720170709 | 20170217
ARC | International Workshop on Applied Reconfigurable Computing | 14 | B3 | Apr 3, 2017 - Apr 7, 2017 | Delft, The Netherlands | Nov 18, 2016 | 2017040320170407 | 20161118
ARCS | Architecture of Computing Systems | 17 | B2 | Apr 3, 2017 - Apr 6, 2017 | Vienna | Oct 28, 2016 | 2017040320170406 | 20161028
ARES | International Conference on Availability: Reliability and Security | 27 | B1 | Aug 29, 2017 - Sep 1, 2017 | Reggio Calabria, Italy | Mar 31, 2017 | 2017082920170901 | 20170331
ArgMas | International Workshop on Argumentation in Multi-Agent Systems | 5 | B4 |  |  |  | z | z
ARM | Workshop on Adaptive and Reflective Middleware | 7 | B4 | Sep 11, 2017 - Sep 13, 2017 | Cambridge, UK | May 19, 2017 | 2017091120170913 | 20170519
ArtAbilitation | The ArtAbilitation Conference | 2 | B5 |  |  |  | z | z
ARVLSI | Conference on Advanced Research in VLSI | 9 | B4 |  |  |  | z | z
ASAMA | International Symposium on Agent Systems and Applications / International Symposium on Mobile Agents | 24 | B1 |  |  |  | z | z
ASAP | IEEE International Conference on Application-specific Systems: Architectures and Processors | 29 | B1 | Jul 10, 2017 - Jul 12, 2017 | Seattle, WA USA | Apr 3, 2017 | 2017071020170712 | 20170403
ASE | IEEE/ACM International Conference on Automated Software Engineering | 55 | A1 | Oct 30, 2017 - Nov 3, 2017 | Illinois, USA  | May 12, 2017 (May 5, 2017) | 2017103020171103 | 20170512
ASIACCS | ASIAN ACM Symposium on Information Computer and Communications Security | 32 | B1 | Apr 2, 2017 - Apr 6, 2017 | Abu Dhabi, UAE | Nov 1, 2016 | 2017040220170406 | 20161101
ASIACRYPT | International Conference on the Theory and Application of Cryptology and Information Security | 69 | A1 |  |  |  | z | z
ASID | International Workshop on Anti-counterfeiting Security and Identification | 6 | B4 |  |  |  | z | z
ASIS&T | Annual Conference of the American Society for Information Science and Technology | 5 | B4 |  |  |  | z | z
ASONAM | International Conference on Advances in Social Network Analysis and Mining | 10 | B4 | Jul 31, 2017 - Aug 3, 2017 | Sydney, Australia | May 1, 2017 | 2017073120170803 | 20170501
ASP-DAC | Asia and South Pacific Design Automation Conference | 48 | A2 | Jun 18, 2017 - Jun 22, 2017 | Austin, TX, USA | TBD | 2017061820170622 | z
ASPLOS | International Conference on Architectural Support for Programming Languages and Operating Systems | 72 | A1 | Apr 8, 2017 - Apr 12, 2017 | Xi'an, China | Aug 15, 2016 (Aug 8, 2016) | 2017040820170412 | 20160815
ASRU | IEEE Workshop on Automatic Speech Recognition & Understanding | 20 | B2 | Dec 16, 2017 - Dec 20, 2017 | Okinawa, Japan | Jun 29, 2017 | 2017121620171220 | 20170629
ASSETS | International ACM SIGACCESS Conference on Computers and Accessibility | 38 | A2 |  |  |  | z | z
AST | Automation of Software Test | 14 | B3 |  |  |  | z | z
ASYNC | Symposium on Asynchronous Circuits and Systems | 37 | A2 |  |  |  | z | z
ATMOS | Workshop on Algorithmic Approaches for Transportation Modeling Optimization and Systems | 8 | B4 | Sep 7, 2017 - Sep 7, 2017 | Vienna, Austria | Jul 1, 2017 | 2017090720170907 | 20170701
ATT | Workshop on Agents in Traffic and Transportation | 4 | B4 |  |  |  | z | z
ATVA | Automated Technology for Verification and Analysis | 22 | B1 | Oct 3, 2017 - Oct 6, 2017 | Pune, India | Apr 21, 2017 | 2017100320171006 | 20170421
AUTOMATA | Cellular Automata Theory and Applicatins | 6 | B4 | Jul 10, 2017 - Jul 14, 2017 | Warsaw, Poland | Feb 17, 2017 | 2017071020170714 | 20170217
AVBPA | International Conference on Audio- and Video-Based Biometric Person | 45 | A2 |  |  |  | z | z
AVI | International Working Conference on Advanced Visual Interfaces | 38 | A2 |  |  |  | z | z
AWIC | Atlantic Web Intelligence Conference | 16 | B2 |  |  |  | z | z
BAIP | International Conference on Recent Trends in Business Administration and Information Processing | 4 | B4 |  |  |  | z | z
BASYS | IEEE/IFIP International Conference on Information Technology for Balanced Automation Systems | 10 | B4 |  |  |  | z | z
BC | IFIP International Conference on Broadband Communications | 3 | B4 | Aug 16, 2017 - Aug 18, 2017 | Vancouver, BC. | Feb 16, 2017 | 2017081620170818 | 20170216
BCB | ACM International Conference on Bioinformatics and Computational Biology | 5 | B4 | Aug 20, 2017 - Aug 23, 2017 | Boston, MA | Mar 6, 2017 | 2017082020170823 | 20170306
BCS-HCI | Conference of the British Computer Society Human Computer Interaction Specialist Group | 6 | B4 | Nov 13, 2017 - Nov 17, 2017 | Glasgow, Scotland | May 12, 2017 | 2017111320171117 | 20170512
BDIM | IEEE/IFIP International Workshop on Business-Driven IT Management | 9 | B4 |  |  |  | z | z
BEA | Workshop on the Innovative Use of NLP for Building Educational Applications | 9 | B4 |  |  |  | z | z
BIBE | IEEE International Symposium on Bioinformatics and Bioengineering | 27 | B1 | Oct 23, 2017 - Oct 25, 2017 | Washington, DC | Jun 25, 2017 | 2017102320171025 | 20170625
BIBM | IEEE International Conference on Bioinformatics and Biomedicine | 9 | B4 | Nov 13, 2017 - Nov 16, 2017 | Kansan City, MO, USA | Aug 1, 2017 | 2017111320171116 | 20170801
BIBMW | IEEE International Conference on Bioinformatics and Biomedicine Workshop | 6 | B4 |  |  |  | z | z
BICA | Annual International Conference on Biologically Inspired Cognitive Architectures | 4 | B4 |  |  |  | z | z
BICS | Brain-Inspired Cognitive Systems Conference | 6 | B4 |  |  |  | z | z
BIOCOMP | International Conference on Bioinformatics and Computational Biology | 8 | B4 |  |  |  | z | z
BIODEVICES | International Conference on Biomedical Electronics and Devices | 6 | B4 | Feb 21, 2017 - Feb 23, 2017 | Porto, Portugal | Oct 20, 2016 | 2017022120170223 | 20161020
BIOINFORMATICS | International Conference on Bioinformatics Models Methods and Algorithms | 3 | B5 | Aug 30, 2017 - Sep 2, 2017 | Hawaii, USA | Jun 30, 2017 | 2017083020170902 | 20170630
BIOSIGNALS | International Conference on Bio-inspired Systems and Signal Processing | 7 | B4 | Feb 21, 2017 - Feb 23, 2017 | Porto, Portugal | TBD | 2017022120170223 | z
BIOTECHNO | International Conference on Bioinformatics Biocomputational Systems and Biotechnologies | 3 | B5 | May 21, 2017 - May 25, 2017 | Barcelona, Spain | Feb 3, 2017 | 2017052120170525 | 20170203
BIR | International Conference on Perspectives in Business Informatics Research | 4 | B4 |  |  |  | z | z
BIRTE | International Workshop on Business Intelligence for the Real Time Enterprise | 1 | B5 |  |  |  | z | z
BIS | International Conference on Business Information Systems | 20 | B2 |  |  |  | z | z
BLISS | Bio-inspired Learning and Intelligent Systems for Security | 7 | B4 |  |  |  | z | z
BMEI | International Conference on BioMedical Engineering and Informatics | 7 | B4 |  |  |  | z | z
BMVC | British Machine Vision Conference | 48 | A2 | Sep 4, 2017 - Sep 7, 2017 | London | May 2, 2017 | 2017090420170907 | 20170502
BNCOD | Workshops of the British National Conference on Databases | 18 | B2 |  |  |  | z | z
BPM | International Conference in Business Process Management | 49 | A2 | Sep 10, 2017 - Sep 15, 2017 | Barcelona, Spain | Mar 13, 2017 (Mar 6, 2017) | 2017091020170915 | 20170313
BPMDS | International Conference on Business Process Modeling Development and Support | 13 | B3 | Jun 12, 2017 - Jun 13, 2017 | Essen, Germany | Mar 1, 2017 (Feb 25, 2017) | 2017061220170613 | 20170301
BPMN | International Workshop on Business Process Modeling Notation | 3 | B5 |  |  |  | z | z
BPSC | Business Process and Services Computing | 8 | B4 |  |  |  | z | z
Broadnets | International Conference on Broadband Communications Networks and Systems | 30 | B1 |  |  |  | z | z
BSB | Brazilian Symposium on Bioinformatics | 7 | B4 |  |  |  | z | z
BTAS | IEEE International Conference on Biometrics: Theory Applications and Systems | 20 | B2 |  |  |  | z | z
BUCC | Workshop on Building and Using Comparable Corpora | 2 | B5 |  |  |  | z | z
BWSS | Brazilian Workshop on Social Simulation | 3 | B5 |  |  |  | z | z
C&T | International Conference on Communities and Technologies | 3 | B5 | Jul 25, 2017 - Jul 27, 2017 | Washington, D.C., USA | Feb 15, 2017 | 2017072520170727 | 20170215
C5 | International Conference on Creating Connecting and Collaborating through Computing | 13 | B3 |  |  |  | z | z
CADE | International Conference on Automated Deduction | 41 | A2 |  |  |  | z | z
CAGD | International Symposium of Computer Aided Geometric Design | 52 | A2 | Jun 26, 2017 - Jun 30, 2017 | Pilsen (Czech Republic) | Mar 1, 2017 | 2017062620170630 | 20170301
CAIA | Conference on Artificial Intelligence for Applications | 1 | B5 |  |  |  | z | z
CAINE | ISCA International Conference on Computer Applications in Industry and Engineering | 9 | B4 | Oct 2, 2017 - Oct 4, 2017 | San Diego | Jun 30, 2017 | 2017100220171004 | 20170630
CAIP | International Conference on Computer Analysis of Images and Patterns | 20 | B2 | Aug 22, 2017 - Aug 24, 2017 | Ystad, Sweden | Apr 20, 2017 | 2017082220170824 | 20170420
CAiSE | International Conference on Advanced Information Systems Engineering | 51 | A2 | Jun 12, 2017 - Jun 16, 2017 | Essen, Germany | Dec 4, 2016 (Nov 28, 2016) | 2017061220170616 | 20161204
CAMAD | IEEE Workshop on Computer-Aided Modeling Analysis and Design of Communication Links and Networks | 7 | B4 | Jun 19, 2017 - Jun 21, 2017 | Lund, Sweden | Apr 18, 2017 | 2017061920170621 | 20170418
CAMP | International Workshop on Computer Architectures for Machine Perception | 14 | B3 |  |  |  | z | z
CANPC | Workshop on Communication Architecture and Applications for Network-Based Parallel Computing | 6 | B4 |  |  |  | z | z
CAOS | Workshop on Computer Architecture and Operating System co-design | 1 | B5 |  |  |  | z | z
CASA | Computer Animation and Social Agents | 13 | B3 | May 22, 2017 - May 24, 2017 | Seoul, South Korea | Jan 24, 2017 | 2017052220170524 | 20170124
CASCON | Annual International Conference on Computer Science and Software Engineering | 30 | B1 | Nov 6, 2017 - Nov 8, 2017 | Markham, Ontario, Canada | Jun 26, 2017 (May 29, 2017) | 2017110620171108 | 20170626
CASES | International Conference on Compilers Architecture and Synthesis for Embedded Systems | 44 | A2 | Sep 24, 2017 - Sep 24, 2017 | Nicosia, Cyprus | Jun 10, 2017 | 2017092420170924 | 20170610
CASE_A | IEEE Conference on Automation Science and Engineering | 28 | B1 |  |  |  | z | z
CASON | International Conference on Computational Aspects of Social Networks | 5 | B4 |  |  |  | z | z
CATA | International Conference on Computers and their Applications | 11 | B3 | May 27, 2017 - May 28, 2017 | Dubai, UAE | Apr 24, 2017 | 2017052720170528 | 20170424
CATCH | Cybersecurity Applications & Technology Conference For Homeland Security | 9 | B4 |  |  |  | z | z
CATE | IASTED International Conference on Computers and Advanced Technology in Education | 9 | B4 |  |  |  | z | z
CAV | International Conference on Computer Aided Verification | 82 | A1 | Jul 22, 2017 - Jul 28, 2017 | Heidelberg, Germany | Jan 24, 2017 | 2017072220170728 | 20170124
CBA | Congresso Brasileiro de Automática | 5 | B4 |  |  |  | z | z
CBEE | Congresso Brasileiro de Educação Especial | 1 | B5 |  |  |  | z | z
CBIC | Congresso Brasileiro de Inteligência Computacional | 1 | B5 |  |  |  | z | z
CBIS | Congresso Brasileiro de Informática em Saúde | 6 | B4 |  |  |  | z | z
CBMI | International Workshop on Content-Based Multimedia Indexing | 12 | B3 | Jun 19, 2017 - Jun 21, 2017 | Florence, Italy | Feb 28, 2017 | 2017061920170621 | 20170228
CBMS | IEEE Symposium on Computer-Based Medical Systems | 26 | B1 | Jun 22, 2017 - Jun 24, 2017 | Thessaloniki, Greece | TBD | 2017062220170624 | z
CBSE | International Symposium Component-Based Software Engineering | 25 | B1 |  |  |  | z | z
CC | International Conference on Compiler Construction | 38 | A2 | Mar 22, 2017 - Mar 23, 2017 | Cambrigde_UK | Nov 30, 2016 | 2017032220170323 | 20161130
CCA | IEEE International Conference on Control Applications | 6 | B3 | Nov 28, 2017 - Dec 1, 2017 | Tallinn, Estonia | Jul 10, 2017 | 2017112820171201 | 20170710
CCA_A | International Conference on Computability and Complexity in Analysis | 13 | B3 |  |  |  | z | z
CCC | IEEE Conference on Computational Complexity | 36 | B1 | Sep 19, 2017 - Sep 22, 2017 | Cali, Colombia | Apr 21, 2017 (Feb 28, 2017) | 2017091920170922 | 20170421
CCCG | Canadian Conference on Computational Geometry | 19 | B2 | Jul 26, 2017 - Jul 28, 2017 | Ottawa, Ontario, Canada | May 1, 2017 | 2017072620170728 | 20170501
CCCT | International Conference on Computer Communication and Control Technologies | 5 | B4 |  |  |  | z | z
CCC_A | Colombian Computing Congress | 2 | B5 |  |  |  | z | z
CCGrid | IEEE/ACM International Symposium on Cluster Computing and the Grid | 60 | A1 | May 14, 2017 - May 17, 2017 | Madrid, Spain | Nov 16, 2016 | 2017051420170517 | 20161116
CCNC | IEEE Consumer Communications and Networking Conference | 31 | B1 | Jan 8, 2017 - Jan 11, 2017 | Las Vegas | Jul 24, 2016 | 2017010820170111 | 20160724
CCO | International Conference on Cooperative Control and Optimization | 6 | B4 |  |  |  | z | z
CCRTS | International Command and Control Research and Technology Symposium | 6 | B4 |  |  |  | z | z
CCS | ACM Conference on Computer and Communications Security | 100 | A1 | Oct 30, 2017 - Nov 3, 2017 | Dallas, USA | May 19, 2017 | 2017103020171103 | 20170519
CCW | IEEE Annual Computer Communications Workshop | 5 | B3 |  |  |  | z | z
CC_A | Creativity and Cognition Conference | 2 | B5 |  |  |  | z | z
CDC | IEEE Conference on Decision and Control | 30 | B1 | Dec 15, 2017 - Dec 17, 2017 | Melbourne | Mar 20, 2017 | 2017121520171217 | 20170320
CDVE | Cooperative Design Visualization and Engineering | 10 | B4 |  |  |  | z | z
CEA | International Conference on Computer Engineering and Applications | 8 | B4 | N/A | N/A | Jun 25, 2017 | z | 20170625
CEAS | International Conference on Email and Anti-Spam | 36 | B1 |  |  |  | z | z
CEC | IEEE Conference on Commerce and Enterprise Computing | 27 | B1 | Jun 5, 2017 - Jun 8, 2017 | Donostia – San Sebastián, Spain | Jan 30, 2017 | 2017060520170608 | 20170130
CEC_A | IEEE Congress on Evolutionary Computation | 34 | A2 |  |  |  | z | z
CELDA | Cognition and Exploratory Learning in Digital Age | 10 | B4 |  |  |  | z | z
CENTERIS | International Conference on Enterprise Information Systems | 3 | B3 |  |  |  | z | z
CERMA | Robotics and Automotive Mechanics Conference Electronics | 9 | B4 |  |  |  | z | z
CETS | Conference on E-business Technology and Strategy | 2 | B5 |  |  |  | z | z
CF | ACM International Conference on Computing Frontiers | 27 | B1 | May 15, 2017 - May 17, 2017 | Siena, Italy | Jan 20, 2017 | 2017051520170517 | 20170120
CGI | Computer Graphics International Conference | 31 | B1 | Jun 27, 2017 - Jun 30, 2017 | Yokohama, Japan | Feb 20, 2017 (Feb 13, 2017) | 2017062720170630 | 20170220
CGIM | International Conference on Computer Graphics and Imaging | 9 | B4 |  |  |  | z | z
CGIV | International Conference on Computer Graphics Imaging and Visualization | 15 | B3 |  |  |  | z | z
CGO | IEEE/ACM International Symposium on Code Generation and Optimization | 41 | A2 | Feb 4, 2017 - Feb 8, 2017 | Austin, TX | Sep 9, 2016 (Sep 2, 2016) | 2017020420170208 | 20160909
CHARME | Conference on Correct Hardware Design and Verification Methods | 25 | B1 |  |  |  | z | z
CHES | Workshop on Cryptographic Hardware and Embedded Systems | 65 | A1 |  |  |  | z | z
CHI | Conference on Human Factors in Computing Systems | 147 | A1 | May 6, 2017 - May 11, 2017 | Colorado Convention Center, Denver, US | Sep 21, 2016 (Sep 14, 2016) | 2017050620170511 | 20160921
CIA | International Workshop on Cooperative Information Agents | 24 | B1 |  |  |  | z | z
CIAA | International Conference on Implementation and Application of Automata | 15 | B3 | Jun 27, 2017 - Jun 30, 2017 | Marne-la-Vallée (Fr | Mar 1, 2017 | 2017062720170630 | 20170301
CIAED | Congresso Internacional ABED de Educação a Distância | 5 | B4 |  |  |  | z | z
CIAIE | Congresso Ibero-Americano de Informática Educativa | 4 | B4 |  |  |  | z | z
CIARP | Iberoamerican Congress on Pattern Recognition | 16 | B2 | Nov 7, 2017 - Nov 10, 2017 | Valparaiso, Chile | May 21, 2017 | 2017110720171110 | 20170521
CIBCB | IEEE Symposium on Computational Intelligence in Bioinformatics and Computational Biology | 15 | B3 |  |  |  | z | z
CIBSE | Ibero-American Conference on Software Engineering | 6 | B4 | May 22, 2017 - May 23, 2017 | Buenos Aires, Argentina | Jan 23, 2017 (Jan 9, 2017) | 2017052220170523 | 20170123
CIC | International Conference on Communications in Computing | 11 | B3 | Sep 11, 2017 - Sep 15, 2017 | Lillehammer, Norway | Mar 1, 2017 | 2017091120170915 | 20170301
CICC | IEEE Custom Integrated Circuits Conference | 11 | B2 |  |  |  | z | z
CICLING | Conference on Intelligent Text Processing and Computational Linguistics | 29 | B1 | Apr 17, 2017 - Apr 23, 2017 | Budapest, Hungary | Feb 1, 2017 (Jan 25, 2017) | 2017041720170423 | 20170201
CIDM | IEEE Symposium on Computational Intelligence and Data Mining | 12 | B3 | Nov 27, 2017 - Dec 1, 2017 | Honolulu, Hawaii, USA | Jul 16, 2017 | 2017112720171201 | 20170716
CIDR | Biennial Conference on Innovative Data Systems Research | 41 | A2 |  |  |  | z | z
CiE | Computability in Europe Conference | 16 | B2 |  |  |  | z | z
CIG | IEEE Symposium on Computacional Intelligence and Games | 24 | B1 | Feb 4, 2017 - Aug 25, 2017 | New York, USA | Aug 7, 2017 | 2017020420170825 | 20170807
CIKM | International Conference on Information and Knowledge Management | 85 | A1 | Nov 6, 2017 - Nov 10, 2017 | SINGAPORE | May 23, 2017 (May 16, 2017) | 2017110620171110 | 20170523
CIM | Conference on Interdisciplinary Musicology | 9 | B4 | Sep 18, 2017 - Sep 20, 2017 | Lund, Sweden | Jun 10, 2017 | 2017091820170920 | 20170610
CIMCA | International Conference on Computational Intelligence for Modelling Control and Automation | 16 | B2 |  |  |  | z | z
CIMSA | IEEE International Conference on Computational Intelligence for Measurement Systems and Applications | 4 | B4 |  |  |  | z | z
CIMSIM | International Conference on Computational Intelligence Modelling and Simulation | 5 | B4 |  |  |  | z | z
CINTED | Ciclo de Palestras Novas Tecnologias na Educação | 1 | B5 |  |  |  | z | z
CIRA | IEEE International Symposium on Computational Intelligence in Robotics and Automation | 22 | B1 |  |  |  | z | z
CIRAS | International Conference on Computational Intelligence Robotics and Autonomous Systems | 4 | B4 |  |  |  | z | z
CIS | International Conference on Computational Intelligence and Security | 12 | B3 | Feb 16, 2017 - Feb 18, 2017 | Dallas, Texas | Feb 13, 2017 (Nov 5, 2016) | 2017021620170218 | 20170213
CISched | IEEE Symposium on Computational Intelligence in Scheduling | 9 | B4 |  |  |  | z | z
CISIS | International Conference on Complex Intelligent and Software Intensive Systems | 15 | B3 | Jul 10, 2017 - Jul 12, 2017 | Turin, Italy | Mar 15, 2017 | 2017071020170712 | 20170315
CISST | International Conference Image Science: Systems and Technology | 8 | B4 |  |  |  | z | z
CISTI | Iberian Conference on Information Systems and Technologies | 5 | B4 | Jun 14, 2017 - Jun 17, 2017 | Lisbon, Portugal | Feb 12, 2017 | 2017061420170617 | 20170212
CISW | International Conference on Computational Intelligence and Security Workshops | 9 | B4 |  |  |  | z | z
CISWSN | Collective Intelligence in Semantic Web and Social Networks | 7 | B4 |  |  |  | z | z
CIT | International Conference on Computer and Information Technology | 22 | B1 | Aug 21, 2017 - Aug 23, 2017 | Helsinki, Finland | Apr 15, 2017 | 2017082120170823 | 20170415
CITA | International Conference on IT in Asia | 2 | B5 |  |  |  | z | z
CKC | Social and Collaborative Construction of Structured Knowledge | 9 | B4 |  |  |  | z | z
CL | Corpus Linguistics Conference | 17 | B2 | Jul 30, 2017 - Aug 4, 2017 | Vancouver, Canada | Feb 6, 2017 | 2017073020170804 | 20170206
CLA | International Conference On Concept Lattices and Their Applications | 10 | B4 |  |  |  | z | z
CLAIO | Congreso Latino-Iberoamericano de Investigación Operativa | 4 | B4 |  |  |  | z | z
CLEF | Cross Language Evaluation Forum Workshop | 16 | B2 | Sep 11, 2017 - Sep 14, 2017 | Dublin, Ireland | Apr 28, 2017 | 2017091120170914 | 20170428
CLEI | Conferencia Latinoamericana de Informática | 8 | B4 | Sep 4, 2017 - Sep 8, 2017 | Córdoba, Argentina | Apr 14, 2017 | 2017090420170908 | 20170414
CLIHC | Latin American Conference on Human-Computer Interaction | 3 | B5 | Nov 8, 2017 - Nov 10, 2017 | Antigua Guatemala, Guatemala | Jun 30, 2017 | 2017110820171110 | 20170630
CLIMA | International Workshop on Computational Logic in Multi-Agent Systems | 8 | B4 |  |  |  | z | z
CLIP | Workshop on Cross-Language Information Processing | 1 | B5 | Sep 17, 2017 - Sep 17, 2017 | Quebec City | Jun 18, 2017 | 2017091720170917 | 20170618
CLOSER | International Conference on Cloud Computing and Services Science | 4 | B4 | Apr 24, 2017 - Apr 26, 2017 | Porto, Portugal | Nov 21, 2016 | 2017042420170426 | 20161121
CLOUD | IEEE International Conference on Cloud Computing | 19 | B2 | Sep 17, 2017 - Sep 19, 2017 | London, UK | Jul 25, 2017 | 2017091720170919 | 20170725
CLOUDCOM | IEEE International Conference on Cloud Computing Technology and Science | 19 | B2 | Dec 11, 2017 - Dec 14, 2017 | Hong Kong | Jun 30, 2017 | 2017121120171214 | 20170630
CLUSTER | Cluster Computing Conference | 39 | A2 | May 14, 2017 - May 17, 2017 | Madrid, Spain | Nov 16, 2016 | 2017051420170517 | 20161116
CMC | International Conference on Communications and Mobile Computing | 9 | B4 |  |  |  | z | z
CMMR | International Symposium on Computer Music Modeling and Retrieval | 2 | B5 | Sep 25, 2017 - Sep 28, 2017 | Porto & Matosinhos, Portugal | Apr 2, 2017 | 2017092520170928 | 20170402
CMV | International Conference on Coordinated and Multiple Views in Exploratory Visualization | 17 | B2 |  |  |  | z | z
CNMAC | Congresso Nacional de Matemática Aplicada e Computacional | 10 | B4 |  |  |  | z | z
CNSM | International Conference on Network and Services Management | 6 | B4 | Nov 26, 2017 - Nov 30, 2017 | Waseda University, Tokyo, Japan | Jun 12, 2017 | 2017112620171130 | 20170612
CNSR | Annual Conference on Communication Networks and Services Research | 17 | B2 |  |  |  | z | z
CoALa | Workshop on Contract Architectures and Languages | 3 | B5 |  |  |  | z | z
COCOA | Conference on Combinatorial Optimization and Applications | 10 | B4 | Dec 16, 2017 - Dec 18, 2017 | Shanghai, China | Jul 15, 2017 (Jul 10, 2017) | 2017121620171218 | 20170715
COCOON | International Computing and Combinatorics Conference | 28 | B1 | Aug 3, 2017 - Aug 5, 2017 | Hong Kong, China | Mar 20, 2017 | 2017080320170805 | 20170320
CODAS | International Symposium on Cooperative Database Systems for Advanced Applications | 12 | B3 |  |  |  | z | z
CODASPY | Data and Application Security and Privacy | 6 | B4 | Mar 22, 2017 - Mar 24, 2017 | Scottsdale, Arizona, USA | Sep 15, 2016 (Sep 8, 2016) | 2017032220170324 | 20160915
CODES+ISSS | International Conference on Hardware/Software Codesign and System Synthesis | 39 | A2 | Mar 15, 2017 - Dec 15, 2017 | CANADA | May 15, 2017 | 2017031520171215 | 20170515
CogSci | Annual Conference of the Cognitive Science Society | 19 | B2 |  |  |  | z | z
COIN | Workshop on Coordination Organization Institutions and Norms in Agent Systems | 6 | B4 | May 8, 2017 - May 9, 2017 | São Paulo, Brazil | Feb 17, 2017 | 2017050820170509 | 20170217
COLING | International Conference on Computational Linguistics | 62 | A1 |  |  |  | z | z
CollaborateCom | International Conference on Collaborative Computing: Networking Applications and Worksharing | 13 | B3 | Nov 11, 2017 - Nov 12, 2017 | GUANGZHOU, PEOPLE'S REPUBLIC OF CHINA | Jul 15, 2017 | 2017111120171112 | 20170715
CollECTeR | Collaborative Electronic Commerce Technology and Research | 10 | B4 |  |  |  | z | z
COLT | Annual Conference on Computational Learning Theory | 46 | A2 | Jul 7, 2017 - Jul 10, 2017 | Amsterdam, Netherlands | Feb 17, 2017 | 2017070720170710 | 20170217
COMMONSENSE | Symposium on Logical Formalizations of Commonsense Reasoning | 10 | B4 |  |  |  | z | z
CompLife | International Symposium on Computational Life Science | 10 | B4 |  |  |  | z | z
COMPSAC | Annual International Computer Software and Applications Conference | 40 | A2 | Jul 4, 2017 - Jul 8, 2017 | Torino, Italy | Jan 13, 2017 | 2017070420170708 | 20170113
CompSysTech | International Conference on Computer Systems and Technologies | 14 | B3 |  |  |  | z | z
COMS | Conference on Optimization Methods and Software | 39 | A2 |  |  |  | z | z
COMSWARE | ICST International Conference on Communication System software and Middleware | 18 | B2 |  |  |  | z | z
CONCUR | International Conference on Concurrency Theory | 50 | A2 | Sep 5, 2017 - Sep 8, 2017 | Berlin | Apr 21, 2017 | 2017090520170908 | 20170421
CONEXT | ACM International Conference on Emerging Networking Experiments and Technologies | 34 | B1 | Dec 12, 2017 - Dec 15, 2017 | Seoul, Korea | Jun 19, 2017 (Jun 12, 2017) | 2017121220171215 | 20170619
Conf-IRM | International Conference on Information Resources Management | 7 | B4 |  |  |  | z | z
CONFENIS | IFIP International Conference on Research and Practical Issues of Enterprise Information Systems | 7 | B4 |  |  |  | z | z
CONGRESS | World Congress on Privacy Security Trust and the Management of e-Business | 1 | B5 | Nov 7, 2017 - Nov 10, 2017 | Valparaiso, Chile | May 21, 2017 | 2017110720171110 | 20170521
CoNLL | Conference on Natural Language Learning | 22 | B1 | Aug 3, 2017 - Aug 4, 2017 | Vancouver, Canada | Apr 23, 2017 | 2017080320170804 | 20170423
CONQUEST | Conquest International Conference on Quality Engineering in Software Technology | 2 | B5 |  |  |  | z | z
CONTECSI | International Conference on Information Systems and Technology Management | 4 | B4 |  |  |  | z | z
COOLCHIPS | IEEE Cool Chips | 3 | B5 |  |  |  | z | z
COOP | International Conference on the Design of Cooperative Systems | 11 | B3 |  |  |  | z | z
CoopIS | International Conference on Cooperative Information Systems | 22 | B1 |  |  |  | z | z
COSIT | Conference on Spatial Information Theory | 29 | B1 | Mar 25, 2017 - Mar 26, 2017 | Geneva, Switzerland | Mar 8, 2017 | 2017032520170326 | 20170308
CP | International Conference on Principles and Practice of Constraint Programming | 48 | A2 | Aug 28, 2017 - Sep 1, 2017 | Melbourne, Australia | Apr 30, 2017 (Apr 27, 2017) | 2017082820170901 | 20170430
CPAIOR | International Conference o Integration of Artificial  Intelligence and Operations Research | 21 | B2 | Jun 5, 2017 - Jun 8, 2017 | Padova, Italy | Nov 21, 2016 | 2017060520170608 | 20161121
CPM | Annual Symposium on Combinatorial Pattern Matching | 33 | B1 | Jul 4, 2017 - Jul 6, 2017 | Warsaw, Poland | Feb 7, 2017 | 2017070420170706 | 20170207
CRIWG | International Workshop on Groupware | 25 | B1 | Aug 8, 2017 - Aug 11, 2017 | Saskatoon, SK, Canada | Mar 20, 2017 | 2017080820170811 | 20170320
CROWNCOM | International Conference on Cognitive Radio Oriented Wireless Networks and Communications | 24 | B1 |  |  |  | z | z
CRYPTO | Annual International Cryptology Conference | 92 | A1 |  |  |  | z | z
CSB | IEEE Computational Systems Bioinformatics Conference | 34 | B1 |  |  |  | z | z
CSC | International Conference on Scientific Computing | 5 | B4 |  |  |  | z | z
CSCL | International Conference on Computer-Supported Collaborative Learning | 35 | B1 |  |  |  | z | z
CSCLP | ERCIM Annual Workshop on Constraint Solving and Contraint Logic Programming | 5 | B4 |  |  |  | z | z
CSCW | ACM Conference on Computer Supported Cooperative Work | 78 | A1 | Feb 25, 2017 - Mar 1, 2017 | Portland, Oregon | May 27, 2016 | 2017022520170301 | 20160527
CSCWD | International Conference on Computer Supported Cooperative Work in Design | 18 | B2 | Apr 26, 2017 - Apr 28, 2017 | Wellington, New Zealand | Nov 15, 2016 | 2017042620170428 | 20161115
CSD | Workshop on Critical Systems Development | 5 | B4 |  |  |  | z | z
CSE | Conference on Computational Science & Engineering | 8 | B4 | N/A | N/A | Jul 15, 2017 | z | 20170715
CSEDU | Internation Conference on Computer Supported Education | 6 | B4 | Apr 21, 2017 - Apr 23, 2017 | Porto, Portugal | Nov 28, 2016 | 2017042120170423 | 20161128
CSEE | International Conference on Computer Science Environment Ecoinformatics and Education | 2 | B5 | Nov 7, 2017 - Nov 9, 2017 | Savannah, GA USA | Jun 23, 2017 (Jun 16, 2017) | 2017110720171109 | 20170623
CSEE&T | Conference on Software Engineering Education and Training | 19 | B2 | Nov 7, 2017 - Nov 9, 2017 | Savannah, GA USA | Jun 23, 2017 (Jun 16, 2017) | 2017110720171109 | 20170623
CSFW | IEEE Computer Security Foundations Workshop | 57 | A1 |  |  |  | z | z
CSL | Annual Conference on Computer Science Logic | 33 | B1 |  |  |  | z | z
CSMR | European Conference on Software Maintenance and Reengineering | 40 | A2 |  |  |  | z | z
CSNDSP | International Symposium on Communication Systems Networks and Digital Signal Processing | 11 | B3 |  |  |  | z | z
CSO | International Joint Conference on Computational Sciences and Optimization | 3 | B5 |  |  |  | z | z
CSREA_EEE | International Conference on E-Learning E-Business Enterprise Information Systems & E-Government | 5 | B4 |  |  |  | z | z
CSSW | Conference on Social Semantic Web | 5 | B4 |  |  |  | z | z
CTAC | Computational Techniques and Applications Conference | 2 | B4 |  |  |  | z | z
CTC | Workshop Cybercrime and Trustworthy Computing | 2 | B5 |  |  |  | z | z
CTW | Cologne-Twente Workshop on graphs and combinatorial optimization | 8 | B4 |  |  |  | z | z
CVPR | IEEE Conference on Computer Vision and Pattern Recognition | 167 | A1 | Jun 24, 2017 - Jun 30, 2017 | Puerto Rico | Nov 11, 2016 | 2017062420170630 | 20161111
CyberGames | International Conference on Games Research and Development | 2 | B5 |  |  |  | z | z
DAARC | Discourse Anaphora and Anaphor Resolution Colloquium | 10 | B4 |  |  |  | z | z
DAC | Design Automation Conference | 102 | A1 | Jun 18, 2017 - Jun 22, 2017 | Austin, TX, USA | TBD | 2017061820170622 | z
DAFX | International Conference on Digital Audio Effects | 21 | B2 | Sep 5, 2017 - Sep 9, 2017 | Edinburgh, UK | Apr 9, 2017 | 2017090520170909 | 20170409
DAIS | IFIP International Conference on Distributed Applications and Interoperable Systems | 18 | B2 |  |  |  | z | z
DALT | International Workshop on Declarative Agent Languages and Technologies | 10 | B4 |  |  |  | z | z
DARS | International Symposium on Distributed Autonomous Robotic Systems | 25 | B1 |  |  |  | z | z
DAS | International Workshop on Document Analysis Systems | 22 | B1 | Sep 24, 2017 - Sep 24, 2017 | Nicosia, Cyprus | Jun 10, 2017 | 2017092420170924 | 20170610
DASC | IEEE International Symposium on Dependable Autonomic and Secure Computing | 13 | B3 | Nov 6, 2017 - Nov 10, 2017 | Orlando - Florida USA | Apr 30, 2017 | 2017110620171110 | 20170430
DASD | Design Analysis and Simulation of Distributed Systems Symposium | 4 | B4 |  |  |  | z | z
DASFAA | International Conference on Database Systems for Advanced Applications | 29 | B1 | Mar 27, 2017 - Mar 30, 2017 | Suzhou | Oct 31, 2016 | 2017032720170330 | 20161031
DATE | Design Automation and Test in Europe Conference and Exhibition | 79 | A1 | Mar 27, 2017 - Mar 31, 2017 | Lausanne, Switzerland | Sep 11, 2016 | 2017032720170331 | 20160911
DaWaK | International Conference on Data Warehousing and Knowledge Discovery | 29 | B1 | Aug 28, 2017 - Aug 31, 2017 | Lyon, France | Mar 27, 2017 (Mar 20, 2017) | 2017082820170831 | 20170327
DB&IS | International Baltic Conference on Databases and Information Systems | 1 | B5 |  |  |  | z | z
DBISP2P | International Workshop on Databases Information Systems and Peer-to-Peer Computing | 6 | B4 |  |  |  | z | z
DBKDA | International Conference on Advances in Databases Knowledge and Data Applications | 5 | B4 | May 21, 2017 - May 25, 2017 | Barcelona, Spain | Feb 3, 2017 | 2017052120170525 | 20170203
DBPL | International Symposium on Database Programming | 17 | B2 | Sep 1, 2017 - Sep 1, 2017 | Munich, Germany | May 12, 2017 (May 5, 2017) | 2017090120170901 | 20170512
DBTA | International Workshop on Database Technology and Applications | 5 | B4 |  |  |  | z | z
DCAI | International Conference on Distributed Computing and Artificial Intelligence | 6 | B4 | Jun 21, 2017 - Jun 23, 2017 | Porto, Portugal | Feb 6, 2017 | 2017062120170623 | 20170206
DCC | Data Compression Conference | 45 | A2 | May 1, 2017 - May 1, 2017 | Atlanta, GA | Jan 10, 2017 | 2017050120170501 | 20170110
DCCS | Dependable Computing and Communications Symposium | 1 | B5 |  |  |  | z | z
DCW | Workshop on Distributed Communities on the Web | 11 | B3 |  |  |  | z | z
DDECS | IEEE Workshop on Design and Diagnostics of Electronic Circuits and Systems | 11 | B3 |  |  |  | z | z
DEBS | International Conference on Distributed Event-Based Systems | 21 | B2 | Jun 19, 2017 - Jun 23, 2017 | Barcelona, Spain, Europe | Mar 7, 2017 (Mar 4, 2017) | 2017061920170623 | 20170307
DeLFI | Fachtagung "e-Learning" der Gesellschaft für Informatik | 10 | B4 |  |  |  | z | z
DENVECT | IEEE International Workshop on Digital Entertainment Networked Virtual Environments and Creative Technology | 1 | B5 |  |  |  | z | z
DEON | International Workshop on Deontic Logic in Computer Science | 14 | B3 |  |  |  | z | z
DepCoS-RELCOMEX | International Conference on Dependability of Computer Systems | 12 | B3 |  |  |  | z | z
DEPEND | International Conference on Dependability | 7 | B4 | Sep 10, 2017 - Sep 14, 2017 | Rome, Italy | May 25, 2017 | 2017091020170914 | 20170525
DesignCon | Design Conference | 13 | B3 |  |  |  | z | z
DEXA | International Conference on Database and Expert Systems Applications | 34 | B1 | Aug 28, 2017 - Aug 31, 2017 | Lyon, France | Mar 18, 2017 (Mar 11, 2017) | 2017082820170831 | 20170318
DFT | IEEE International Symposium on Defect and Fault Tolerance in VLSI Systems | 30 | B1 |  |  |  | z | z
DGCI | Digital Geometry for Computer Imagery | 20 | B2 |  |  |  | z | z
DGO | International Conference on Digital Government Research | 23 | B1 |  |  |  | z | z
DICTAP | International Conference on Digital Information and Communication Technology and Its Applications | 3 | B5 | Jun 29, 2017 - Jul 1, 2017 | Faculty of Management, Comenius Universi | May 29, 2017 | 2017062920170701 | 20170529
DIGITEL | IEEE International Conference on Digital Game and Intelligent Toy Enhanced Learning | 11 | B3 |  |  |  | z | z
DIGRA | Digital Games Research Conference | 17 | B2 | Jul 2, 2017 - Jul 6, 2017 | Melbourne, Australia | Mar 5, 2017 | 2017070220170706 | 20170305
DIMVA | GI International Conference on Detection of Intrusions and Malware and Vulnerability Asessment | 24 | B1 | Jul 6, 2017 - Jul 7, 2017 | Bonn, Germany | Feb 27, 2017 | 2017070620170707 | 20170227
DIPES | IFIP Working Conference on Distributed and Parallel Embedded Systems | 12 | B3 |  |  |  | z | z
DIS | Designing Interative Systems Conference | 49 | A2 | Jun 10, 2017 - Jun 14, 2017 | EdinBurgh | Jan 10, 2017 | 2017061020170614 | 20170110
DISC | International Symposium on Distributed Computing | 40 | A2 | Oct 16, 2017 - Oct 19, 2017 | Vienna, Austria | May 8, 2017 (May 1, 2017) | 2017101620171019 | 20170508
DIWeb | International Workshop on Data Integration over the Web | 8 | B4 |  |  |  | z | z
DL | International Workshop on Description Logics | 36 | B1 | N/A | N/A | Jul 1, 2017 | z | 20170701
DLT | International Conference on Developments in Language Theory | 19 | B2 |  |  |  | z | z
DMDW | Design and Management of Data Warehouses | 16 | B2 |  |  |  | z | z
DMO | Data Mining and Optimization | 5 | B4 |  |  |  | z | z
DMTCS | International Conference on Discrete Mathematics and Theoretical Computer Science | 21 | B2 |  |  |  | z | z
DNA | International Meeting on DNA Computing | 10 | B4 | Sep 24, 2017 - Sep 28, 2017 | Austin, TX, United States | Apr 1, 2017 | 2017092420170928 | 20170401
DNIS | Databases in Networked Information Systems | 11 | B3 |  |  |  | z | z
DOA | International Symposium on Distributed Objects and Applications | 19 | B2 |  |  |  | z | z
DocEng | ACM Symposium on Document Engineering | 32 | B1 | Sep 4, 2017 - Sep 7, 2017 | Valletta, Malta | Apr 3, 2017 (Mar 27, 2017) | 2017090420170907 | 20170403
DOLAP | International Workshop on Data Warehousing and OLAP | 31 | B1 |  |  |  | z | z
DRCN | International Workshop on Design of Reliable Communications Networks | 20 | B2 |  |  |  | z | z
DRM | ACM Workshop on Digital Rights Management | 22 | B1 | N/A | N/A | Sep 30, 2017 | z | 20170930
DS | International Conference on Discovery Science | 25 | B1 | Oct 15, 2017 - Oct 17, 2017 | Kyoto, Japan | Jun 6, 2017 | 2017101520171017 | 20170606
DS-RT | IEEE International Symposium on Distributed Simulation and Real Time Applications | 20 | B2 | Oct 15, 2017 - Oct 17, 2017 | Kyoto, Japan | Jun 6, 2017 | 2017101520171017 | 20170606
DSD | EUROMICRO Conference on Digital System Design | 27 | B1 | Aug 30, 2017 - Sep 1, 2017 | Vienna, Austria | Apr 15, 2017 | 2017083020170901 | 20170415
DSDE | International Conference on Data Storage and Data Engineering | 4 | B4 |  |  |  | z | z
DSL | Domain-Specific Languages for Software Engineering Conference | 5 | B4 |  |  |  | z | z
DSM | Workshop on Domain-Specific Modeling | 12 | B3 | Sep 11, 2017 - Sep 13, 2017 | Aalto University, Finland | Apr 21, 2017 | 2017091120170913 | 20170421
DSN | International Conference on Dependable Systems and Networks | 65 | A1 | Jun 26, 2017 - Jun 29, 2017 | Denver,CO,USA | Dec 5, 2016 (Nov 28, 2016) | 2017062620170629 | 20161205
DSOM | IFIP/IEEE International Workshop on Distributed Systems: Operations and Management | 24 | B1 |  |  |  | z | z
DSP | International Conference on Digital Signal Processing | 37 | A2 |  |  |  | z | z
DSS | IFIP International Conference on Decision Support Systems | 4 | B4 | Jun 21, 2017 - Jun 23, 2017 | Vilamoura, Algarve, Portugal | Feb 15, 2017 | 2017062120170623 | 20170215
DSSNS | IEEE Workshop on Dependability and Security in Sensor Networks and Systems | 7 | B4 |  |  |  | z | z
DUX | ACM Designing for User Experience Conference | 15 | B3 |  |  |  | z | z
DYSPAN | IEEE Symposioum on New Frontiers in Dynamic Spectrum Access Networks | 8 | B2 |  |  |  | z | z
E-ACTIVITIES | International Conference on E-Activities | 4 | B4 | N/A | N/A | Jul 30, 2017 | z | 20170730
e-Learning | IADIS International Conference on e-Learning | 7 | B4 | N/A | N/A | Jun 1, 2107 | z | 21070601
E-SCIENCE | Brazilian e-Science Workshop | 2 | B5 | Oct 11, 2017 - Oct 13, 2017 | UPHSD, Las Piñas, Manila, Philippines | Sep 11, 2017 | 2017101120171013 | 20170911
E2EMON | IEEE/IFIP Workshop on End-to-End Monitoring Techniques and Services | 12 | B3 |  |  |  | z | z
E4MAS | International Workshop on Environments for Multiagent Systems | 2 | B4 |  |  |  | z | z
EA | Workshop on Early Aspects | 11 | B3 | N/A | N/A | Sep 26, 2016 | z | 20160926
EACL | Conference of the European Chapter of the Association for Computational Linguistics | 37 | A2 | Apr 3, 2017 - Apr 7, 2017 | Valencia | Nov 23, 2016 | 2017040320170407 | 20161123
EAMT | Annual Conference of the European Association for Machine Translation | 8 | B3 | Aug 25, 2017 - Aug 27, 2017 | Busan, Korea | Jul 25, 2017 | 2017082520170827 | 20170725
EANN | International Conference on Engineering Applications of Neural Networks | 4 | B4 | Aug 25, 2017 - Aug 27, 2017 | Athens, Greece | Apr 20, 2017 | 2017082520170827 | 20170420
EASE | International Conference on Evaluation and Assessment in Software Engineering | 9 | B4 | Jun 15, 2017 - Jun 16, 2017 | Sweden | Jan 29, 2017 (Jan 22, 2017) | 2017061520170616 | 20170129
EATIS | Euro American Conference on Telematics and Information Systems | 9 | B4 |  |  |  | z | z
EC | ACM Conference on Electronic Commerce | 58 | A1 | Sep 12, 2017 - Sep 15, 2017 | Tallinn, Estonia | Apr 10, 2017 (Apr 3, 2017) | 2017091220170915 | 20170410
EC-TEL | European Conference on Technology Enhanced Learning | 19 | B2 | Sep 12, 2017 - Sep 15, 2017 | Tallinn, Estonia | Apr 10, 2017 (Apr 3, 2017) | 2017091220170915 | 20170410
ECAI | European Conference on Artificial Intelligence | 49 | A2 |  |  |  | z | z
ECAL | European Conference on Artificial Life | 27 | B1 | Sep 4, 2017 - Sep 8, 2017 | Lyon, France | Apr 9, 2017 | 2017090420170908 | 20170409
ECBS | Annual IEEE International Conference and Workshop on the Engineering of Computer Based Systems | 29 | B1 | Aug 31, 2017 - Sep 1, 2017 | Larnaca, Cyprus | Jun 1, 2017 | 2017083120170901 | 20170601
ECC | European Control Conference | 27 | B1 | Dec 18, 2017 - Dec 20, 2017 | Alexandria | Jul 15, 2017 | 2017121820171220 | 20170715
ECCB | European Conference on Computational Biology | 34 | B1 |  |  |  | z | z
ECCO | Conference of European Chapter on Combinatorial Optimization | 3 | B5 |  |  |  | z | z
ECCTD | European Conference on Circuit Theory and Design | 12 | B3 |  |  |  | z | z
ECCV | European Conference on Computer Vision | 95 | A1 |  |  |  | z | z
ECIR | European Conference on Information Retrieval | 39 | A2 | Apr 8, 2017 - Apr 13, 2017 | Aberdeen, Scotland, UK | Oct 15, 2016 | 2017040820170413 | 20161015
ECIS | European Conference on Information Systems | 33 | B1 | Jun 6, 2017 - Jun 6, 2017 | Gimarães, Portugal | Apr 3, 2017 | 2017060620170606 | 20170403
ECKM | European Conference on Knowledge Management | 7 | B4 | Sep 7, 2017 - Sep 8, 2017 | Barcelona, Spain | Feb 16, 2017 | 2017090720170908 | 20170216
ECMDA | European Conference on Model Driven Architecture Foundations and Applications | 25 | B1 |  |  |  | z | z
ECML | European Conference on Machine Learning | 47 | A2 | Sep 18, 2017 - Sep 22, 2017 | Skopje, Macedonia | Apr 20, 2017 (Apr 13, 2017) | 2017091820170922 | 20170420
ECOOP | European Conference on Object-oriented Programming | 61 | A1 | Jun 19, 2017 - Jun 23, 2017 | Barcelona, Spain | Jan 13, 2017 (Jan 7, 2017) | 2017061920170623 | 20170113
ECOWS | IEEE European Conference on Web Services | 27 | B1 |  |  |  | z | z
ECRTS | Euromicro Conference on Real-Time Systems | 47 | A2 |  |  |  | z | z
ECSA | European Conference on Software Architecture | 15 | B3 | Sep 11, 2017 - Sep 15, 2017 | Canterbury, UK | Apr 26, 2017 (Apr 19, 2017) | 2017091120170915 | 20170426
ECSCW | European Conference on Computer Supported Cooperative Work | 27 | B1 | Aug 28, 2017 - Sep 1, 2017 | Sheffield, UK | Nov 18, 2016 | 2017082820170901 | 20161118
ECSQARU | European Conference on Symbolic and Quantitative Approaches to Reasoning with Uncertainty | 25 | B1 |  |  |  | z | z
ECWeb | International Conference on Electronic Commerce and Web Technology | 27 | B1 |  |  |  | z | z
EC_A | IADIS International Conference e-Commerce | 6 | B4 |  |  |  | z | z
ED-MEDIA | World Conference on Educational Multimedia Hypermedia and Telecommunications | 14 | B3 | Jan 20, 2017 - Jun 1, 2017 | Baltimore, MD | Jun 1, 2017 | 2017012020170601 | 20170601
EDBT | International Conference on Extending Database Technology | 52 | A2 | Mar 21, 2017 - Mar 24, 2017 | Venice, Italy | Sep 12, 2016 (Sep 5, 2016) | 2017032120170324 | 20160912
EDCC | European Dependable Computing Conference | 17 | B2 | Sep 4, 2017 - Sep 8, 2017 | Geneva, Switzerland | Apr 17, 2017 (Apr 10, 2017) | 2017090420170908 | 20170417
EDM | International Conference on Educational Data Mining | 13 | B3 | Jun 23, 2017 - Jun 25, 2017 | Crimson Resort & Spa, Cebu, Philippines | Feb 25, 2017 | 2017062320170625 | 20170225
EDOC | IEEE International Enterprise Distributed Object Computing Conference | 42 | A2 | Oct 10, 2017 - Oct 13, 2017 | Québec City, Canada | May 7, 2017 (Apr 30, 2017) | 2017101020171013 | 20170507
EDOCW | IEEE International Enterprise Distributed Object Computing Conference Workshops | 13 | B3 |  |  |  | z | z
EDUCON | IEEE Engineering Education Conference | 7 | B4 |  |  |  | z | z
Edutainment | International Conference on E-learning and Games | 14 | B3 |  |  |  | z | z
EEE | IEEE International Conference on e-Technology e-Commerce and e- Service | 24 | B1 | Sep 16, 2017 - Sep 17, 2017 | Toronto | TBD | 2017091620170917 | z
EGC | European Grid Conference | 21 | B2 | Jan 23, 2017 - Jan 27, 2017 | Grenoble, France | Oct 14, 2016 (Oct 7, 2016) | 2017012320170127 | 20161014
EGSR | Eurographics Symposium on Rendering | 34 | B1 | Jun 19, 2017 - Jun 21, 2017 | Helsinki, Finland | Mar 31, 2017 (Mar 27, 2017) | 2017061920170621 | 20170331
EGVE | Eurographics Workshop on Virtual Environments | 10 | B4 |  |  |  | z | z
EICS | The ACM SIGCHI Symposium on Engineering Interactive Computing Systems | 30 | B1 | Jun 26, 2017 - Jun 29, 2017 | Lisbon, Portugal | Jan 17, 2017 | 2017062620170629 | 20170117
EISWT | International Conference on Enterprise Information Systems and Web Technologies | 4 | B4 |  |  |  | z | z
EIT | IEEE International Conference on Electro/Information Technology | 6 | B4 |  |  |  | z | z
EJC | European-Japanese Conference on Information Modelling and Knowledge Bases | 4 | B4 |  |  |  | z | z
EKAW | International Conference on Knowledge Engineering and Knowledge Management | 33 | B1 |  |  |  | z | z
EKNOW | International Conference on Information Process and Knowledge Management | 5 | B4 | Mar 19, 2017 - Mar 23, 2017 | Nice, France | Dec 1, 2016 | 2017031920170323 | 20161201
ELAVIO | Latin American Operations Research Summer School | 2 | B5 |  |  |  | z | z
ELC | Encontro de Linguística de Corpus | 2 | B5 |  |  |  | z | z
ELearn | World Conference on E-Learning in Corporate Government Healthcare & Higher Education | 15 | B3 |  |  |  | z | z
ELML | International Conference on Mobile Hybrid and On-line Learning | 5 | B4 | Mar 19, 2017 - Mar 23, 2017 | Nice, France | Dec 1, 2016 | 2017031920170323 | 20161201
EMBC | International Conference of the IEEE Engineering in Medicine and Biology Society | 26 | B1 | Jul 11, 2017 - Jul 15, 2017 | Jeju Island, Korea | Feb 3, 2017 | 2017071120170715 | 20170203
EMicro | South Microelectronics School | 2 | B5 |  |  |  | z | z
EMMSAD | Exploring Modelling Methods in Systems Analysis and Design | 7 | B4 | Jun 12, 2017 - Jun 13, 2017 | Essen, Germany | Mar 1, 2017 (Feb 22, 2017) | 2017061220170613 | 20170301
Emnets | Workshop on Embedded Networked Sensors | 16 | B2 |  |  |  | z | z
EMNLP | Conference on Empirical Methods in Natural Language Processing | 72 | A1 | Sep 7, 2017 - Sep 11, 2017 | Copenhagen | Apr 14, 2017 | 2017090720170911 | 20170414
EMO | International Conference on Evolutionary Multi-Criterion Optimization | 40 | A2 |  |  |  | z | z
EMS | EMS Network Conference | 1 | B5 | Aug 14, 2017 - Aug 17, 2017 | Bristol, UK | Apr 21, 2017 | 2017081420170817 | 20170421
EMSOFT | ACM Conference on Embedded Software | 42 | A2 |  |  |  | z | z
ENADI | Encontro de Administração da Informação | 3 | B5 |  |  |  | z | z
ENASE | International Conference on Evaluation of Novel Approaches to Software Engineering | 7 | B4 | Apr 28, 2017 - Apr 29, 2017 | Porto, Portugal | Nov 30, 2016 | 2017042820170429 | 20161130
Engopt | International Conference on Engineering Optimization | 5 | B4 |  |  |  | z | z
ENIA | Encontro Nacional de Intelig | 8 | B4 |  |  |  | z | z
ENLG | European Workshop on Natural Language Generation | 4 | B4 |  |  |  | z | z
ENRI | Encontro de Robótica Inteligente | 5 | B4 |  |  |  | z | z
ENTER | International Conference on Information and Communication Technologies in Tourism | 17 | B2 |  |  |  | z | z
EOMAS | Enterprise and Organizational Modeling and Simulation | 3 | B5 | Jun 12, 2017 - Jun 13, 2017 | Essen, Germany | Apr 2, 2017 | 2017061220170613 | 20170402
EON | Workshop on Evaluation of Ontology-based Tools | 12 | B3 |  |  |  | z | z
ePart | International Conference on Electronic Participation | 6 | B4 |  |  |  | z | z
EPIA | Portuguese Conference on Artiificial Intelligence/Encontro Português de Inteligência Artificial | 19 | B2 | Sep 5, 2017 - Sep 8, 2017 | Porto | Apr 1, 2017 | 2017090520170908 | 20170401
EPIO | Escuela de Perfeccionamiento en Investigación Operativa | 1 | B5 |  |  |  | z | z
EpiRob | International Conference on Epigenetic Robotics | 6 | B4 | Sep 18, 2017 - Sep 21, 2017 | Lisbon, Portugal | Mar 30, 2017 | 2017091820170921 | 20170330
ER | International Conference on Conceptual Modeling | 42 | A2 | Nov 6, 2017 - Nov 9, 2017 | Valencia | Apr 17, 2017 (Apr 10, 2017) | 2017110620171109 | 20170417
ERAD | Escola Regional de Alto Desempenho | 1 | B5 |  |  |  | z | z
ERSA | International Conference on Engineering of Reconfigurable Systems and Algorithms | 15 | B3 |  |  |  | z | z
ESA | European Symposium on Algorithms | 46 | A2 | Sep 4, 2017 - Sep 6, 2017 | Vienna, Austria | TBD | 2017090420170906 | z
ESAIR | Exploiting Semantic Annotations in Information Retrieval | 4 | B4 |  |  |  | z | z
ESANN | European Symposium on Artificial Neural Networks | 27 | B1 | Apr 26, 2017 - Apr 28, 2017 | Bruges, Belgium | Nov 19, 2016 | 2017042620170428 | 20161119
ESAW | International Workshop on Engineering Societies in the Agents World | 13 | B3 |  |  |  | z | z
eScience | IEEE International Conference on e-science and Grid Computing | 22 | B1 | Oct 24, 2017 - Oct 27, 2017 | Auckland, New Zealand | Jun 23, 2017 | 2017102420171027 | 20170623
ESEC/FSE | European Software Engineering Conference and the ACM SIGSOFT Symposium on the Foundations of Software Engineering | 49 | A2 | Sep 4, 2017 - Sep 8, 2017 | Paderborn, Germany | Feb 27, 2017 | 2017090420170908 | 20170227
ESELAW | Experimental Software Engineering Latin American Workshop | 4 | B4 |  |  |  | z | z
ESEM | International Symposium on Empirical Software Engineering and Measurement | 44 | A2 | Nov 9, 2017 - Nov 10, 2017 | Toronto | Apr 7, 2017 (Mar 31, 2017) | 2017110920171110 | 20170407
ESM | European Simulation Multiconference | 16 | B2 | Oct 25, 2017 - Oct 27, 2017 | Lisbon, Portugal | Jun 15, 2017 | 2017102520171027 | 20170615
ESOP | European Symposium on Programming ETAPS) | 53 | A2 |  |  |  | z | z
ESORICS | European Symposium on Research in Computer Security | 45 | A2 | Sep 11, 2017 - Sep 15, 2017 | Oslo, Norway | Apr 19, 2017 | 2017091120170915 | 20170419
ESS | European Simulation Symposium | 12 | B3 |  |  |  | z | z
ESSCIRC | European Solid-State Circuits Conference | 33 | B1 | Sep 11, 2017 - Sep 14, 2017 | Leuven (Belgium) | Apr 10, 2017 | 2017091120170914 | 20170410
ESSDERC | European Solid-State Device Research Conference | 12 | B3 | Sep 11, 2017 - Sep 14, 2017 | Leuven (Belgium) | Apr 10, 2017 | 2017091120170914 | 20170410
ESTIMedia | IEEE Workshop on Embedded Systems for Real-Time Multimedia | 13 | B3 |  |  |  | z | z
ESWC | European Semantic Web Conference | 57 | A1 | May 28, 2017 - Jun 1, 2017 | Portoroz, Slovenia | Dec 14, 2016 (Dec 7, 2016) | 2017052820170601 | 20161214
ETCS | IEEE International Workshop on Education Technology and Computer Science | 8 | B4 |  |  |  | z | z
ETELEMED | International Conference on eHealth Telemedicine and Social Medicine | 7 | B4 | Mar 19, 2017 - Mar 23, 2017 | Nice, France | Dec 1, 2016 | 2017031920170323 | 20161201
ETFA | IEEE International Conference on Emerging Technologies and Factory Automation | 26 | B1 | Sep 12, 2017 - Sep 15, 2017 | Limassol, Cyprus | Mar 12, 2017 | 2017091220170915 | 20170312
ETHICOMP | International Conference on the Social and Ethical Impacts of Information and Communication Technologies | 6 | B4 |  |  |  | z | z
ETRA | Eye Tracking Research and Applications | 3 | B5 |  |  |  | z | z
ETS | IEEE European Test Symposium | 18 | B2 |  |  |  | z | z
EUC | IFIP International Conference on Embedded and Ubiquitous Computing | 20 | B2 | Jul 22, 2017 - Jul 23, 2017 | Guangzhou, China | Feb 10, 2017 | 2017072220170723 | 20170210
EUMAS | European Workshop on Multi-Agent Systems | 14 | B3 |  |  |  | z | z
EURO | European Conference on Operational Research | 4 | B4 | Aug 30, 2017 - Sep 1, 2017 | Spain, Santiago de Compostela | Feb 9, 2017 (Feb 2, 2017) | 2017083020170901 | 20170209
Euro-Par | European Conference on Parallel Computing | 43 | A2 | Aug 30, 2017 - Sep 1, 2017 | Spain, Santiago de Compostela | Feb 9, 2017 (Feb 2, 2017) | 2017083020170901 | 20170209
Euro-Plop | European Conference on Pattern Languages of Programs | 16 | B2 | Aug 30, 2017 - Sep 1, 2017 | Spain, Santiago de Compostela | Feb 9, 2017 (Feb 2, 2017) | 2017083020170901 | 20170209
EuroBot | European Workshop on Advanced Mobile Robots | 3 | B3 |  |  |  | z | z
EUROCAST | International Workshop on Computer Aided Systems Theory | 13 | B3 |  |  |  | z | z
EuroCG | European Workshop on Computational Geometry | 10 | B4 | Apr 5, 2017 - Apr 7, 2017 | Malmö University, Sweden | Jan 9, 2017 | 2017040520170407 | 20170109
EUROCOMB | European Conference on Combinatorics Graph Theory and Applications | 1 | B5 |  |  |  | z | z
EUROCON | The International Conference on "Computer as a Tool" | 12 | B3 | Jul 6, 2017 - Jul 8, 2017 | Ohrid, Macedonia | Jan 30, 2017 | 2017070620170708 | 20170130
EuroCrypt | International Conference on the Theory and Application of Cryptographic Techniques | 97 | A1 | Apr 30, 2017 - May 4, 2017 | Paris, France | Oct 1, 2016 | 2017043020170504 | 20161001
EuroGP | European Conference on Genetic Programming | 35 | B1 | Apr 19, 2017 - Apr 21, 2017 | Amsterdam | Nov 1, 2016 | 2017041920170421 | 20161101
EUROGRAPHICS | Eurographics Annual Conference of the European Association for Computer Graphics | 5 | A2 | Jun 19, 2017 - Jun 21, 2017 | Helsinki, Finland | Mar 31, 2017 (Mar 27, 2017) | 2017061920170621 | 20170331
EuroIMSA | IASTED International Conference on Internet and Multimedia Systems and Applications | 9 | B4 |  |  |  | z | z
EuroITV | European Interactive TV Conference | 14 | B3 |  |  |  | z | z
EuroMPI | European MPI Users Group Conference | 27 | B1 | Sep 25, 2016 - Sep 28, 2016 | Chicago, IL, USA | May 22, 2017 | 2016092520160928 | 20170522
EUROPKI | European Public Key Infrastructure Workshop | 16 | B2 |  |  |  | z | z
EUROPT | Workshop on Advances in Continuous Optimization | 2 | B5 |  |  |  | z | z
EuroSPI | European Conference on Software Process Improvement | 20 | B2 |  |  |  | z | z
EuroSys | European Conference on Computer Systems | 47 | A2 | Apr 23, 2017 - Apr 26, 2017 | Belgrade, Serbia | Oct 21, 2016 (Oct 14, 2016) | 2017042320170426 | 20161021
EuroVis | Eurographics/IEEE-VGTC Symposium on Visualization | 23 | B1 | Jun 12, 2017 - Jun 16, 2017 | Barcelona | Dec 12, 2016 (Dec 5, 2016) | 2017061220170616 | 20161212
EUSIPCO | European Signal Processing Conference | 23 | B1 | Aug 28, 2017 - Sep 2, 2017 | Kos, Greece | Mar 5, 2017 | 2017082820170902 | 20170305
Evoapplications | European Conference on the Applications of Evolutionary Computation | 2 | B5 |  |  |  | z | z
Evobio | European Conference on Evolutionary Computation Machine Learning and Data Mining in Computational Biology | 11 | B3 |  |  |  | z | z
EvoCOP | European Conference on Evolutionary Computation in Combinatorial Optimization | 20 | B2 |  |  |  | z | z
EWME | European Workshop on Microelectronics Education | 5 | B4 |  |  |  | z | z
EWSN | European conference on Wireless Sensor Networks | 37 | A2 | Feb 20, 2017 - Feb 22, 2017 | Uppsala, Sweden​  | Sep 19, 2016 (Sep 12, 2016) | 2017022020170222 | 20160919
FAABS | IEEE Workshop on Formal Approaches to Agent-Based Systems | 18 | B2 |  |  |  | z | z
FASE | Fundamental Approaches to Software Engineering | 42 | A2 |  |  |  | z | z
FAST | USENIX Conference on File and Storage Technologies | 55 | A1 | Feb 27, 2017 - Mar 2, 2017 | Santa Clara, CA | Sep 27, 2016 | 2017022720170302 | 20160927
FC | Financial Cryptography  | 36 | B1 | Apr 3, 2017 - Apr 7, 2017 | Sliema, Malta | Nov 4, 2016 | 2017040320170407 | 20161104
FCCM | IEEE Symposium on Field-Programmable Custom Computing Machines | 46 | A2 |  |  |  | z | z
FCS | International Conference on Foundations of Computer Science | 5 | B4 |  |  |  | z | z
FCT | International Symposium on Fundamentals of Computation Theory | 18 | B2 | Sep 11, 2017 - Sep 13, 2017 | Bordeaux, France | TBD | 2017091120170913 | z
FDG | Foundations of Digital Games | 11 | B3 | Aug 14, 2017 - Aug 17, 2017 | Hyannis, MA, USA | Mar 6, 2017 | 2017081420170817 | 20170306
FDL | Forum on Specification and Design Languages | 15 | B3 |  |  |  | z | z
FDTC | Workshop on Fault Diagnosis and Tolerance in Cryptography | 17 | B2 |  |  |  | z | z
FECS | International Conference on Frontiers in Education: Computer Science and Computer Engineering | 5 | B4 |  |  |  | z | z
FedCSIS | Federated Conference on Computer Science and Information Systems | 3 | B5 | Sep 3, 2017 - Sep 6, 2017 | Prague, Czech Republic | May 10, 2017 | 2017090320170906 | 20170510
FEF | The Finite Elements in Flow Problems Conference | 2 | B5 |  |  |  | z | z
FG | International Conference on Automatic Face and Gesture Recognition | 70 | A1 | Jul 22, 2017 - Jul 23, 2017 | Toulouse, France | Mar 28, 2017 | 2017072220170723 | 20170328
FGCN | International Conference on Future Generation Communication and Networking | 13 | B3 |  |  |  | z | z
FGIT | Future Generation Information Technology | 5 | B4 |  |  |  | z | z
FIE | IEEE Frontiers in Education Conference | 24 | B1 | Oct 18, 2017 - Oct 21, 2017 | Indianapolis, IN | Feb 9, 2017 | 2017101820171021 | 20170209
FIND | Dynamic Taxonomies and Faceted Search | 1 | B5 |  |  |  | z | z
FINTDI | Promotion and Innovation with New Technologies in Engineering Education | 1 | B5 |  |  |  | z | z
FIT | Frontiers of Information Technology | 5 | B4 | Dec 18, 2017 - Dec 20, 2017 | Islamabad, Pakistan | Jul 15, 2017 | 2017121820171220 | 20170715
FLAIRS | Florida Artificial Intelligence Research Society | 25 | B1 | May 22, 2017 - May 24, 2017 | Marco Island, Florida | Nov 21, 2016 | 2017052220170524 | 20161121
FLINS | International FLINS Conference on Uncertainty Modeling in Knowledge Engineering and Decision Making | 7 | B4 |  |  |  | z | z
FLOPS | International Symposium on Functional and Logic Programming | 22 | B1 |  |  |  | z | z
FM | International Symposium on Formal Methods | 35 | B1 |  |  |  | z | z
FMCAD | Formal Methods in Computer Aided Design | 33 | B1 | Oct 2, 2017 - Oct 6, 2017 | Vienna, Austria | May 8, 2017 (May 1, 2017) | 2017100220171006 | 20170508
FMICS | International Workshop on Formal Methods for Industrial Critical Systems | 9 | B4 | Sep 18, 2017 - Sep 20, 2017 | University of Torino, Italy | May 21, 2017 (May 14, 2017) | 2017091820170920 | 20170521
FMN | Future Multimedia Networking | 4 | B4 |  |  |  | z | z
FMOODS | International Conference on Formal Methods for Open Object-Based Distributed Systems | 28 | B1 |  |  |  | z | z
FOAL | Foundations of Aspect-Oriented Languages Workshop | 18 | B2 |  |  |  | z | z
FOCI | IEEE Symposium on Foundations of Computational Intelligence | 15 | B3 | Aug 14, 2017 - Aug 14, 2017 | Vancouver, BC, Canada | May 26, 2017 | 2017081420170814 | 20170526
FOCS | IEEE Symposium on Foundations of Computer Science | 94 | A1 | Oct 15, 2017 - Oct 17, 2017 | Berkeley, California | Apr 6, 2017 | 2017101520171017 | 20170406
FOGA | Foundations of Genetic Algorithms | 13 | B3 |  |  |  | z | z
FoIKS | International Symposium on Foundations on Information and Knowledge Systems | 18 | B2 |  |  |  | z | z
FOIS | International Conference on Formal Ontology in Information Systems | 25 | B1 |  |  |  | z | z
FOMI | Formal Ontologies Meet Industry | 6 | B4 |  |  |  | z | z
FOOL | International Workshop on Foundations of Object-Oriented Languages | 7 | B3 |  |  |  | z | z
FORMATS | International Conference on Formal Modelling and Analysis of Timed Systems | 22 | B1 |  |  |  | z | z
FORTE | IFIP WG 6.1 International Conference on Formal Methods for Networked and Distributed Systems | 29 | B1 | Oct 2, 2017 - Oct 6, 2017 | Forte Village Resort | Mar 1, 2017 | 2017100220171006 | 20170301
FOSSACS | Foundations of Software Science and Computation Structures Conference | 37 | A2 | Apr 22, 2017 - Apr 29, 2017 | Uppsala, Sweden | Oct 21, 2016 (Oct 14, 2016) | 2017042220170429 | 20161021
FPGA | ACM/SIGDA International Symposium on Field-Programmable Gate Arrays | 53 | A2 | Jul 10, 2017 - Jul 12, 2017 | Seattle, WA USA | Apr 3, 2017 | 2017071020170712 | 20170403
FPL | International Conference on Field-Programmable Logic and Applications | 50 | A2 | Sep 4, 2017 - Sep 8, 2017 | Ghent, Belgium | Apr 5, 2017 (Mar 29, 2017) | 2017090420170908 | 20170405
FPSAC | International Conference on Formal Power Series and Algebraic Combinatorics | 9 | B4 |  |  |  | z | z
FQAS | International Conference on Flexible Query Answering Systems | 17 | B2 |  |  |  | z | z
FroCoS | International Symposium on Frontiers of Combining Systems | 1 | B3 | Sep 25, 2017 - Sep 29, 2017 | Brasilia, Brazil | Apr 28, 2017 (Apr 24, 2017) | 2017092520170929 | 20170428
FSE | Fast Software Encryption Workshop | 46 | A2 | Sep 4, 2017 - Sep 8, 2017 | Paderborn, Germany | Feb 27, 2017 | 2017090420170908 | 20170227
FSE_A | ACM SIGSOFT Conference on the Foundations of Software Engineering | 59 | A1 |  |  |  | z | z
FSKD | International Conference on Fuzzy Systems and Knowledge Discovery | 18 | B2 |  |  |  | z | z
FSR | International Symposium on Field and Service Robotics | 4 | B4 |  |  |  | z | z
FSTTCS | IARCS Conference on Foundations of Software Technology and Theoretical Computer Science | 33 | B1 |  |  |  | z | z
FTFC | IEEE Faible Tension Faible Consommation | 2 | B5 |  |  |  | z | z
FTJP | Workshop on Formal Techniques for Java Programs | 8 | B4 |  |  |  | z | z
FTP | International Workshops on First-Order Theorem Proving | 5 | B4 |  |  |  | z | z
FTRTFT | Formal Techniques in Real-Time and Fault Tolerant Systems | 20 | B2 |  |  |  | z | z
FUSION | International Conference on Information Fusion | 25 | B1 | Jul 10, 2017 - Jul 13, 2017 | Xi'an, China | Mar 1, 2017 | 2017071020170713 | 20170301
FUZZ | IEEE International Conference on Fuzzy Systems | 38 | A2 | Jul 9, 2017 - Jul 12, 2017 | Naples, Italy | Feb 10, 2017 | 2017070920170712 | 20170210
GADA | International Conference on Grid computing High-performance and Distributed Applications | 1 | B5 |  |  |  | z | z
GAME-ON | European GAME-ON Conference | 9 | B3 | Oct 30, 2017 - Oct 31, 2017 | Heraklion, Crete, Greece | May 31, 2017 | 2017103020171031 | 20170531
GCA | International Conference on Grid Computing and Applications | 8 | B4 | Nov 19, 2017 - Nov 22, 2017 | Aomori, Japan | Aug 6, 2017 (Aug 1, 2017) | 2017111920171122 | 20170806
GCC | International Conference on Grid and Cooperative Computing | 18 | B2 |  |  |  | z | z
GD | International Symposium on Graph Drawing | 18 | B2 | Sep 25, 2017 - Sep 27, 2017 | Boston, MA, U.S.A. | Jun 11, 2017 | 2017092520170927 | 20170611
GDC | Game Developers Conference | 22 | B1 | Oct 12, 2017 - Oct 13, 2017 | Aveiro, Portugal | Jun 29, 2017 (Jun 22, 2017) | 2017101220171013 | 20170629
GECCO | Genetic and Evolutionary Computation Conference | 66 | A1 | Jul 15, 2017 - Jul 19, 2017 | Berlin, Germany | Feb 6, 2017 (Jan 30, 2017) | 2017071520170719 | 20170206
GECON | Grid Economics and Business Models | 12 | B3 | Sep 19, 2017 - Sep 21, 2017 | Biarritz-Anglet-Bayonne, France | Jun 7, 2017 (Jun 1, 2017) | 2017091920170921 | 20170607
GEFS | International Workshop on Genetic and Evolving Fuzzy Systems | 7 | B4 |  |  |  | z | z
GENSIPS | IEEE International Workshop on Genomic Signal Processing and Statistics | 8 | B4 |  |  |  | z | z
GeoComp | International Conference on GeoComputation | 13 | B3 |  |  |  | z | z
GeoInfo | Simpósio Brasileiro de Geoinformática | 14 | B3 |  |  |  | z | z
GEOPROCESSING | International Conference on Advanced Geographic Information Systems Applications and Services | 4 | B4 | Mar 19, 2017 - Mar 23, 2017 | Nice, France | Dec 1, 2016 | 2017031920170323 | 20161201
GeoS | International Conference on Geospatial Semantics | 14 | B3 |  |  |  | z | z
GI | Graphics Interface | 51 | A2 | Sep 25, 2017 - Sep 29, 2017 | Chemnitz | May 14, 2017 | 2017092520170929 | 20170514
GIIS | IEEE International Global Information Infrastructure Symposium | 8 | B4 | Oct 25, 2017 - Oct 27, 2017 | Saint Pierre, Reunion Island, France | May 1, 2017 | 2017102520171027 | 20170501
GIR | Workshop on Geographic Information Retrieval | 19 | B2 |  |  |  | z | z
GIS | International Symposium on Advances in Geographic Information Systems | 33 | B1 | Nov 7, 2017 - Nov 10, 2017 | Los Angeles, CA | Jun 13, 2017 (Jun 6, 2017) | 2017110720171110 | 20170613
GIScience | International Conference on Geographic Information Science | 23 | B1 | Sep 4, 2017 - Sep 4, 2017 | L'Aquila, Italy | May 12, 2017 | 2017090420170904 | 20170512
Globe | Conference on Data Management in Grid and P2P Systems | 5 | B4 |  |  |  | z | z
GLOBECOM | IEEE Global Telecommunications Conference | 90 | A1 | Dec 4, 2017 - Dec 8, 2017 | Singapore | Apr 1, 2017 | 2017120420171208 | 20170401
GLSVLSI | ACM Great Lakes Symposium on VLSI | 28 | B1 | May 10, 2017 - May 12, 2017 | Banff, Alberta, Canada | Jan 6, 2017 (Dec 23, 2016) | 2017051020170512 | 20170106
GMP | Geometry Modeling and Processing Conference | 18 | B2 | Apr 17, 2017 - Apr 19, 2017 | Xiamen, China | Nov 10, 2016 (Nov 3, 2016) | 2017041720170419 | 20161110
GOW | Global Optimization Workshop | 2 | B5 |  |  |  | z | z
GPC | Advances in Grid and Pervasive Computing | 12 | B3 | May 11, 2017 - May 14, 2017 | Cetara, Amalfi Coast, Italy | Jan 15, 2017 | 2017051120170514 | 20170115
GPCE | International Conference on Generative Programming and Component Engineering | 37 | A2 | Oct 23, 2017 - Oct 24, 2017 | Vancouver, Canada | Jul 2, 2017 (Jun 25, 2017) | 2017102320171024 | 20170702
GRAPP | International Conference on Computer Graphics Theory and Applications | 9 | B4 |  |  |  | z | z
GREC | IAPR International Workshop on Graphics Recognition | 1 | B3 |  |  |  | z | z
GREENCOM | Green Computing and Communications | 6 | B4 | Jun 21, 2017 - Jun 23, 2017 | Exeter, UK | Feb 23, 2017 | 2017062120170623 | 20170223
GREENCOM-CPSCOM | IEEE-ACM International Conference on Green Computing and Communications and International Conference on Cyber Physical and Social Computing | 6 | B4 | Jun 21, 2017 - Jun 23, 2017 | Exeter, UK | Feb 23, 2017 | 2017062120170623 | 20170223
GRID | IEEE/ACM International Conference on Grid Computing | 46 | A2 | Dec 4, 2017 - Dec 8, 2017 | Singapore | Apr 1, 2017 | 2017120420171208 | 20170401
GROUP | International Conference on Supporting Group Work | 45 | A2 | Dec 1, 2016 - Mar 15, 2017 | N/A | Mar 15, 2017 (Dec 1, 2016) | 2016120120170315 | 20170315
GSD | International Workshop on Global Software Development for the Practitioner | 14 | B3 |  |  |  | z | z
GSDI | International Conference for Global Spatial Data Infrastructure | 9 | B4 |  |  |  | z | z
GWC | International Global Wordnet Conference | 14 | B3 |  |  |  | z | z
HAIS | International Conference on Hybrid Artificial Intelligence Systems | 9 | B4 | Jun 21, 2017 - Jun 23, 2017 | La Rioja, Spain | Feb 1, 2017 (Apr 5, 2017) | 2017062120170623 | 20170201
HAPTICS | International Symposium on Haptic Interfaces for Virtual Environment and Teleoperator Systems | 28 | B1 |  |  |  | z | z
HASE | IEEE International Symposium on High Assurance Systems Engineering | 22 | B1 | Jan 12, 2017 - Jan 14, 2017 | Singapore | Sep 12, 2016 | 2017011220170114 | 20160912
HASKELL | Haskell Workshop | 22 | B1 |  |  |  | z | z
HC | Humans and Computers | 3 | B5 |  |  |  | z | z
HCII | International Conference on Human-Computer Interaction | 17 | B2 |  |  |  | z | z
HCS | IEEE Hot Chips Symposium | 7 | B4 |  |  |  | z | z
HealthCom | International Workshop on Enterprise Networking and Computing in Health Care Industry | 10 | B4 | Oct 12, 2017 - Oct 15, 2017 | Dalian, China | Jul 25, 2017 | 2017101220171015 | 20170725
HEALTHINF | International Conference on Health Informatics | 7 | B4 | Feb 21, 2017 - Feb 23, 2017 | Porto, Portugal | Oct 20, 2016 | 2017022120170223 | 20161020
HIC | Health Informatics Conference | 8 | B4 |  |  |  | z | z
HICSS | Annual Hawaii International Conference on System Sciences | 97 | A1 | Jan 4, 2017 - Jan 7, 2017 | Hilton Waikoloa Village | Jun 15, 2016 | 2017010420170107 | 20160615
HIKM | Healthcare Information and Knowledge Management | 6 | B4 |  |  |  | z | z
HiPC | International Conference on High Performance Computing | 25 | B1 | Dec 18, 2017 - Dec 21, 2017 | Jaipur, India | Jun 15, 2017 (Jun 5, 2017) | 2017121820171221 | 20170615
HiPEAC | International Conference on High-Performance and Embedded Architectures and Compilers | 17 | B2 | Jan 23, 2017 - Jan 25, 2017 | Stockholm, Sweden  | Jun 1, 2016 | 2017012320170125 | 20160601
HIPS | International Workshop on High-Level Programming Models and Supportive Environments | 10 | B4 |  |  |  | z | z
HIS | International Conference on Hybrid Intelligent Systems | 18 | B2 | Sep 25, 2017 - Sep 26, 2017 | Katowice | May 31, 2017 | 2017092520170926 | 20170531
HLDVT | IEEE International High Level Design Validation and Test Workshop | 6 | B3 | Oct 5, 2017 - Oct 6, 2017 | Santa Cruz, CA | Jul 23, 2017 (Jul 14, 2017) | 2017100520171006 | 20170723
HLT | Human Language Technology Conference | 78 | A1 |  |  |  | z | z
HM | International Workshop on Hybrid Metaheuristics | 15 | B3 |  |  |  | z | z
HoloMAS | Holonic and Multi-Agent Systems for Manufacturing | 17 | B2 | Aug 28, 2017 - Aug 30, 2017 | Lyon, France | Mar 27, 2017 | 2017082820170830 | 20170327
HOPL | History of Programming Languages Conference | 2 | B5 |  |  |  | z | z
HOST | Hardware-Oriented Security and Trust HOST) IEEE International Workshop on | 14 | B3 | May 1, 2017 - May 5, 2017 | McLean, VA, USA | Nov 1, 2016 | 2017050120170505 | 20161101
Hot-P2P | International Workshop on Hot Topics in Peer-to-Peer Systems | 8 | B4 | Dec 1, 2017 - Dec 1, 2017 |  VMWare Campus | Aug 4, 2017 (Jul 28, 2017) | 2017120120171201 | 20170804
HOTI | IEEE Symposium on High-Performance Interconnects | 23 | B1 |  |  |  | z | z
HotInterconnect | Symposium on High-Performance Interconnects | 21 | B2 |  |  |  | z | z
HotMobile | Workshop on Mobile Computing Systems and Applications | 18 | B2 | Feb 21, 2017 - Feb 22, 2017 | Sonoma, California | Oct 18, 2016 | 2017022120170222 | 20161018
HOTNETS | Workshop on Hot Topics in Networks | 15 | B3 | Dec 1, 2017 - Dec 1, 2017 |  VMWare Campus | Aug 4, 2017 (Jul 28, 2017) | 2017120120171201 | 20170804
HotOS | USENIX Workshop on Hot Topics in Operating Systems | 44 | A2 | May 7, 2017 - May 10, 2017 | Whistler, BC | Jan 25, 2017 | 2017050720170510 | 20170125
HPCA | International Symposium on High-Performance Computer Architecture | 73 | A1 | Feb 4, 2017 - Feb 8, 2017 | Austin, TX, USA | Aug 1, 2016 (Jul 25, 2016) | 2017020420170208 | 20160801
HPCN | International Conference on High Performance Computing and Networking | 14 | B3 |  |  |  | z | z
HPCS | International Symposium on High Performance Computing Systems and Applications | 15 | B3 | Jul 17, 2017 - Jul 21, 2017 | Genoa, Italy | Mar 24, 2017 | 2017071720170721 | 20170324
HPDC | IEEE International Symposium on High Performance Distributed Computing | 62 | A1 | Jun 26, 2017 - Jun 30, 2017 | Washington DC | Jan 17, 2017 (Jan 10, 2017) | 2017062620170630 | 20170117
HRI | IEEE/ACM International Conference on Human Robot Interaction | 38 | A2 | Mar 6, 2016 - Mar 9, 2016 | Vienna, Austria | Oct 3, 2016 | 2016030620160309 | 20161003
Humanoids | IEEE RAS International Conference on Humanoids Robots | 17 | B2 | Nov 15, 2017 - Nov 17, 2017 | 15 Nov - 17 Nov 2017 | Jul 15, 2017 | 2017111520171117 | 20170715
HVC | International Haifa Verification Conference | 5 | B4 | Nov 13, 2017 - Nov 15, 2017 | Haifa, Israel | Jul 21, 2017 | 2017111320171115 | 20170721
Hypertext | ACM Conference on Hypertext and Hypermedia | 45 | A2 | Jul 4, 2017 - Jul 7, 2017 | Prague, Czech Republic | Feb 17, 2017 (Feb 10, 2017) | 2017070420170707 | 20170217
I | IADIS International Conference Informatics | 3 | B5 | Jun 26, 2017 - Jun 30, 2017 | Washington DC | Jan 17, 2017 (Jan 10, 2017) | 2017062620170630 | 20170117
I&CPS | IEEE Industrial and Commercial Power Systems Technical Conference - Conference Record | 10 | B4 | Jun 26, 2017 - Jun 30, 2017 | Washington DC | Jan 17, 2017 (Jan 10, 2017) | 2017062620170630 | 20170117
I-KNOW | International Conference on Knowledge Management and Knowledge Technologies | 3 | B5 | Jun 26, 2017 - Jun 30, 2017 | Washington DC | Jan 17, 2017 (Jan 10, 2017) | 2017062620170630 | 20170117
I-SEMANTICS | International Conference on Semantic Systems | 11 | B3 | Jun 26, 2017 - Jun 30, 2017 | Washington DC | Jan 17, 2017 (Jan 10, 2017) | 2017062620170630 | 20170117
I-SPAN | International Symposium on Parallel Architectures Algorithms and Networks | 22 | B1 | Jun 26, 2017 - Jun 30, 2017 | Washington DC | Jan 17, 2017 (Jan 10, 2017) | 2017062620170630 | 20170117
I2TS | Information and Telecommunication Technologies | 6 | B4 |  |  |  | z | z
I3D | ACM Symposium on Interactive 3D Graphics and Games | 50 | A2 |  |  |  | z | z
I3E | IFIP Conference on e-Commerce e-Business and e-Government | 12 | B3 |  |  |  | z | z
IAAI | Conference on Innovative Applications in Artificial Intelligence | 14 | B2 | Feb 6, 2017 - Feb 9, 2017 | San Francisco, California USA | Sep 12, 2016 | 2017020620170209 | 20160912
IAIM | Annual Conference of the International Academy for Information Management | 3 | B5 |  |  |  | z | z
IAIT | International Conference on Advances in Information Technology | 3 | B5 |  |  |  | z | z
IAS | IEEE Industry Applications Society Annual Conference | 16 | B1 |  |  |  | z | z
IAT | ACM International Conference on Intelligent Agent Technology | 25 | B1 |  |  |  | z | z
IAW | Annual IEEE Information Assurance Workshop | 34 | B1 |  |  |  | z | z
IAWTIC | International Conference on Intelligent Agents Web Technologies and Internet Commerce | 3 | B4 |  |  |  | z | z
IBERAMIA | Ibero-American Artificial Intelligence Conference | 16 | B2 |  |  |  | z | z
IBERCHIP | Workshop Iberchip | 6 | B4 |  |  |  | z | z
IBICA | International Conference on Innovations in Bio-inspired Computing and Applications | 1 | B5 |  |  |  | z | z
IBIMA | International Business Information Management | 6 | B4 |  |  |  | z | z
IC3K | International Joint Conference on Knowledge Discovery Knowledge Engineering and Knowledge Management | 8 | B4 | Nov 1, 2017 - Nov 3, 2017 | Funchal, Madeira, Portugal | May 22, 2017 | 2017110120171103 | 20170522
ICA3PP | International Conference on Algorithms and Architectures for Parallel Processing | 10 | B3 | Aug 21, 2017 - Aug 23, 2017 | Helsinki, Finland | Apr 15, 2017 | 2017082120170823 | 20170415
ICAART | International Conference on Agents and Artificial Intelligence | 8 | B4 | Feb 24, 2017 - Feb 26, 2017 | Porto, Portugal | Oct 4, 2016 | 2017022420170226 | 20161004
ICAC | IEEE International Conference on Autonomic Computing | 43 | A2 | Jul 17, 2017 - Jul 21, 2017 | Columbus, OH, USA | TBD | 2017071720170721 | z
ICAD | International Conference on Auditory Display | 28 | B1 | Nov 30, 2017 - Dec 1, 2017 | UNIVERSITA' DEGLI STUDI DI TORINO | Aug 31, 2017 | 2017113020171201 | 20170831
ICAI | International Conference on Artificial Intelligence | 16 | B2 |  |  |  | z | z
ICAIE | International Conference on Artificial Intelligence and Education | 2 | B5 |  |  |  | z | z
ICAIL | International Conference on Artificial Intelligence and Law | 26 | B1 | Jun 12, 2017 - Jun 16, 2017 | London | Jan 27, 2017 (Jan 6, 2017) | 2017061220170616 | 20170127
ICAIS | International Conference on Adaptive and Intelligent Systems | 11 | B3 |  |  |  | z | z
ICAISC | International Conference on Artifical Intelligence and Soft Computing | 17 | B2 | Jun 11, 2017 - Jun 15, 2017 | Zakopane, Poland | Jan 20, 2017 | 2017061120170615 | 20170120
ICAL | IEEE International Conference on Automation and Logistics | 9 | B4 |  |  |  | z | z
ICALIP | International Conference on Audio Language and Image Processing | 9 | B4 |  |  |  | z | z
ICALP | International Colloquium on Automata Languages and Programming | 61 | A1 | Jul 10, 2017 - Jul 14, 2017 | Warsaw, Poland | Feb 17, 2017 | 2017071020170714 | 20170217
ICALT | IEEE International Conference on Advanced Learning Technologies | 35 | B1 | Jul 3, 2017 - Jul 7, 2017 | Timisoara, Romania | Jan 20, 2017 | 2017070320170707 | 20170120
ICANN | International Conference on Artificial Neural Networks | 30 | B1 | Sep 11, 2017 - Sep 15, 2017 | Alghero, Sardinia, Italy | Apr 3, 2017 | 2017091120170915 | 20170403
ICAPS | International Conference on Automated Planning and Scheduling | 49 | A2 | Jul 18, 2017 - Jul 23, 2017 | Pittsburgh, USA | Nov 22, 2016 (Nov 18, 2016) | 2017071820170723 | 20161122
ICAR | International Conference on Advanced Robotics | 21 | B2 | Jul 10, 2017 - Jul 12, 2017 | Hong Kong | Apr 15, 2017 | 2017071020170712 | 20170415
ICARCV | International Conference on Control Automation Robotics and Vision | 23 | B1 |  |  |  | z | z
ICAS | International Conference on Autonomic and Autonomous Systems | 17 | B2 | May 21, 2017 - May 25, 2017 | Barcelona, Spain | Feb 3, 2017 | 2017052120170525 | 20170203
ICASSP | International Conference on Acoustics Speech and Signal Processing | 84 | A1 | Mar 5, 2017 - Mar 9, 2017 | New Orleans, LA, USA | Sep 12, 2016 | 2017030520170309 | 20160912
ICAT | International Conference on Artificial Reality and Telexistence | 14 | B3 | Oct 26, 2017 - Oct 28, 2017 | Sarajevo | Jun 30, 2017 | 2017102620171028 | 20170630
ICATPN | International Conference on the Application and Theory of Petri Nets | 36 | B1 | Jun 26, 2017 - Jun 27, 2017 | Zaragoza | Jun 15, 2016 | 2017062620170627 | 20160615
ICB | International Conference on Biometrics | 21 | B2 |  |  |  | z | z
ICBAKE | International Conference on Biometrics and Kansei Engineering | 5 | B4 |  |  |  | z | z
ICBBE | International Conference on Bioinformatics and Biomedical Engineering | 10 | B4 | Nov 12, 2017 - Nov 14, 2017 | Seoul, South Korea | Jul 20, 2017 | 2017111220171114 | 20170720
ICC | IEEE International Conference on Communications | 53 | A2 | May 21, 2017 - May 25, 2017 | Paris, France | Oct 14, 2016 | 2017052120170525 | 20161014
ICCABS | IEEE International Conference on Computational Advances in Bio and Medical Sciences | 3 | B5 |  |  |  | z | z
ICCAD | International Conference on Computer-Aided Design | 75 | A1 | Nov 13, 2017 - Nov 17, 2017 | Irvine, CA | Apr 24, 2017 (Apr 17, 2017) | 2017111320171117 | 20170424
ICCAS | International Conference on Control Automation and Systems | 8 | B4 |  |  |  | z | z
ICCBR | International Conference on Case-Based Reasoning | 4 | B4 | Jun 26, 2017 - Jun 28, 2017 | Trondheim, Norway | Mar 20, 2017 | 2017062620170628 | 20170320
ICCBSS | IEEE International Conference on COTS-Based Software Systems | 20 | B2 |  |  |  | z | z
ICCC | International Conference on Computer Communication | 7 | B4 | Dec 13, 2017 - Dec 16, 2017 | Chengdu, China | Jul 25, 2017 | 2017121320171216 | 20170725
ICCCAS | International Conference on Communications Circuits and Systems | 10 | B4 |  |  |  | z | z
ICCCN | International Conference on Computer Communications and Networks | 34 | B1 | Jul 31, 2017 - Aug 3, 2017 | Vancouver, Canada | Feb 21, 2017 (Feb 14, 2017) | 2017073120170803 | 20170221
ICCD | International Conference on Computer Design | 41 | A2 | Nov 5, 2017 - Nov 7, 2017 | Boston, MA | Jun 28, 2017 (Jun 19, 2017) | 2017110520171107 | 20170628
ICCDCS | International Caribbean Conference on Devices Circuits and Systems | 10 | B4 |  |  |  | z | z
ICCE | International Conference on Computers in Education | 18 | B2 | Mar 3, 2017 - Mar 4, 2017 | Kuala Lumpur, Malaysia | Feb 15, 2017 | 2017030320170304 | 20170215
ICCI | International Conference on Computing and Information | 1 | B4 |  |  |  | z | z
ICCIIS | International Conference on Communications and Intelligence Information Security | 3 | B5 |  |  |  | z | z
ICCIMA | International Conference on Computational Intelligence and Multimedia Applications | 18 | B2 |  |  |  | z | z
ICCI_A | IEEE International Conference on Cognitive Informatics | 18 | B2 |  |  |  | z | z
ICCL | IEEE International Conference on Computer Languages | 2 | B5 |  |  |  | z | z
ICCMS | International Conference on Computer Modeling and Simulation | 6 | B4 | Jan 20, 2017 - Jan 23, 2017 | University of Canberra, Australia | Nov 20, 2016 | 2017012020170123 | 20161120
ICCOPT | International Conference on Continuous Optimization | 1 | B5 |  |  |  | z | z
ICCS | International Conference on Computational Science | 37 | A2 | Jun 12, 2017 - Jun 14, 2017 | Zurich, Switzerland | Jan 31, 2017 | 2017061220170614 | 20170131
ICCSA | International Conference on Computational Science and its Applications | 27 | B1 | Jul 3, 2017 - Jul 6, 2017 | Trieste, Italy | Jan 29, 2017 | 2017070320170706 | 20170129
ICCSE | International Conference on Computer Science & Education | 6 | B4 | Jul 6, 2017 - Jul 9, 2017 | Tsinghua University, Beijing, China | May 15, 2017 | 2017070620170709 | 20170515
ICCST | Annual International Carnahan Conference on Security Technology | 8 | B4 | Oct 26, 2017 - Oct 26, 2017 | Madrid | May 12, 2017 | 2017102620171026 | 20170512
ICCS_A | International Conference on Conceptual Structures | 28 | B1 |  |  |  | z | z
ICCTA | International Conference on Computing: Theory and Applications | 11 | B3 |  |  |  | z | z
ICCV | IEEE International Conference on Computer Vision | 120 | A1 | Oct 22, 2017 - Oct 29, 2017 | Venice, Italy | Mar 17, 2017 | 2017102220171029 | 20170317
ICDAR | IEEE International Conference on Document Analysis and Recognition | 48 | A2 | Nov 10, 2017 - Nov 15, 2017 | Kyoto, Japan | Mar 15, 2017 | 2017111020171115 | 20170315
ICDCIT | International Conference on Distributed Computing and Internet Technology | 10 | B4 | Jan 13, 2017 - Jan 16, 2017 | Bhubaneswar,India. | TBD | 2017011320170116 | z
ICDCN | IEEE International Conference on Distributed Computing and Networking | 10 | B4 | Jan 4, 2017 - Jan 7, 2017 | Hyderabad, India | Jul 22, 2016 | 2017010420170107 | 20160722
ICDCS | IEEE International Conference on Distributed Computing Systems | 78 | A1 | Jun 5, 2017 - Jun 8, 2017 | Atlanta, Georgia, USA | Dec 12, 2016 (Dec 5, 2016) | 2017060520170608 | 20161212
ICDE | IEEE International Conference on Data Engineering | 113 | A1 | Apr 19, 2017 - Apr 22, 2017 | San Diego | Oct 11, 2016 | 2017041920170422 | 20161011
ICDE_A | World Conference on Open Learning and Distance Education | 2 | B5 |  |  |  | z | z
ICDF | IFIP WG 11.9 International Conference on Digital Forensics | 1 | B5 |  |  |  | z | z
ICDIM | International Conference on Digital Information Management | 11 | B3 | Sep 12, 2017 - Sep 14, 2017 | Fukuoka, Japan | Jul 1, 2017 | 2017091220170914 | 20170701
ICDL | IEEE International Conference on Development and Learning | 15 | B3 | Sep 18, 2017 - Sep 21, 2017 | Lisbon, Portugal | Mar 30, 2017 | 2017091820170921 | 20170330
ICDL-EpiRob | IEEE Conference on Development and Learning and Epigenetic Robotics | 19 | B2 | Sep 18, 2017 - Sep 21, 2017 | Lisbon, Portugal | Mar 30, 2017 | 2017091820170921 | 20170330
ICDLE | International Conference on Distance Learning and Education | 1 | B5 | Dec 20, 2017 - Dec 22, 2017 | Barcelona, Spain | Aug 10, 2017 | 2017122020171222 | 20170810
ICDM | IEEE International Conference on Data Mining | 77 | A1 | Nov 18, 2017 - Nov 21, 2017 | NEW ORLEANS, USA | Jun 5, 2017 | 2017111820171121 | 20170605
ICDS | International Conference on Digital Society | 11 | B3 | Mar 19, 2017 - Mar 23, 2017 | Nice, France | Dec 1, 2016 | 2017031920170323 | 20161201
ICDT | International Conference on Database Theory | 27 | B1 | Apr 23, 2017 - Apr 27, 2017 | Venice, Italy | Jan 15, 2017 | 2017042320170427 | 20170115
ICDVRAT | International Conference on Disability Virtual Reality and Associated Technologies | 1 | B5 |  |  |  | z | z
ICE-B | International Conference on e-Business | 7 | B4 | Jul 26, 2017 - Jul 28, 2017 | Madrid, Spain | Mar 2, 2017 | 2017072620170728 | 20170302
ICEB | International Conference on Electronic Business | 6 | B4 |  |  |  | z | z
ICEBE | IEEE International Conference on e-Business Engineering | 20 | B2 |  |  |  | z | z
ICEC | International Conference on Electronic Commerce | 34 | B1 |  |  |  | z | z
ICECCS | IEEE International Conference on Engineering of Complex Computer Systems | 24 | B1 | Nov 5, 2017 - Nov 8, 2017 | Fukuoka, Japan | Jun 19, 2017 (Jun 5, 2017) | 2017110520171108 | 20170619
ICECE | International Conference on Engineering and Computer Education | 5 | B4 |  |  |  | z | z
ICECS | IEEE International Conference on Electronics: Circuits and Systems | 24 | B1 | Sep 13, 2017 - Sep 15, 2017 | Ottawa, Canada | Jul 5, 2017 | 2017091320170915 | 20170705
ICEC_A | IFIP International Conference on Entertainment Computing | 3 | B5 |  |  |  | z | z
ICEE | International Conference on Engineering Education | 16 | B2 | Oct 29, 2017 - Oct 31, 2017 | Boumerdes-Algeria | Jun 30, 2017 | 2017102920171031 | 20170630
ICEIMT | International Confernce on Enterprise Integration and Modelling Technology | 7 | B4 |  |  |  | z | z
ICEIS | International Conference on Enterprise Information Systems  | 24 | B1 | Apr 26, 2017 - Apr 29, 2017 | Porto | Nov 30, 2016 | 2017042620170429 | 20161130
ICEIT | International Conference on Educational and Information Technology | 3 | B5 | Mar 7, 2017 - Mar 10, 2017 | Clare College, Cambridge, UK | Dec 30, 2016 | 2017030720170310 | 20161230
ICELET | International Conference on E-Learning and E-Teaching | 2 | B5 |  |  |  | z | z
ICEMT | International Conference on Education and Management Technology | 3 | B5 | Jul 9, 2017 - Jul 11, 2017 | Singapore | May 20, 2017 | 2017070920170711 | 20170520
ICER | International Computing Education Research Workshop | 7 | B4 | Aug 17, 2017 - Aug 21, 2017 | University of Washington Tacoma, USA | Apr 14, 2017 (Apr 7, 2017) | 2017081720170821 | 20170414
ICETC | International Conference on Education Technology and Computer | 6 | B4 | Dec 20, 2017 - Dec 22, 2017 | Barcelona, Spain | Aug 5, 2017 | 2017122020171222 | 20170805
ICETE | International Conference on E-Business and Telecommunication Networks | 5 | B4 | Jul 26, 2017 - Jul 28, 2017 | Madrid, Spain | Mar 9, 2017 | 2017072620170728 | 20170309
ICFCA | International Conference on Formal Concept Analysis | 20 | B2 | Jun 12, 2017 - Jun 16, 2017 | Rennes, France | Dec 19, 2016 (Dec 12, 2016) | 2017061220170616 | 20161219
ICFCSE | International Conference on Future Computer Science and Education | 1 | B5 |  |  |  | z | z
ICFEM | International Conference on Formal Engineering Methods | 27 | B1 | Nov 13, 2017 - Nov 17, 2017 | Xi'an | May 7, 2017 (Apr 30, 2017) | 2017111320171117 | 20170507
ICFP | International Conference on Functional Programming | 53 | A2 |  |  |  | z | z
ICGEC | International Conference on Genetic and Evolutionary Computing | 3 | B5 |  |  |  | z | z
ICGeS | International Conference on Global E-Security | 2 | B5 | Aug 20, 2017 - Aug 22, 2017 | Kitakyushu City, Japan | Jun 15, 2017 | 2017082020170822 | 20170615
ICGSE | IEEE International Conference on Global Software Engineering | 20 | B2 | May 22, 2017 - May 23, 2017 | Buenos Aires, Argentina | Dec 19, 2016 | 2017052220170523 | 20161219
ICGT | International Conference on Graph Transformation | 25 | B1 |  |  |  | z | z
ICHIT | International Conference on Hybrid Information Technology | 16 | B2 |  |  |  | z | z
ICHL | International Conference on Hybrid Learning and Education | 6 | B4 |  |  |  | z | z
ICIAM | International Congress on Industrial and Applied Mathematics | 3 | B4 |  |  |  | z | z
ICIAP | International Conference on Image Analysis and Processing | 25 | B1 | Sep 11, 2017 - Sep 15, 2017 | Catania, Italy | Mar 31, 2017 | 2017091120170915 | 20170331
ICIAS | International Conference on Intelligent and Advanced Systems | 9 | B4 |  |  |  | z | z
ICICA | International Conference on Information Computing and Applications | 2 | B5 | Jan 20, 2017 - Jan 23, 2017 | University of Canberra, Australia | Nov 20, 2016 | 2017012020170123 | 20161120
ICICS | International Conference on Information and Communication Security | 33 | B1 | Apr 4, 2017 - Apr 6, 2017 | Irbid, Jordan | Dec 26, 2016 | 2017040420170406 | 20161226
ICIDS | International Conference on Interactive Digital Storytelling | 13 | B3 |  |  |  | z | z
ICIG | International Conference on Image and Graphics | 13 | B3 |  |  |  | z | z
ICINCO | International Conference on Informatics in Control Automation and Robotics | 10 | B4 | Jul 29, 2017 - Jul 31, 2017 | Madrin, Spain | Apr 18, 2017 | 2017072920170731 | 20170418
ICIP | IEEE International Conference on Image Processing | 69 | A1 | Sep 18, 2017 - Sep 20, 2017 | Beijing, China | TBD | 2017091820170920 | z
ICIQ | International Conference on Information Quality | 22 | B1 | Oct 5, 2017 - Oct 7, 2017 | Granada | Oct 5, 2017 (Apr 30, 2017) | 2017100520171007 | 20171005
ICIRA | International Conference on Intelligent Robotics and Applications | 8 | B4 |  |  |  | z | z
ICIS | International Conference on Information Systems | 47 | A2 | Oct 25, 2017 - Oct 28, 2017 | Shanghai, China | Jun 30, 2017 | 2017102520171028 | 20170630
ICISO | International Conference on Informatics and Semiotics in Organisations | 1 | B5 |  |  |  | z | z
ICISS | International Conference on Information Systems Security | 13 | B3 | Dec 16, 2017 - Dec 20, 2017 | IIT-Bombay, India | Jul 24, 2017 | 2017121620171220 | 20170724
ICIS_A | IEEE/ACIS International Conference on Computer and Information Science | 20 | B2 |  |  |  | z | z
ICIT | IEEE International Conference on Industrial Technology | 11 | B3 | Dec 27, 2017 - Dec 29, 2017 | Singapore | Oct 23, 2017 | 2017122720171229 | 20171023
ICITA | International Conference on Information Technology and Applications | 18 | B2 | Jul 1, 2017 - Jul 4, 2017 | Sydney, Australia | May 30, 2017 | 2017070120170704 | 20170530
ICITS | International Conference on Intelligent Tutoring Systems | 44 | A2 | Jun 25, 2017 - Jun 27, 2017 | Beijing,China | Apr 20, 2017 | 2017062520170627 | 20170420
ICIW | International Conference on Internet and Web Applications and Services | 15 | B3 | Jun 25, 2017 - Jun 29, 2017 | Venice, Italy | Feb 5, 2017 | 2017062520170629 | 20170205
ICL | International Conference on Interactive Collaborative Learning | 2 | B5 | Jun 27, 2017 - Jun 29, 2017 | Nottingham, UK | Apr 5, 2017 | 2017062720170629 | 20170405
ICLP | International Conference on Logic Programming | 35 | B1 | Aug 28, 2017 - Sep 1, 2017 | Melbourne, Australia | Mar 17, 2017 (Mar 10, 2017) | 2017082820170901 | 20170317
ICLS | International Conference of the Learning Sciences | 24 | B1 |  |  |  | z | z
ICMA | International Conference on Manufacturing Automation | 2 | B5 | Oct 22, 2017 - Oct 23, 2017 | Guraiger, Abha 62529 | Jul 25, 2017 | 2017102220171023 | 20170725
ICMC | International Computer Music Conference | 32 | B1 | Oct 16, 2017 - Oct 20, 2017 | Shanghai, China | Mar 24, 2017 | 2017101620171020 | 20170324
ICMCS | IEEE International Conference on Multimedia Computing and Systems | 6 | B3 |  |  |  | z | z
ICME | IEEE International Conference on Multimedia and Expo | 66 | A1 | Jul 10, 2017 - Jul 14, 2017 | Hong Kong | Dec 2, 2016 (Nov 25, 2016) | 2017071020170714 | 20161202
ICMI | International Conference on Multimodal Interfaces | 46 | A2 | Nov 13, 2017 - Nov 17, 2017 | Glasgow, Scotland | May 12, 2017 | 2017111320171117 | 20170512
ICMIC | International Conference on Modelling Identification and Control | 3 | B5 |  |  |  | z | z
ICML | International Conference on Machine Learning | 124 | A1 | Jun 20, 2017 - Jun 22, 2017 | Sydney, Australia | Feb 5, 2017 | 2017062020170622 | 20170205
ICMLA | International Conference on Machine Learning and Applications | 17 | B2 | Dec 18, 2017 - Dec 21, 2017 | CANCUN, MEXICO | Jul 6, 2017 | 2017121820171221 | 20170706
ICMLC | International Conference on Machine Learning and Cybernetics | 6 | B4 | Feb 24, 2017 - Feb 26, 2017 | Singapore | Dec 30, 2016 | 2017022420170226 | 20161230
ICMPC | International Conference of Music Perception and Cognition | 9 | B4 |  |  |  | z | z
ICMR | ACM International Conference on Multimedia Retrieval | 51 | A2 | Jun 6, 2017 - Jun 9, 2017 | Bucharest, Romania | Jan 27, 2017 | 2017060620170609 | 20170127
ICMSAO | International Conferecen on Modeling Simulation and Applied Optimization | 3 | B5 |  |  |  | z | z
ICMT | International Conference on Model Transformation | 17 | B2 | Jul 17, 2017 - Jul 18, 2017 | Marburg, Germany | Feb 24, 2017 (Feb 17, 2017) | 2017071720170718 | 20170224
ICMTS | IEEE International Conference on Microelectronics Test Structures | 14 | B3 | Jan 3, 2017 - Jan 5, 2017 | Rome, Italy | Nov 25, 2016 | 2017010320170105 | 20161125
ICN | International Conference on Networking | 24 | B1 | Apr 23, 2017 - Apr 27, 2017 | Venice, Italy | Jan 15, 2017 | 2017042320170427 | 20170115
ICNC | International Conference on Natural Computation | 5 | B4 | Jan 26, 2017 - Jan 29, 2017 | Santa Clara Marriott, Santa Clara, CA | Jul 5, 2016 | 2017012620170129 | 20160705
ICNCC | International Conference on Network Communication and Computer | 1 | B5 | Dec 8, 2017 - Dec 10, 2017 | Kunming, China | Aug 5, 2017 | 2017120820171210 | 20170805
ICNP | IEEE International Conference on Network Protocols | 65 | A1 | Oct 10, 2017 - Oct 13, 2017 | Toronto, Canada | May 15, 2017 (May 8, 2017) | 2017101020171013 | 20170515
ICNS | International conference on Networking and Services | 14 | B3 | Dec 8, 2017 - Dec 10, 2017 | Kunming, China | Aug 5, 2017 | 2017120820171210 | 20170805
ICNSC | IEEE Intl Networking Sensing and Control | 12 | B3 | May 16, 2017 - May 18, 2017 | Calabria (Southern Italy) | Jan 15, 2017 | 2017051620170518 | 20170115
ICoFCS | International Conference on Forensic Computer Science | 2 | B5 |  |  |  | z | z
ICOIN | International Conference on Information Networking | 22 | B1 | Jan 11, 2017 - Jan 13, 2017 | Da Nang, Vietnam | Sep 26, 2016 | 2017011120170113 | 20160926
ICOMP | International Conference on Internet Computing | 20 | B2 |  |  |  | z | z
ICON | IEEE International Conference on Networks | 10 | B3 | Dec 18, 2017 - Dec 21, 2017 | Jadavpur University, Kolkata, India | Aug 27, 2017 | 2017121820171221 | 20170827
ICONIP | International Conference on Neural Information Processing | 23 | B1 | Oct 14, 2017 - Oct 18, 2017 | Guangzhou, China | Jun 10, 2017 | 2017101420171018 | 20170610
ICONS | International Conference on Systems | 12 | B3 | Apr 23, 2017 - Apr 27, 2017 | Venice, Italy | Jan 15, 2017 | 2017042320170427 | 20170115
ICORD | International Conference on Operational Research for Development | 1 | B5 |  |  |  | z | z
ICORE | International Conference on Reputation | 4 | B4 |  |  |  | z | z
ICOTA | International Conference on Optimization: Techniques And Applications | 3 | B5 |  |  |  | z | z
ICPADS | International Conference on Parallel and Distributed Systems | 28 | B1 | Dec 15, 2017 - Dec 17, 2017 | Shenzhen, China | Jul 1, 2017 | 2017121520171217 | 20170701
ICPC | IEEE International Conference on Program Comprehension | 41 | A2 | May 20, 2017 - May 28, 2017 | Buenos Aires - Argentina | Dec 19, 2016 (Dec 12, 2016) | 2017052020170528 | 20161219
ICPP | International Conference on Parallel Processing | 42 | A2 | Aug 14, 2017 - Aug 17, 2017 | Bristol, UK | Feb 27, 2017 | 2017081420170817 | 20170227
ICPR | International Conference on Pattern Recognition | 81 | A1 |  |  |  | z | z
ICPS | IEEE/ACS International Conference on Pervasive Services | 20 | B2 | Nov 18, 2017 - Nov 19, 2017 | Dubai, UAE | Nov 1, 2017 (Oct 1, 2017) | 2017111820171119 | 20171101
ICRA | IEEE International Conference on Robotics and Automation | 116 | A1 | May 29, 2017 - Jun 3, 2017 | Singapore | Sep 10, 2016 | 2017052920170603 | 20160910
ICS | ACM International Conference on Supercomputing | 52 | A2 | Jun 14, 2017 - Jun 16, 2017 | Chicago, USA | Jan 18, 2017 (Jan 11, 2017) | 2017061420170616 | 20170118
ICSC | IEEE International Conference on Semantic Computing | 21 | B2 | Jan 30, 2017 - Feb 1, 2017 | San Diego, CA, USA | Oct 7, 2016 | 2017013020170201 | 20161007
ICSC_A | International Computer Science Conference | 16 | B2 |  |  |  | z | z
ICSE | ACM/IEEE International Conference on Software Engineering | 117 | A1 | May 20, 2017 - May 28, 2017 | Buenos Aires, Argentina | Aug 26, 2016 | 2017052020170528 | 20160826
ICSEA | International Conference on Software Engineering Advances | 13 | B3 | Oct 28, 2017 - Oct 30, 2017 | Los Angeles, USA | Jul 10, 2017 | 2017102820171030 | 20170710
ICSM | IEEE International Conference on Software Maintenance | 53 | A2 |  |  |  | z | z
ICSMA | International Conference on Smart Manufacturing Application | 7 | B4 |  |  |  | z | z
ICSNC | International Conference on Systems and Networks Communication | 13 | B3 | Oct 8, 2017 - Oct 12, 2017 | Athens, Greece | Jun 20, 2017 | 2017100820171012 | 20170620
ICSOB | International Conference on Software Business | 3 | B5 | Jun 12, 2017 - Jun 13, 2017 | Essen, Germany | Mar 3, 2017 (Feb 24, 2017) | 2017061220170613 | 20170303
ICSOC | International Conference on Service Oriented Computing | 47 | A2 | Nov 13, 2017 - Nov 16, 2017 | Malaga, Spain | Jun 6, 2017 | 2017111320171116 | 20170606
ICSOFT | International Conference on Software and Data Technologies | 10 | B4 | Jul 26, 2017 - Jul 28, 2017 | Madrid, Spain | Mar 2, 2017 | 2017072620170728 | 20170302
ICSP | International Conference on Stochastic Programming | 12 | B3 | Mar 13, 2017 - Mar 15, 2017 | Bangkok, Thailand | Feb 1, 2017 | 2017031320170315 | 20170201
ICSR | IEEE International Conference on Software Reuse | 23 | B1 | Sep 28, 2017 - Sep 29, 2017 | Berlin, Germany | Jun 15, 2017 | 2017092820170929 | 20170615
ICSSEA | International Conference on Software and Systems Engineering and their Applications | 5 | B4 |  |  |  | z | z
ICSSP | International Conference on on Software and Systems Process | 3 | B5 |  |  |  | z | z
ICST | International Conference on Software Testing Verification and Validation | 21 | B2 | Mar 13, 2017 - Mar 18, 2017 | Tokyo, Japan | Sep 23, 2016 | 2017031320170318 | 20160923
ICSTW | IEEE International Conference on Software Testing Verification and Validation Workshop | 14 | B3 |  |  |  | z | z
ICT | International Conference on Telecommunications | 17 | B2 | Dec 11, 2017 - Dec 13, 2017 | Münster, Germany | Jul 15, 2017 | 2017121120171213 | 20170715
ICTAC | International Colloquium on Theoretical Aspects of Computing | 15 | B3 | Oct 23, 2017 - Oct 27, 2017 | Hanoi, Vietnam | Apr 22, 2017 | 2017102320171027 | 20170422
ICTAI | IEEE International Conference on Tools with Artificial Intelligence | 38 | A2 | Nov 6, 2017 - Nov 8, 2017 | Boston, MA, USA | Jul 15, 2017 | 2017110620171108 | 20170715
ICTEL | International Conference on Technology-Enhanced Learning | 3 | B5 | Jul 26, 2017 - Jul 27, 2017 | Barcelona, Spain | Jul 24, 2017 | 2017072620170727 | 20170724
ICTERI | International Conference on ICT in Education Research and Industrial Applications | 1 | B5 |  |  |  | z | z
ICTL | International Conferece on Temporal Logic | 3 | B5 |  |  |  | z | z
ICTON | International Conference on Transparent Optical Networks | 9 | B4 |  |  |  | z | z
ICTSS | IFIP International Conference on Testing Software and Systems | 24 | B1 | Oct 9, 2017 - Oct 11, 2017 | St-Petersburg, Russia | May 30, 2017 (May 23, 2017) | 2017100920171011 | 20170530
ICUIMC | International Conference on Ubiquitous Information Management and Communication | 10 | B4 |  |  |  | z | z
ICUMT | Ultra Modern Telecommunications | 10 | B4 |  |  |  | z | z
ICVES | IEEE International Conference on Vehicular Electronics and Safety | 8 | B4 | Jun 27, 2017 - Jun 28, 2017 | Vienna, Austria | Feb 26, 2017 | 2017062720170628 | 20170226
ICVS | International Conference on Computer Vision Systems | 26 | B1 | Jul 10, 2017 - Jul 13, 2017 | Shenzhen, China | Mar 30, 2017 | 2017071020170713 | 20170330
ICVS_A | International Conference on Virtual Storytelling | 16 | B2 |  |  |  | z | z
ICWE | International Conference on Web Engineering | 28 | B1 | Jun 5, 2017 - Jun 8, 2017 | Rome, Italy | Jan 22, 2017 | 2017060520170608 | 20170122
ICWET | International Conference & Workshop on Emerging Trends in Technology | 5 | B4 |  |  |  | z | z
ICWI | International Conference WWW/Internet | 16 | B2 |  |  |  | z | z
ICWL | International Conference on Web-based Learning | 14 | B3 | Sep 20, 2017 - Sep 22, 2017 | Cape Town | Apr 18, 2017 (Apr 11, 2017) | 2017092020170922 | 20170418
ICWMC | International Conference on Wireless and Mobile Communications | 14 | B3 | Jul 23, 2017 - Jul 27, 2017 | Nice, France | Apr 5, 2017 | 2017072320170727 | 20170405
ICWN | International Conference on Wireless Networks | 17 | B2 |  |  |  | z | z
ICWS | IEEE International Conference on Web Services | 58 | A1 | Jun 25, 2017 - Jun 30, 2017 | Honolulu, HI, USA | Jan 12, 2017 | 2017062520170630 | 20170112
ICWSM | AAAI International Conference on Weblogs and Social Media | 26 | B1 | May 16, 2017 - May 18, 2017 | Montreal, QC, Canada | Jan 13, 2017 (Jan 6, 2017) | 2017051620170518 | 20170113
IDA | International Symposium on Intelligent Data Analysis | 21 | B2 | Oct 26, 2017 - Oct 28, 2017 | London, UK | May 19, 2017 | 2017102620171028 | 20170519
IDAACS | Intelligent Data Acquisition and Advanced Computing Systems Technology and Applications | 9 | B4 | Sep 21, 2017 - Sep 23, 2017 | Bucharest, Romania | Mar 22, 2017 | 2017092120170923 | 20170322
IDC | International Conference on Interaction Design and Children | 16 | B2 | Oct 11, 2017 - Oct 13, 2017 | Belgrade | Apr 4, 2017 | 2017101120171013 | 20170404
IDC_A | IEEE Conference on Information Decision and Control | 9 | B4 |  |  |  | z | z
IDEAL | International Conference on Intelligent Data Engineering and Automated Learning | 20 | B2 | Oct 30, 2017 - Nov 1, 2017 | Guilin, China | Jun 30, 2017 | 2017103020171101 | 20170630
IDEAS_A | International Database Engineering and Applications Symposium | 28 | B1 |  |  |  | z | z
IE | International Conference on Intelligent Environments | 5 | B4 | Aug 21, 2017 - Aug 25, 2017 | Seoul, Korea | Mar 5, 2017 | 2017082120170825 | 20170305
IECON | Annual Conference of the IEEE Industrial Electronics Society | 36 | B1 | Nov 5, 2017 - Nov 8, 2017 | Beijing | Apr 17, 2017 | 2017110520171108 | 20170417
IEEEIS | IEEE Conference On Intelligent Systems | 22 | B1 |  |  |  | z | z
IEEEIT | IEEE Symposium on Information Theory | 63 | A1 |  |  |  | z | z
IEEERFID | IEEE IEEE International Conference on RFID | 12 | B3 |  |  |  | z | z
IESA | International Conference on Interoperability for Enterprise Software and Applications | 11 | B3 |  |  |  | z | z
IESS | International Conference on Exploring Services Science | 5 | B4 |  |  |  | z | z
IESS_A | International Embedded Systems Symposium | 1 | B5 |  |  |  | z | z
IFAC | Triennial Event of International Federation of Automatic Control | 12 | B2 | Jul 9, 2017 - Jul 14, 2017 | Toulouse, France | Nov 11, 2016 | 2017070920170714 | 20161111
IFIPAI | IFIP Artificial Intelligence | 3 | B5 |  |  |  | z | z
IFL | International Symposium on Implementation and Application of Functional Languages | 2 | B3 |  |  |  | z | z
IFM | International Conference on Integrated Formal Methods | 24 | B1 | Jul 19, 2017 - Jul 20, 2017 | Marburg, Germany | Feb 24, 2017 (Feb 17, 2017) | 2017071920170720 | 20170224
IFSA | International Fuzzy Systems Association World Congress | 26 | B1 | Jun 27, 2017 - Jun 30, 2017 | Otsu, JAPAN | Jan 15, 2017 | 2017062720170630 | 20170115
IGARSS | IEEE International Geoscience and Remote Sensing Symposium | 14 | B1 |  |  |  | z | z
IGCC | International Green Computing Conference | 9 | B4 |  |  |  | z | z
IGIC | IEEE International Games Innovation Conference | 1 | B5 |  |  |  | z | z
IGIP | IGIP International Symposium on Engineering Education | 4 | B4 |  |  |  | z | z
IH | Information Hiding Workshop | 26 | B1 |  |  |  | z | z
IHC | Simpósio Brasileiro sobre Fatores Humanos em Sistemas Computacionais | 10 | B4 |  |  |  | z | z
IHI | IHI ACM SIGHIT International Health Informatics Symposium | 6 | B4 |  |  |  | z | z
IITSI | International Symposium on Intelligent Information Technology and Security Informatics | 5 | B4 |  |  |  | z | z
IIWAS | Information Integration and Web-based Applications and Services | 12 | B3 |  |  |  | z | z
IJCAI | International Joint Conference on Artificial Intelligence | 90 | A1 | Aug 19, 2017 - Aug 25, 2017 | Melbourne, Australia | Feb 19, 2017 | 2017081920170825 | 20170219
IJCAR | International Joint Conference on Automated Reasoning | 36 | B1 |  |  |  | z | z
IJCBS | International Joint Conference on Bioinformatics Systems Biology and Intelligent Computing | 5 | B4 |  |  |  | z | z
IJCNLP | International Joint Conference on Natural Language Processing | 20 | B2 | Nov 27, 2017 - Dec 1, 2017 | Taipei, Taiwan | Jun 30, 2017 | 2017112720171201 | 20170630
IJCNN | IEEE International Joint Conference on Neural Networks | 46 | A2 | May 14, 2017 - May 19, 2017 | Anchorage, Alaska | Nov 15, 2016 | 2017051420170519 | 20161115
IKE | International Conference on Information and Knowledge Engineering | 10 | B4 |  |  |  | z | z
ILP | International Conference on Inductive Logic Programming | 27 | B1 | Sep 4, 2017 - Sep 6, 2017 | Orléans - France | May 5, 2017 (Apr 28, 2017) | 2017090420170906 | 20170505
IM | IFIP/IEEE International Symposium on Integrated Network Management | 32 | B1 | May 8, 2017 - May 12, 2017 | Lisbon, Portugal | Sep 4, 2016 | 2017050820170512 | 20160904
IMAGAPP/IVAPP | International Conference on Imaging Theory and Applications and International Conference on Information Visualization Theory and Applications | 2 | B5 | Feb 27, 2017 - Mar 1, 2017 | Porto, Portugal | Oct 20, 2016 | 2017022720170301 | 20161020
IMC | ACM SIGCOMM Internet Measurement Conference | 93 | A1 | Nov 1, 2017 - Nov 3, 2017 | London, UK | May 18, 2017 (May 11, 2017) | 2017110120171103 | 20170518
IMCSIT | International Multiconference on Computer Science and Information Technology | 10 | B4 |  |  |  | z | z
IMF | International Conference on IT Security Incident Management and IT Forensics | 5 | B4 |  |  |  | z | z
IMR | International Meshing Roundtable Conference | 28 | B1 |  |  |  | z | z
IMS | Advanced Information Management and Service | 4 | B4 |  |  |  | z | z
IMSA | International Conference on Internet and Multimedia Systems and Applications | 10 | B4 |  |  |  | z | z
IMS_A | IFAC Workshop on Intelligent Manufacturing Systems | 4 | B4 |  |  |  | z | z
INA-OCMC | Interconnection Network Architectures: On-Chip Multi-Chip | 3 | B5 |  |  |  | z | z
INAP | Joint Conference on Application of Declarative Programming and Knowledge Management | 7 | B4 |  |  |  | z | z
InCoB | International Conference on Bioinformatics | 1 | B5 |  |  |  | z | z
INDIN | IEEE International Conference on Industrial Informatics | 12 | B3 | Jul 24, 2017 - Jul 26, 2017 | Emden, Germany | Apr 14, 2017 | 2017072420170726 | 20170414
INDOCRYPT | International Conference on Cryptology in India | 30 | B1 |  |  |  | z | z
INEX | International Workshop of the Inititative for the Evaluation of XML Retrieval | 8 | B4 |  |  |  | z | z
INFOCOM | Annual Joint Conference of the IEEE Computer and Communications Societies | 204 | A1 | May 1, 2017 - May 4, 2017 | Atlanta, GA | Jul 29, 2016 (Jul 22, 2016) | 2017050120170504 | 20160729
INFORMS | Joint International Meeting | 3 | B5 |  |  |  | z | z
INLG | International Natural Language Generation Conference | 21 | B2 | Sep 4, 2017 - Sep 7, 2017 | Santiago de Compostela, Spain | May 1, 2017 | 2017090420170907 | 20170501
INOC | International Network Optimization Conference | 10 | B3 |  |  |  | z | z
InSITE | Informing Science and IT Education Conference | 6 | B4 |  |  |  | z | z
INTED | International Technology Education and Development Conference | 4 | B4 | Mar 6, 2017 - Mar 8, 2017 | Valencia | Dec 1, 2016 | 2017030620170308 | 20161201
INTERACT | IFIP TC13 International Conference on Human-Computer Interaction | 30 | B1 | Sep 25, 2017 - Sep 29, 2017 | Mumbai India | Jun 27, 2017 | 2017092520170929 | 20170627
Interspeech | International Conference on Spoken Language Processing | 80 | A1 | Aug 20, 2017 - Aug 24, 2017 | Stockholm, Sweden | Mar 21, 2017 (Mar 14, 2017) | 2017082020170824 | 20170321
IOLTS | IEEE International On-Line Testing Symposium | 25 | B1 |  |  |  | z | z
IPAW  | International Provenance and Annotation Workshop | 20 | B2 |  |  |  | z | z
IPCCC | IEEE International Performance Computing and Communications Conference | 24 | B1 | Dec 10, 2017 - Dec 12, 2017 | San Diego, CA | Aug 8, 2017 (Jul 25, 2017) | 2017121020171212 | 20170808
IPCO | Conference on Integer Programming and Combinatorial Optimization | 26 | B1 |  |  |  | z | z
IPCV | International Conference on Image Processing Computer Vision and Pattern Recognition | 10 | B4 |  |  |  | z | z
IPDPS | IEEE International Parallel and Distributed Processing Symposium | 73 | A1 | May 29, 2017 - Jun 2, 2017 | Orlando, Florida USA | Oct 23, 2016 (Oct 18, 2016) | 2017052920170602 | 20161023
IPDS | International Computer Performance and Dependability Symposium | 7 | B4 |  |  |  | z | z
IPEC | International Symposium on Parameterized and Exact Computation | 20 | B2 | Sep 6, 2017 - Sep 8, 2017 | Vienna | Jun 28, 2017 (Jun 25, 2017) | 2017090620170908 | 20170628
IPMU | International Conference on Information Processing and Management of Uncertainty in Knowledge-Based Systems | 16 | B2 |  |  |  | z | z
IPOM | IEEE International Workshop on IP Operations and Management | 9 | B3 |  |  |  | z | z
IPPS | International Parallel Processing Symposium | 71 | A1 |  |  |  | z | z
IPSN | International Symposium on Information Processing in Sensor Networks | 88 | A1 | Apr 18, 2017 - Apr 21, 2017 | Pittsburgh, Pennsylvania, USA | Oct 13, 2016 (Oct 6, 2016) | 2017041820170421 | 20161013
IPT | Workshop on Immersive Projection Technology | 9 | B3 |  |  |  | z | z
IPTPS | International Workshop on Peer-to-Peer Systems | 63 | A1 |  |  |  | z | z
IQIS | Information Quality in Information Systems | 15 | B3 |  |  |  | z | z
IR | Research and Development in Information Retrieval | 2 | B5 | Sep 7, 2017 - Sep 7, 2017 | Copenhagen, Denmark | Jun 2, 2017 | 2017090720170907 | 20170602
IRI | IEEE International Conference on Information Reuse and Integration | 21 | B2 | Aug 4, 2017 - Aug 6, 2017 | San Diego, CA, USA | Apr 13, 2017 | 2017080420170806 | 20170413
IRMA | Information Resources Management Association International Conference | 6 | B4 |  |  |  | z | z
IROS | IEEE/RJS International Conference on Intelligent Robots and Systems | 58 | A1 | Sep 24, 2017 - Sep 28, 2017 | Vancouver, Canada | Mar 1, 2017 | 2017092420170928 | 20170301
IS | IADIS International Conference Information Systems | 4 | B4 | Aug 13, 2017 - Aug 17, 2017 | Halifax, Nova Scotia, Canada | Jun 28, 2017 | 2017081320170817 | 20170628
ISAAC | International Symposium on Algorithms and Computation | 27 | B1 | Dec 9, 2017 - Dec 12, 2017 | Phuket, Thailand  | Jun 29, 2017 | 2017120920171212 | 20170629
ISADS | International Symposium on Autonomous Decentralized Systems | 25 | B1 |  |  |  | z | z
ISAM | IEEE International Symposium on Assembly and Manufacturing | 11 | B3 |  |  |  | z | z
ISBI | IEEE International Symposium on Biomedical Imaging: From Nano to Macro | 34 | B1 | Apr 18, 2017 - Apr 21, 2017 | Melbourne, Sydney | Oct 24, 2016 | 2017041820170421 | 20161024
ISBRA | International Symposium on Bioinformatics Research and Applications | 11 | B3 | May 30, 2017 - Jun 1, 2017 | Honolulu, Hawaii, USA | Feb 10, 2017 | 2017053020170601 | 20170210
ISC | Information Security Conference | 37 | A2 | Jun 22, 2017 - Jun 22, 2017 | Frankfurt, Germany | Apr 21, 2107 | 2017062220170622 | 21070421
ISCA | ACM International Symposium on Computer Architecture | 110 | A1 | Jun 25, 2017 - Jun 28, 2017 | Toronto, Canada | Nov 18, 2016 (Nov 11, 2016) | 2017062520170628 | 20161118
ISCAS | IEEE International Symposium on Circuits and Systems | 159 | A1 | May 28, 2017 - May 31, 2017 | Baltimore, MD, USA | Nov 11, 2016 | 2017052820170531 | 20161111
ISCC | IEEE Symposium on Computers and Communications | 43 | A2 | Jul 3, 2017 - Jul 6, 2017 | Heraklion, Crete, Greece | Jan 30, 2017 | 2017070320170706 | 20170130
ISCIS | International Symposium on Computer and Information Sciences | 18 | B2 |  |  |  | z | z
ISCO | International Symposium on Combinatorial Optimization | 3 | B5 | Jan 5, 2017 - Jan 6, 2017 | Coimbatore | Oct 5, 2016 | 2017010520170106 | 20161005
ISCRAM | Intelligent Human Computer Systems for Crisis Response and Management | 9 | B4 | Oct 18, 2017 - Oct 20, 2017 | Xanthi Greece | Jun 12, 2017 | 2017101820171020 | 20170612
ISC_A | International Supercomputing Conference | 2 | B5 |  |  |  | z | z
ISD | International Conference on Information Systems Development | 3 | B4 | Sep 6, 2017 - Sep 7, 2017 | UCLan Cyprus | Apr 15, 2017 | 2017090620170907 | 20170415
ISDA | International Conference on Intelligent Systems Design and Applications | 16 | B2 |  |  |  | z | z
ISDPE | International Symposium on Data Privacy and E-commerce | 7 | B4 |  |  |  | z | z
ISECON | Information Systems Education Conference | 6 | B4 |  |  |  | z | z
ISECS | International Symposium Electronic Commerce and Security | 10 | B4 |  |  |  | z | z
ISER | International Symposium on Experimental Robotics | 7 | B3 |  |  |  | z | z
ISI | IEEE Intelligence and Security Informatics | 27 | B1 | Nov 24, 2017 - Nov 26, 2017 | Tokyo, Japan | Jul 5, 2017 | 2017112420171126 | 20170705
ISIC | IEEE International Symposium on Intelligent Control | 15 | B3 |  |  |  | z | z
ISICA | International Symposium on Intelligence Computation and Applications | 9 | B4 |  |  |  | z | z
ISIE | IEEE International Symposium on Industrial Electronics | 32 | B1 | Jun 19, 2017 - Jun 21, 2017 | Edinburgh, Scotland | Feb 3, 2017 | 2017061920170621 | 20170203
ISIMP | International Symposium on Intelligent Multimedia Video and Speech Processing | 16 | B2 |  |  |  | z | z
ISIPTA | International Symposium on Imprecise Probabilities: Theories and Applications | 21 | B2 |  |  |  | z | z
ISIT | IEEE International Symposium on Information Theory | 31 | B1 |  |  |  | z | z
ISITA | IEEE International Symposium on Information Theory and Its Applications | 14 | B3 |  |  |  | z | z
ISLPED | International Symposium on Low Power Electronics and Design | 66 | A1 | Jul 24, 2017 - Jul 26, 2017 | Taipei, Taiwan | Mar 6, 2017 (Feb 27, 2017) | 2017072420170726 | 20170306
ISM | IEEE International Symposium on Multimedia | 19 | B2 | Dec 11, 2017 - Dec 13, 2017 | Taichung, Taiwan | Jul 15, 2017 | 2017121120171213 | 20170715
ISMAR | International Symposium on Mixed and Augmented Reality | 48 | A2 | Oct 9, 2017 - Oct 13, 2017 | Nantes (France) | Mar 15, 2017 | 2017100920171013 | 20170315
ISMB | Intelligent Systems in Molecular Biology | 91 | A1 | Jul 21, 2017 - Jul 25, 2017 | TBD | Jan 26, 2017 | 2017072120170725 | 20170126
ISMIR | International Conference on Music Information Retrieval | 61 | A1 | Oct 23, 2017 - Oct 28, 2017 | Suzhou, China | Apr 28, 2017 (Apr 21, 2017) | 2017102320171028 | 20170428
ISMIS | International Syposium on Methodologies for Intelligent Systems | 21 | B2 | Jun 26, 2017 - Jun 29, 2017 | Warsaw, Poland | Feb 9, 2017 | 2017062620170629 | 20170209
ISMM | International Symposium on Mathematical Morphology | 25 | B1 | Jun 18, 2017 - Jun 23, 2017 | Barcelona, Spain | Mar 5, 2017 | 2017061820170623 | 20170305
ISMM_A | International Symposium on Memory Management | 25 | B1 |  |  |  | z | z
ISMP | International Symposium on Mathematical Programming | 4 | B4 |  |  |  | z | z
ISMVL | IEEE International Symposium on Multiple-Valued Logic | 21 | B2 |  |  |  | z | z
ISoLA | International Symposium on Leveraging Applications of Formal Methods Verification and Validation | 7 | B4 |  |  |  | z | z
ISORC | IEEE International Symposium on Object-Oriented Real-Time Distributed Computing | 33 | B1 |  |  |  | z | z
ISPA | International Symposium on Parallel and Distributed Processing and Applications | 13 | B3 | Sep 18, 2017 - Sep 20, 2017 | Ljubljana, Slovenia | May 7, 2017 | 2017091820170920 | 20170507
ISPASS | IEEE International Symposium on Performance Analysis of Systems and Software | 32 | B1 | Apr 23, 2017 - Apr 25, 2017 |  San Francisco Bay Area, California | Oct 14, 2016 (Oct 7, 2016) | 2017042320170425 | 20161014
ISPD | ACM International Symposium on Physical Design | 42 | A2 |  |  |  | z | z
ISPDC | International Symposium on Parallel and Distributed Computing | 17 | B2 | Jul 3, 2017 - Jul 6, 2017 | Innsbruck, Austria | Jan 29, 2017 | 2017070320170706 | 20170129
ISQED | IEEE International Symposium on Quality Electronic Design | 34 | B1 |  |  |  | z | z
ISRA | International Symposium on Robotics and Automation | 24 | B1 |  |  |  | z | z
ISRE | IEEE International Symposium on Requirements Engineering | 19 | B2 |  |  |  | z | z
ISRR | International Symposium of Robotics Research | 3 | B4 | Dec 11, 2017 - Dec 14, 2017 | Puerto Varas, Chile | Jun 15, 2017 | 2017121120171214 | 20170615
ISSADS | IEEE International Symposium and School on Advance Distributed Systems | 7 | B4 |  |  |  | z | z
ISSCC | IEEE International Solid-State Circuits Conference | 28 | B1 | Feb 5, 2017 - Feb 9, 2017 | San Francisco | Sep 12, 2017 | 2017020520170209 | 20170912
ISSCS | International Symposium on Signals Circuits and Systems | 12 | B3 |  |  |  | z | z
ISSDQ | International Symposium on Spatial Data Quality | 6 | B4 |  |  |  | z | z
ISSPA | IEEE International Symposium on Signal Processing and its Applications | 8 | B3 |  |  |  | z | z
ISSRE | IEEE International Symposium on Software Reliability Engineering | 41 | A2 | Oct 23, 2017 - Oct 26, 2017 | Toulouse, France | May 5, 2017 (Apr 28, 2017) | 2017102320171026 | 20170505
ISSTA | International Symposium on Software Testing and Analysis | 52 | A2 | Jul 9, 2017 - Jul 13, 2017 | Santa Barbara, California, USA | Feb 3, 2017 | 2017070920170713 | 20170203
IST | IEEE International Workshop on Imaging Systems and Techniques | 6 | B4 | Nov 30, 2017 - Dec 1, 2017 | Università degli Studi di Torino | Aug 31, 2017 | 2017113020171201 | 20170831
ISTA | International Conference on Information Systems Technology and its Application | 11 | B3 | Oct 14, 2017 - Oct 14, 2017 | Breno | Jun 10, 2017 | 2017101420171014 | 20170610
ISVLSI | IEEE Computer Society Annual Symposium on VLSI | 29 | B1 | Jul 3, 2017 - Jul 5, 2017 | Bochum | Mar 10, 2017 | 2017070320170705 | 20170310
ISWC | International Semantic Web Conference | 89 | A1 | Oct 21, 2017 - Oct 25, 2017 | Vienna, Austria | May 15, 2017 (May 8, 2017) | 2017102120171025 | 20170515
ISWC_A | IEEE International Symposium on Wearable Computing | 47 | A2 |  |  |  | z | z
ISWPC | Wireless Pervasive Computing | 22 | B1 |  |  |  | z | z
ISWSA | International Conference on Intelligent Semantic Web-Services and Applications | 2 | B5 |  |  |  | z | z
ITAT | Conference on Theory and Practice of Information Technologies | 5 | B4 |  |  |  | z | z
ITBAM | International Conference on Information Technology in Bio- and Medical Informatics | 3 | B5 |  |  |  | z | z
ITC | International Test Conference | 62 | A1 | Sep 8, 2017 - Sep 8, 2017 | Genua | May 1, 2017 | 2017090820170908 | 20170501
ITEM | Conference on Information Technology in Educational Management | 4 | B4 |  |  |  | z | z
ITHE | International Conference on Information Technology Based Higher | 5 | B4 |  |  |  | z | z
ITHET | International Conference on Information Technology Based Higher Education and Training | 13 | B3 |  |  |  | z | z
ITiCSE | Annual Conference on Integrating Technology into Computer Science Education | 35 | B1 | Jul 1, 2017 - Jul 5, 2017 | University of Bologna, Italy | Jan 15, 2017 | 2017070120170705 | 20170115
ITNG | International Conference on Information Technology New Generations | 22 | B1 | Apr 10, 2017 - Apr 12, 2017 | Tuscany Suites , Las Vegas, Nevada, USA | Nov 25, 2016 | 2017041020170412 | 20161125
ITP | Interactive Theorem Proving continues TPHOL) | 10 | B4 |  |  |  | z | z
ITRE | International Conference on Information Technology: Research and Education | 16 | B2 |  |  |  | z | z
ITS | International Conference on Interactive Tabletops and Surfaces | 17 | B2 | Oct 16, 2017 - Oct 20, 2017 | Mielparque Yokohama, Yokohama, Japan | Apr 21, 2017 | 2017101620171020 | 20170421
ITSC | Conference on Intelligent Transportation Systems | 25 | B1 | Oct 16, 2017 - Oct 20, 2017 | Mielparque Yokohama, Yokohama, Japan | Apr 21, 2017 | 2017101620171020 | 20170421
ITS_A | International Telecommunications Symposium | 10 | B4 |  |  |  | z | z
IUI | International Conference on Intelligent User Interfaces | 62 | A1 | Mar 13, 2017 - Mar 16, 2017 | Limassol, Cyprus | Oct 14, 2016 (Oct 9, 2016) | 2017031320170316 | 20161014
IUKM | Integrated Uncertainty in Knowledge Modelling and Decision Making | 1 | B5 |  |  |  | z | z
IV | International Conference on Information Visualization | 32 | B1 | Jul 20, 2017 - Jul 21, 2017 | Vienna, Austria | Apr 3, 2017 | 2017072020170721 | 20170403
IVA | International Conference on Intelligent Virtual Agents | 30 | B1 | Aug 27, 2017 - Aug 30, 2017 | Stockholm | Apr 14, 2017 | 2017082720170830 | 20170414
IVCNZ | Image and Vision Computing Conference | 10 | B4 |  |  |  | z | z
IV_A | Intelligent Vehicles Conference | 4 | B4 |  |  |  | z | z
IWANN | International Work-Conference on Artificial and Natural Neural Networks | 20 | B2 | Jun 14, 2017 - Jun 16, 2017 | Cádiz, Spain | Jan 25, 2017 | 2017061420170616 | 20170125
IWCC | IEEE International Workshop on Cluster Computing | 8 | B4 | Aug 29, 2017 - Sep 1, 2017 | Reggio Calabria, Italy | May 14, 2017 | 2017082920170901 | 20170514
IWCIA | International Workshop on Combinatorial Image Analysis | 10 | B4 | Jun 19, 2017 - Jun 21, 2017 | Plovdiv, Bulgaria | Jan 15, 2017 | 2017061920170621 | 20170115
IWCMC | ACM International Wireless Communications and Mobile Computing Conference | 25 | B1 | Jun 26, 2017 - Jun 30, 2017 | Valencia (Spain) | Jan 10, 2017 | 2017062620170630 | 20170110
IWCS | International Workshop on Computational Semantics | 7 | B3 | Sep 19, 2017 - Sep 22, 2017 | Montpellier, France | Apr 15, 2017 | 2017091920170922 | 20170415
IWFHR | International Workshop on Frontiers in Handwriting Recognition | 22 | B1 |  |  |  | z | z
IWLS | International Workshop on Logic and Synthesis | 19 | B2 | Jun 17, 2017 - Jun 18, 2017 | Austin, TX - USA | Mar 12, 2017 (Mar 5, 2017) | 2017061720170618 | 20170312
IWOS | International Workshop on Self-Organizing Systems | 10 | B4 |  |  |  | z | z
IWPSE | International Workshop on Principles of Software Evolution | 23 | B1 |  |  |  | z | z
IWPT | International Conference on Parsing Technologies | 13 | B3 | Sep 20, 2017 - Sep 22, 2017 | Pisa, Italy | Jul 5, 2017 | 2017092020170922 | 20170705
IWQoS | International Workshop on Quality of Service | 44 | A2 | Jun 14, 2017 - Jun 16, 2017 | Vilanova i la Geltru, Barcelona, Spain | Mar 8, 2017 (Feb 28, 2017) | 2017061420170616 | 20170308
IWSCA | IEEE International Workshop on Semantic Computing and Applications | 5 | B4 |  |  |  | z | z
IWSM-MENSURA | International Workshop on Software Measurement and the International Conference on Software Process and Product Measurement | 7 | B4 | Oct 24, 2017 - Oct 26, 2017 | Gothenburg, Sweden | Apr 24, 2017 | 2017102420171026 | 20170424
IWSSIP | International Workshop on Systems Signals and Image Processing | 8 | B4 |  |  |  | z | z
JADT | International conference on statistical analysis of textual data | 6 | B4 |  |  |  | z | z
JCDL | ACM IEEE Joint Conference on Digital Libraries | 53 | A2 |  |  |  | z | z
JCIS | Joint Conference on Information Sciences | 18 | B2 | Jul 19, 2017 - Jul 21, 2017 | San Cristóbal de La Laguna, Tenerife | Feb 19, 2017 | 2017071920170721 | 20170219
JDP | Jornada de Descrição do Português | 1 | B5 |  |  |  | z | z
JELIA | European Conference on Logics in Artificial Intelligence | 30 | B1 |  |  |  | z | z
JIISIC | Jornadas Iberoamericanas de Ingeniería de Software e Ingeniería del Conocimiento | 4 | B4 | Aug 17, 2017 - Aug 18, 2017 | Latacunga, Ecuador | Mar 31, 2017 | 2017081720170818 | 20170331
JMLC | Joint Modular Languages Conference | 13 | B3 |  |  |  | z | z
JSSPP | Workshop on Job Scheduling Strategies for Parallel Processing | 32 | B1 | Jun 2, 2017 - Jun 2, 2017 | Orlando, FL, USA | Feb 12, 2017 | 2017060220170602 | 20170212
JTRES | International Workshop on Java Technologies for Real-time and Embedded Systems | 12 | B3 |  |  |  | z | z
JURIX | International Conference on Legal Knowledge and Information Systems | 12 | B3 |  |  |  | z | z
K-CAP | Knowledge Capture | 34 | B1 | Sep 4, 2017 - Sep 7, 2017 | Prague, Czech Republic | May 10, 2017 | 2017090420170907 | 20170510
KAMC | Knowledge Acquisition from Multimedia Content | 3 | B5 |  |  |  | z | z
KDD | ACM SIGKDD Conference on Knowledge Discovery and Data Mining | 133 | A1 | Aug 13, 2017 - Aug 17, 2017 | Halifax, Nova Scotia - Canada | Dec 9, 2016 | 2017081320170817 | 20161209
KES | International Conference on Knowledge-Based and Intelligent Information and Engineering Systems | 27 | B1 | Jun 21, 2017 - Jun 23, 2017 | Vilamoura, Algarve, Portugal | Feb 15, 2017 | 2017062120170623 | 20170215
KES-AMSTA | International Conference on Agent and Multi-Agent Systems: Technologies and Applications | 13 | B3 | Jun 21, 2017 - Jun 23, 2017 | Vilamoura, Algarve, Portugal | Feb 15, 2017 | 2017062120170623 | 20170215
KR | International Conference on Principles of Knowledge Representation and Reasoning | 46 | A2 | Aug 19, 2017 - Aug 25, 2017 | Melbourne, Australia | Feb 19, 2017 | 2017081920170825 | 20170219
KSEM | International Conference on Knowledge Science Engineering and Management | 11 | B3 | Aug 19, 2017 - Aug 20, 2017 | Melbourne, Australia | Apr 30, 2017 | 2017081920170820 | 20170430
LA-WASP | Latin American Workshop on Aspect-Oriented Software Development | 4 | B4 | Mar 5, 2017 - Mar 9, 2017 | New Orleans, LA, USA | Sep 12, 2016 | 2017030520170309 | 20160912
LA-WEB | Latin American Web Congress | 19 | B2 | Mar 5, 2017 - Mar 9, 2017 | New Orleans, LA, USA | Sep 12, 2016 | 2017030520170309 | 20160912
LACLO | Latin American Conference on Learning Objects | 3 | B5 | Oct 9, 2017 - Oct 13, 2017 | La Plata, Argentina | Jun 5, 2017 | 2017100920171013 | 20170605
LADC | Latin-American Symposium on Dependable Computing | 12 | B3 |  |  |  | z | z
LAGOS | Latin-American Algorithms Graphs and Optimization Symposium | 4 | B4 | Oct 29, 2017 - Oct 31, 2017 | Lagos, Nigeria | May 31, 2017 | 2017102920171031 | 20170531
LANC | Latin America Networking Conference | 6 | B4 |  |  |  | z | z
LANMAN | IEEE Workshop on Local and Metropolitan Area Networks | 17 | B2 |  |  |  | z | z
LANOMS | Latin American Network Operations and Management Symposium | 7 | B4 |  |  |  | z | z
LARS | IEEE Latin American Robotics Symposium | 8 | B4 | Nov 9, 2017 - Nov 11, 2017 | Curitiba, PR, Brazil | Jun 12, 2017 | 2017110920171111 | 20170612
LASCAS | IEEE Latin American Symposium on Circuits and Systems - LASCAS | 2 | B5 |  |  |  | z | z
LATA | Language and Automata Theory and Applications | 12 | B3 | Mar 6, 2017 - Mar 10, 2017 | Umeå, Sweden | Oct 21, 2016 | 2017030620170310 | 20161021
LaTeCH | Workshop on Language Technology for Cultural Heritage Social Sciences and Humanities | 1 | B5 |  |  |  | z | z
LATIN | Latin American Symposium on Theoretical Informatics | 26 | B1 | Nov 6, 2017 - Nov 7, 2017 | Valparaiso, Chile | Aug 7, 2017 | 2017110620171107 | 20170807
LATINCOM | IEEE Latin-american Conference on Communications | 5 | B4 | Nov 8, 2017 - Nov 10, 2017 | Guatemala City | Jul 31, 2017 | 2017110820171110 | 20170731
Latincrypt | International Conference on Cryptology and Information Security in Latin | 5 | B4 |  |  |  | z | z
LATINTCS | Latin American Theoretical Informatics Symposium | 25 | B1 |  |  |  | z | z
LATW | IEEE Latin American Test Workshop | 8 | B4 |  |  |  | z | z
LAW | The Linguistic Annotation Workshop | 13 | B3 | N/A | N/A | Aug 30, 2017 | z | 20170830
LCN | IEEE Conference on Local Computer Networks | 48 | A2 | Oct 9, 2017 - Oct 12, 2017 | Singapore, Singapore | Apr 7, 2017 (Mar 31, 2017) | 2017100920171012 | 20170407
LCPC | International Workshop on Languages and Compilers for Parallel Computing | 9 | B4 |  |  |  | z | z
LCTES | ACM SIGPLAN/SIGBED Conference on Languages Compilers Tools and Theory for Embedded Systems | 34 | B1 | Jun 21, 2017 - Jun 22, 2017 | Barcelona, Spain | Feb 14, 2017 | 2017062120170622 | 20170214
LDTA | Language Descriptions Tools and Applications | 7 | B4 |  |  |  | z | z
LFCS | Symposium on Logical Foundations of Computer Science | 12 | B3 |  |  |  | z | z
LICS | IEEE Symposium on Logic in Computer Science | 52 | A2 | Jun 20, 2017 - Jun 23, 2017 | Reykjavik, Iceland | Jan 9, 2017 (Jan 3, 2017) | 2017062020170623 | 20170109
LIGHTSEC | Workshop on Lightweight Security and Privacy: Devices Protocols and Applications | 3 | B5 |  |  |  | z | z
LINZ | Linz Seminar on Fuzzy Set Theory | 6 | B4 |  |  |  | z | z
LION | Learning and Intelligent Optimization | 9 | B4 |  |  |  | z | z
LoCA | International Symposium on Location- and Context-Awareness | 21 | B2 |  |  |  | z | z
LocWeb | International Workshop on Location and the Web | 8 | B4 |  |  |  | z | z
LOPSTR | International Symposium on Logic-based Program Synthesis and Transformation | 2 | B3 | Oct 10, 2017 - Oct 12, 2017 | Namur, Belgium | Jun 13, 2017 (Jun 6, 2017) | 2017101020171012 | 20170613
LPAR | Logic Programming and Automated Reasoning | 32 | B1 | May 7, 2017 - May 12, 2017 | Maun, Botswana | Jan 15, 2017 (Jan 1, 2017) | 2017050720170512 | 20170115
LPNMR | International Conference on Logic Programming and Non-monotonic Reasoning | 30 | B1 | Jul 3, 2017 - Jul 6, 2017 | Hanasaari, Espoo, Finland | Feb 9, 2017 (Feb 5, 2017) | 2017070320170706 | 20170209
LREC | International Conference on Language Resources and Evaluation | 49 | A2 |  |  |  | z | z
LSFA | Workshop on Logical and Semantic Frameworks with Applications | 5 | B4 | Sep 23, 2017 - Sep 24, 2017 | Brasília, Brazil | Jun 21, 2017 | 2017092320170924 | 20170621
m-ICTE | International Conference on Multimedia and ICT in Education | 2 | B5 | Jul 4, 2017 - Jul 8, 2017 | Torino, Turin, Italy | Apr 3, 2017 | 2017070420170708 | 20170403
M4M | Methods for Modalities | 5 | B4 |  |  |  | z | z
MABS | International Workshop on Multi-Agent-Based Simulation | 11 | B3 |  |  |  | z | z
MASCOTS | IEEE International Symposium on Modeling Analysis and Simulation of Computer and Telecommunication Systems | 42 | A2 | Sep 20, 2017 - Sep 22, 2017 | Banff, Alberta, Canada | Apr 11, 2017 (Apr 4, 2017) | 2017092020170922 | 20170411
MASS | IEEE International Conference on Mobile Ad-hoc and Sensor Systems | 35 | B1 | Oct 22, 2017 - Oct 25, 2017 | Orlando, Florida, USA | May 1, 2017 | 2017102220171025 | 20170501
MATA | International Workshop on Mobility Aware Technologies and Applications | 17 | B2 |  |  |  | z | z
MATES | German Conference on Multiagent System Technologies | 17 | B2 | Aug 23, 2017 - Aug 26, 2017 | Leipzig, Germany | May 12, 2017 | 2017082320170826 | 20170512
Matheuristics | International Workshop on Model-based metaheuristics | 5 | B4 |  |  |  | z | z
MCCSIS | IADIS International Conference Web Based Communities and Social Media | 2 | B5 |  |  |  | z | z
MCM | International Conference on Mathematics and Computation in Music | 4 | B4 |  |  |  | z | z
MCO | International Conference on Modelling Computation and Optimization in Information Systems and Management Sciences | 7 | B4 |  |  |  | z | z
MCS | International Workshop on Multiple Classifier Systems | 34 | B1 |  |  |  | z | z
MDA/MDM | International Conference on Mobile Data Access/Management | 39 | A2 | May 29, 2017 - Jun 1, 2017 | KAIST, Taejeon, South Korea | Dec 15, 2016 | 2017052920170601 | 20161215
MDAI | Modeling Decisions for Artificial Intelligence | 15 | B3 |  |  |  | z | z
MDM | IEEE International Conference on Mobile Data Management | 38 | A2 | May 29, 2017 - Jun 1, 2017 | KAIST, Taejeon, South Korea | Dec 15, 2016 | 2017052920170601 | 20161215
Med-Hoc-Net | IEEE IFIP Annual Mediterranean Ad Hoc Networking Workshop | 7 | B4 | Jun 28, 2017 - Jun 30, 2017 | Budva, Montenegro | Apr 20, 2017 | 2017062820170630 | 20170420
MEDINFO | International Health Medical) Informatics Congress | 27 | B1 |  |  |  | z | z
MESH | International Conference on Advances in Mesh Networks | 5 | B4 | Jun 11, 2017 - Jun 13, 2017 | San Diego, CA, USA | Dec 22, 2016 (Dec 15, 2016) | 2017061120170613 | 20161222
META | International Conference on Metaheuristics and Nature Inspired Computing | 2 | B5 | Nov 27, 2017 - Dec 1, 2017 | Honolulu, Hawaii, USA | Jul 16, 2017 | 2017112720171201 | 20170716
METMBS | International Conference on Mathematics and Engineering Techniques in Medicine and Biological Sciences | 8 | B4 |  |  |  | z | z
MEWS | International Workshop on Mining for Enhanced Web Search | 1 | B5 |  |  |  | z | z
MFCS | International Symposium on Mathematical Foundations of Computer Science | 33 | B1 | Aug 21, 2017 - Aug 25, 2017 | Aalborg, Denmark | Apr 24, 2017 (Apr 20, 2017) | 2017082120170825 | 20170424
MFI | IEEE International Conference on Multisensor Fusion and Integration for Intelligent Systems | 16 | B2 | Nov 16, 2017 - Nov 18, 2017 | EXCO, Daegu, Korea | Jul 10, 2017 | 2017111620171118 | 20170710
MFPS | Mathematical Foundations of Programming Semantics | 2 | B4 | Jun 12, 2017 - Jun 15, 2017 | Ljubljana University, Slovenia | Mar 17, 2017 | 2017061220170615 | 20170317
MGC | International Workshop on Middleware for Grid Computing | 13 | B3 |  |  |  | z | z
MIC | Metaheuristic International Conference | 13 | B3 | Jul 4, 2017 - Jul 7, 2017 | Barcelona, Spain | Jan 23, 2017 | 2017070420170707 | 20170123
MICAI | Mexican International Conference on Artificial Intelligence | 19 | B2 |  |  |  | z | z
MICCAI | International Conference on Medical Image Computing and Computer Assisted Intervention | 56 | A1 | Sep 17, 2017 - Sep 17, 2017 | Quebec City | Jun 18, 2017 | 2017091720170917 | 20170618
MICRO | International Symposium on Microarchitecture | 86 | A1 | Oct 14, 2017 - Oct 18, 2017 | Boston | Apr 4, 2017 (Mar 28, 2017) | 2017101420171018 | 20170404
Middleware | ACM/IFIP/USENIX International Middleware Conference | 49 | A2 | Dec 11, 2017 - Dec 15, 2017 | Las Vegas, Nevada, USA | May 19, 2017 (May 12, 2017) | 2017121120171215 | 20170519
MINES | International Conference on Multimedia Information Networking and Security | 4 | B4 |  |  |  | z | z
MIS | Workshop on Multimedia Information Systems | 10 | B4 | May 8, 2017 - May 10, 2017 | Monastir, Tunisia | Mar 15, 2017 | 2017050820170510 | 20170315
MISTA | Multidisciplinary International Scheduling Conference: Theory and Applications | 3 | B5 |  |  |  | z | z
MKM | Mathematical Knowledge Management | 20 | B2 |  |  |  | z | z
ML | IADIS International Conference on Mobile Learning | 12 | B3 | Sep 7, 2017 - Sep 7, 2017 | Oxford, UK | May 31, 2017 | 2017090720170907 | 20170531
MLDM | IAPR International Conference on Machine Learning and Data Mining | 19 | B2 | Jul 15, 2017 - Jul 21, 2017 | New York, USA | Jan 15, 2017 | 2017071520170721 | 20170115
MLMTA | International Conference on Machine Learning and Applications | 9 | B4 |  |  |  | z | z
MLSP | IEEE International Workshop on Machine Learning for Signal Processing | 16 | B2 | Sep 25, 2017 - Sep 28, 2017 | Roppongi, Tokyo, Japan | May 28, 2017 | 2017092520170928 | 20170528
MMAR | IEEE IFAC International Conference on Methods and Models in Automation and Robotics | 4 | B4 |  |  |  | z | z
MMBIA | IEEE Computer Society Workshops on Mathematical Methods in Biomedical Image Analysis | 21 | B2 |  |  |  | z | z
MMCN | ACM/SPIE Multimedia Computing and Networking | 9 | B3 |  |  |  | z | z
MMEDIA | International Conference on Advances in Multimedia | 6 | B4 | Apr 23, 2017 - Apr 27, 2017 | Venice, Italy | Jan 15, 2017 | 2017042320170427 | 20170115
MMM | International Multi-Media Modelling Conference | 22 | B1 | Aug 28, 2017 - Aug 30, 2017 | Warsaw, Poland | Mar 4, 2017 | 2017082820170830 | 20170304
MMNS | IFIP/IEEE Conference on Management of Multimedia Networks and Services | 15 | B3 |  |  |  | z | z
MMSP | International Workshop on Multimedia Signal Processing | 12 | B2 | Oct 16, 2017 - Oct 18, 2017 | Luton, United Kingdom | Jun 1, 2017 | 2017101620171018 | 20170601
MMSys | ACM SIGMM Conference on Multimedia Systems | 11 | B3 | Jun 20, 2017 - Jun 23, 2017 | Taipei, Taiwan | Dec 9, 2016 | 2017062020170623 | 20161209
MMVR | Annual Medicine Meets Virtual Reality Conference | 4 | B4 |  |  |  | z | z
MobiCASE | Mobile Computing Applications and Services | 5 | B4 |  |  |  | z | z
MOBICOM | ACM International Conference on Mobile Computing and Networking | 112 | A1 | Oct 16, 2017 - Oct 20, 2017 | Snowbird, Utah | Mar 16, 2017 (Mar 9, 2017) | 2017101620171020 | 20170316
MobiDE | Workshop on Data Engineering for Wireless and Mobile Access | 20 | B2 |  |  |  | z | z
MOBIHOC | ACM Symposium of Mobile and Ad-hoc Computing | 110 | A1 | Jul 10, 2017 - Jul 14, 2017 | IIT Madras,  Chennai, India | Jan 20, 2017 (Jan 13, 2014) | 2017071020170714 | 20170120
MobileHCI | International Conference on Human-Computer Interaction with Mobile Devices and Services | 42 | A2 | Sep 4, 2017 - Sep 7, 2017 | Vienna | Feb 9, 2017 | 2017090420170907 | 20170209
MOBILITY | Mobility Conference | 13 | B3 | Oct 8, 2017 - Oct 12, 2017 | Athens, Greece | Jun 20, 2017 | 2017100820171012 | 20170620
MOBIMEDIA | International Conference on Mobile Multimedia Communications | 9 | B4 | Jul 13, 2017 - Jul 14, 2017 | CHONGQING, CHINA | Mar 1, 2017 | 2017071320170714 | 20170301
Mobiquitous | International Conference on Mobile and Ubiquitous Systems: Networks and Services | 32 | B1 | Nov 6, 2017 - Nov 9, 2017 | Melbourne | Jun 30, 2017 (Aug 15, 2017) | 2017110620171109 | 20170630
MobiSys | International Conference on Mobile Systems Applications and Services | 66 | A1 | Jun 19, 2017 - Jun 23, 2017 | Niagara Falls, NY, USA | Dec 8, 2016 (Dec 1, 2016) | 2017061920170623 | 20161208
MobiWac | ACM International Workshop on Mobility Management and Wireless Access | 15 | B3 | Nov 21, 2017 - Nov 25, 2017 | Miami Beach, USA | Jun 30, 2017 | 2017112120171125 | 20170630
MODELS | ACM/IEEE International Conference on Model-Driven Engineering Languages and Systems | 54 | A2 | Sep 17, 2017 - Sep 22, 2017 | Austin, Texas | Apr 21, 2017 (Apr 14, 2017) | 2017091720170922 | 20170421
MoDISE-EUS | Model Driven Information Systems Engineering: Enterprise User and System Models | 3 | B5 |  |  |  | z | z
MODSIM | International Congress on Modelling and Simulation | 12 | B3 |  |  |  | z | z
MOMPES | IEEE International Workshop on Model Based Methodologies for Pervasive and Embedded Software | 9 | B4 |  |  |  | z | z
MoodleMoot | MoodleMoot Brasil | 2 | B5 |  |  |  | z | z
MOPTA | Annual Conference on Modeling and Optimization: Theory and Applications | 1 | B5 |  |  |  | z | z
MPAC | International Workshop on Middleware for Pervasive and Ad-hoc Computing | 14 | B3 |  |  |  | z | z
MPC | International Conference on Mathematics of Program Construction | 22 | B1 |  |  |  | z | z
MPOOL | Workshop on Multiparadigm Programming with Object Oriented Languages | 3 | B5 |  |  |  | z | z
MS | Multimedia Semantics | 8 | B4 |  |  |  | z | z
MSA | International Conference on Multimedia Systems and Applications | 1 | B5 |  |  |  | z | z
MSC | IEEE Multi-conference on Systems and Control | 3 | B5 |  |  |  | z | z
MSE | International Conference on Microelectronic Systems Education | 10 | B4 | Sep 4, 2017 - Sep 4, 2017 | Trento, Italy | Jun 15, 2017 | 2017090420170904 | 20170615
MSN | International Conference on Mobile Ad-hoc and Sensor Networks | 14 | B3 |  |  |  | z | z
MSR | International Workshop on Mining Software Repositories | 32 | B1 | May 20, 2017 - May 21, 2017 | Buenos Aires | Feb 10, 2017 (Feb 3, 2017) | 2017052020170521 | 20170210
MSV | International Conference on Modeling: Simulation and Visualization Methods | 6 | B4 |  |  |  | z | z
MSVVEIS | International Workshop on Modelling Simulation Verification and Validation of Enterprise Information Systems | 7 | B4 |  |  |  | z | z
MSWIM | ACM International Conference on Modeling Analysis and Simulation of Wireless and Mobile Systems | 35 | B1 | Nov 21, 2017 - Nov 25, 2017 | Miami, USA | May 20, 2017 | 2017112120171125 | 20170520
MTAGS | Workshop on Many-Task Computing on Grids and Supercomputers  | 7 | B4 |  |  |  | z | z
MTSR | International Conference on Metadata and Semantics Research | 5 | B4 | Nov 28, 2017 - Dec 1, 2017 | Tallinn, Estonia | Jul 10, 2017 | 2017112820171201 | 20170710
MTSummit | Machine Translation Summit | 18 | B2 |  |  |  | z | z
MTV | Microprocessor Test and Verification | 11 | B3 |  |  |  | z | z
Multimedia | ACM Multimedia Conference | 83 | A1 | Dec 11, 2017 - Dec 13, 2017 | Taichung, Taiwan | Jul 15, 2017 | 2017121120171213 | 20170715
MULTIPROG | Workshop on Programmability Issues for Multi-Core Computers | 2 | B5 | Jan 24, 2017 - Jan 24, 2017 | Stockholm, Sweden | Nov 4, 2016 | 2017012420170124 | 20161104
MUM | International Conference on Mobile and Ubiquitous Multimedia | 21 | B2 | Apr 7, 2017 - Apr 8, 2017 | St. Francis Institute of Technology, Mum | Oct 30, 2016 | 2017040720170408 | 20161030
MVA | IAPR International Workshop on Machine Vision Applications | 15 | B3 | May 8, 2017 - May 12, 2017 | Nagoya, Japan | Dec 12, 2016 | 2017050820170512 | 20161212
MWC | IEEE CAS Symposium on Emerging Technologies: Frontiers of Mobile and Wireless Communication | 13 | B3 |  |  |  | z | z
MWE | Workshop on Multiword Expressions | 14 | B3 | Apr 4, 2017 - Apr 4, 2017 | Valencia, Spain | Jan 16, 2017 | 2017040420170404 | 20170116
MWSCAS | IEEE International Midwest Symposium on Circuits and Systems | 26 | B1 | Aug 6, 2017 - Aug 9, 2017 | Boston, MA | Apr 1, 2017 | 2017080620170809 | 20170401
NAACL | North American Chapter of the Association for Computational Linguistics Annual Meeting | 73 | A1 |  |  |  | z | z
NAFIPS | North American Fuzzy Information Processing Society International Conference | 15 | B2 |  |  |  | z | z
NANO | IEEE Conference on Nanotechnology | 7 | B4 | N/A | N/A | Dec 2, 2017 | z | 20171202
Nanoarch | IEEE/ACM International Symposium on Nanoscale Architectures | 8 | B4 | Jul 25, 2017 - Jul 27, 2017 | Newport, RI, USA  | Apr 29, 2017 | 2017072520170727 | 20170429
NAS | Networking Architecture and Storage | 10 | B4 | Aug 7, 2017 - Aug 9, 2017 | Shenzhen, China | Apr 10, 2017 | 2017080720170809 | 20170410
NCA | IEEE International Symposium on Network Computing and Applications | 31 | B1 |  |  |  | z | z
NCIS | International Conference on Network Computing and Information Security | 2 | B5 |  |  |  | z | z
NDSS | Usenix Networked and Distributed System Security Symposium | 64 | A1 | Feb 26, 2017 - Mar 3, 2017 | San Diego, CA | Aug 16, 2016 (Aug 12, 2016) | 2017022620170303 | 20160816
Net-Con | IFIP International Conference on Network Control and Engineering for QoS: Security and Mobility | 6 | B4 | Jul 18, 2017 - Jul 19, 2017 | Asian Institute of Technology (AIT), Con | Jul 16, 2017 | 2017071820170719 | 20170716
NETCOM | International Conference on Networks & Communications | 4 | B4 | Nov 25, 2017 - Nov 26, 2017 | Dubai, UAE | Jul 1, 2017 | 2017112520171126 | 20170701
NetGames | Workshop on Network and System Support for Games | 34 | B1 | Jun 22, 2017 - Jun 23, 2017 | Taipei, Taiwan | Feb 10, 2017 | 2017062220170623 | 20170210
Networking | IFIP International Conference on Networking | 31 | B1 | Oct 27, 2017 - Oct 30, 2017 | Chengdu, China | Aug 5, 2017 | 2017102720171030 | 20170805
NEW2AN | Next Generation Teletraffic and Wired/Wireless Advanced Networking | 13 | B3 | Aug 28, 2017 - Aug 30, 2017 | St. Petersburg, Russia | Jun 1, 2017 | 2017082820170830 | 20170601
NEWCAS | IEEE North-East Workshop on Circuits and Systems | 8 | B4 |  |  |  | z | z
NFM | NASA Formal Methods | 7 | B4 | May 16, 2017 - May 18, 2017 | NASA Ames Research Center, Moffett Field | Dec 5, 2016 (Nov 28, 2016) | 2017051620170518 | 20161205
NGITS | International Conference on Next Generation Information Technologies and Systems | 7 | B4 |  |  |  | z | z
NIDISC | Workshop on Nature Inspired Distributed Computing | 1 | B5 |  |  |  | z | z
NIME | New Interfaces for Music Expression | 32 | B1 | May 15, 2017 - May 19, 2017 | Copenhagen | Jan 31, 2017 (Jan 24, 2017) | 2017051520170519 | 20170131
NIPS | Neural Information Processing Systems | 88 | A1 | Dec 4, 2017 - Dec 9, 2017 | Long Beach, CA, USA | May 19, 2017 | 2017120420171209 | 20170519
NLDB | International Conference on Applications of Natural Language to Data Bases | 22 | B1 | Jun 21, 2017 - Jun 23, 2017 | University of Liège, Liège, Belgium | Feb 12, 2017 (Feb 5, 2017) | 2017062120170623 | 20170212
NLP-KE | International Conference on Natural Language Processing and Knowledge Engineering | 5 | B4 | N/A | N/A | Jul 8, 2017 | z | 20170708
NLPCS | International Workshop on Natural Language Processing and Cognitive Science | 4 | B4 |  |  |  | z | z
NMR | International Workshop on Non-Monotonic Reasoning | 20 | B2 |  |  |  | z | z
NOCARC | International Workshop on Network on Chip Architectures | 5 | B4 |  |  |  | z | z
NOCS | International Symposium on Networks-on-Chip | 28 | B1 | Oct 19, 2017 - Oct 20, 2017 | Seoul, South Korea | May 1, 2017 (Apr 24, 2017) | 2017101920171020 | 20170501
NOMS | IEEE/IFIP Network Operations and Management Symposium | 37 | A2 |  |  |  | z | z
NORCHIP | The Nordic Microelectronics event | 15 | B3 |  |  |  | z | z
NordiCHI | Nordic Conference on Human-computer interaction | 35 | B1 |  |  |  | z | z
NOSSDAV | Network and Operating System Support for Digital A/V | 41 | A2 | Jun 20, 2017 - Jun 23, 2017 | Taipei, Taiwan  | Feb 10, 2017 | 2017062020170623 | 20170210
NPAR | Non-Photorealistic Animation and Rendering | 39 | A2 |  |  |  | z | z
NPC | IFIP International Conference on Network and Parallel Computing | 12 | B3 | Oct 20, 2017 - Oct 21, 2017 | Hefei, China | Jun 10, 2017 | 2017102020171021 | 20170610
NSDI | Symposium on Networked Systems: Design and Implementation | 69 | A1 | Mar 27, 2017 - Mar 29, 2017 | Boston, MA | Sep 21, 2016 (Sep 14, 2016) | 2017032720170329 | 20160921
NTMS | New Technologies Mobility and Security | 10 | B4 |  |  |  | z | z
NWeSP | International Conference on Next Generation Web Services Practices | 14 | B3 |  |  |  | z | z
NyirCrypt | Central European Conference on Cryptography | 2 | B5 |  |  |  | z | z
OASICS | OpenAcess Series in Informatics | 3 | B5 |  |  |  | z | z
OASIS | Foundations of Intrusion Tolerant Systems | 1 | B5 |  |  |  | z | z
OBI | Ontology-supported Business Intelligence | 6 | B4 |  |  |  | z | z
ODBASE | International Conference on Ontologies Databases and Applications of Semantics | 5 | B3 |  |  |  | z | z
ODBIS | Ontologies-Based Databases and Information Systems | 4 | B4 |  |  |  | z | z
ODP | IFIP International Conference on Open Distributed Processing | 1 | B3 |  |  |  | z | z
OKCon | Open Knowledge Conference | 2 | B5 |  |  |  | z | z
OL | Ontology Learning | 4 | B4 | Dec 7, 2017 - Dec 8, 2017 | The Hong Kong Polytechnic University | May 18, 2017 | 2017120720171208 | 20170518
OM | Ontology Matching | 24 | B1 |  |  |  | z | z
ONISW | Ontologies and Information Systems for the Semantic Web | 5 | B4 |  |  |  | z | z
ONTOSE | Ontology Conceptualization and Epistemology for Information Systems Software Engineering and Service Science | 3 | B5 |  |  |  | z | z
OOIS | International Conference on Object-Oriented Information Systems | 16 | B2 |  |  |  | z | z
OOPSLA | ACM SIGPLAN International Conference on Object-Oriented Programming Systems Languages and Applications | 76 | A1 | Oct 25, 2017 - Oct 27, 2017 | Vancouver | Apr 17, 2017 (Apr 13, 2017) | 2017102520171027 | 20170417
OPODIS | International Conference on Principles of Distributed Systems | 19 | B2 |  |  |  | z | z
OPTIM | Optimization of Electrical and Electronic Equipment | 9 | B4 | Jan 1, 2017 - Jun 30, 2017 | In the ‘Advances in Computational Intell | Jun 30, 2017 (Feb 15, 2017) | 2017010120170630 | 20170630
OptMAS | Workshop on Optimisation in Multi-Agent Systems | 1 | B5 |  |  |  | z | z
OR | International Conference on Operations Research | 5 | B4 |  |  |  | z | z
OSDI | Usenix Symposium on Operating Systems Design and Implementation | 76 | A1 |  |  |  | z | z
OSS | International Conference on Open Source Systems | 16 | B2 |  |  |  | z | z
OWLED | OWLED Workshop on OWL: Experiences and Directions | 26 | B1 | Jul 4, 2017 - Jul 4, 2017 | Milan | Apr 15, 2017 | 2017070420170704 | 20170415
OZCHI | Australasian Computer-Human Interaction Conference | 22 | B1 | Nov 27, 2017 - Dec 1, 2017 | Brisbane | Jun 16, 2017 | 2017112720171201 | 20170616
P2P | International Conference on Peer-to-Peer Computing | 46 | A2 | Nov 8, 2017 - Nov 10, 2017 | Barcelona, Spain | Jul 15, 2017 | 2017110820171110 | 20170715
PAAMS | International Conference on Practical Applications of Agents and Multiagent Systems | 9 | B4 | Jun 21, 2017 - Jun 23, 2017 | Porto, Portugal | Feb 6, 2017 | 2017062120170623 | 20170206
PAAP | International Symposium on Parallel Architectures Algorithms and Programming | 3 | B5 |  |  |  | z | z
PACCS | Pacific-Asia Conference on Circuits Communications and Systems | 5 | B4 |  |  |  | z | z
PACIFICVIS | IEEE Pacific Visualization Symposium | 11 | B3 |  |  |  | z | z
PACT | International Conference on Parallel Architectures and Compilation Techniques | 53 | A2 | Sep 13, 2017 - Sep 17, 2017 | Portland, Oregon, USA | Mar 14, 2017 | 2017091320170917 | 20170314
PADL | International Symposium on Practical Aspects of Declarative Languages | 30 | B1 | Jan 16, 2017 - Jan 17, 2017 | Paris, France | Sep 19, 2016 (Sep 12, 2016) | 2017011620170117 | 20160919
PADO | Symposium on Programs as Data Objects | 10 | B4 |  |  |  | z | z
PADS | ACM/IEEE/SCS Workshop on Parallel and Distributed Simulation | 27 | B1 | N/A | N/A | May 31, 2017 | z | 20170531
PADTAD | Workshop on Parallel and Distributed Systems: Testing Analysis and Debugging | 14 | B3 |  |  |  | z | z
PAKM | International Conference on Practical Aspects of Knowledge Management | 19 | B2 |  |  |  | z | z
PAM | Passive and Active Measurement Conference | 40 | A2 | Mar 30, 2017 - Mar 31, 2017 | Sydney | Oct 14, 2016 (Oct 7, 2016) | 2017033020170331 | 20161014
ParCo | International Conference on Parallel Computing | 14 | B3 | Sep 12, 2017 - Sep 15, 2017 | Bologna, Italy | Jul 31, 2017 (Feb 28, 2017) | 2017091220170915 | 20170731
PATAT | International Conference on the Practice and Theory of Automated Timetabling | 12 | B3 |  |  |  | z | z
PATMOS | International Workshop on Power and Timing Modeling: Optimization and Simulation | 17 | B2 |  |  |  | z | z
PBG | IEEE/Eurographics Symposium on Point-Based Graphics | 18 | B2 |  |  |  | z | z
PCNS | SPIE Conference on Performance and Control of Network Systems | 2 | B5 |  |  |  | z | z
PCS | International Picture Coding Symposium | 25 | B1 |  |  |  | z | z
PDC | Participatory Design Conference | 22 | B1 |  |  |  | z | z
PDCAT | International Conference on Parallel and Distributed Computing Applications and Technologies | 18 | B2 |  |  |  | z | z
PDCS | International Conference on Parallel and Distributed Computing and Systems | 5 | B3 |  |  |  | z | z
PDMC-HIBI | International Workshop on Parallel and Distributed Methods in Verification International Workshop on High Performance Computational Systems | 6 | B4 |  |  |  | z | z
PDP | Euromicro International Conference on Parallel Distributed and network-based Processing | 27 | B1 | Mar 6, 2017 - Mar 8, 2017 | St. Petersburg, Russia | Oct 12, 2016 | 2017030620170308 | 20161012
PDPTA | International Conference on Parallel and Distributed Processing Techniques and Applications | 21 | B2 | Jul 17, 2017 - Jul 20, 2017 | Las Vegas, USA | Apr 14, 2017 | 2017071720170720 | 20170414
PE-WASUN | ACM International Symposium on Performance Evaluation of Wireless Ad Hoc Sensor and Ubiquitous Networks | 21 | B2 |  |  |  | z | z
PEPM | Symposium on Partial Evaluation and Program Manipulation | 25 | B1 |  |  |  | z | z
PERCOL | Workshop on Pervasive Collaboration and Social Networking | 30 | B1 |  |  |  | z | z
PerCom | Annual IEEE International Conference on Pervasive Computing and Communications | 54 | A2 | Mar 13, 2017 - Mar 17, 2017 | Kona, Hawaii, USA | Sep 30, 2016 (Sep 23, 2016) | 2017031320170317 | 20160930
PEREL | Workshop on Pervasive Learning Life and Leisure | 23 | B1 |  |  |  | z | z
PERFOMANCE | IFIP International Symposium on Computer Performance Modeling Measurements and Evaluation | 10 | B4 |  |  |  | z | z
PERVASIVE | International Conference on Pervasive Computing | 48 | A2 | Sep 11, 2017 - Sep 15, 2017 | Maui, HI, USA | Apr 1, 2017 | 2017091120170915 | 20170401
PETRA | International Conference on Pervasive Technologies Related to Assistive Environments | 12 | B3 | Jun 21, 2017 - Jun 23, 2017 | Rhodes, Greece | Jan 30, 2017 | 2017062120170623 | 20170130
PG | Pacific Conference on Computer Graphics and Applications | 40 | A2 |  |  |  | z | z
PhiSE | Philosophical Foundations on Information Systems Engineering | 3 | B5 |  |  |  | z | z
PIMRC | IEEE International Symposium on Personal and Indoor Mobile Radio Conference | 60 | A1 | Oct 8, 2017 - Oct 13, 2017 | Montreal, Canada | May 19, 2017 | 2017100820171013 | 20170519
PKAW | Pacific Rim Knowledge Acquisition Workshop | 6 | B4 |  |  |  | z | z
PKC | International Conference on Theory and Practice of Public-Key Cryptography | 54 | A2 | Mar 28, 2017 - Mar 28, 2017 | Amsterdam, Netherlands | Oct 6, 2016 | 2017032820170328 | 20161006
PKDD | European Conference on Principles and Practice of Knowledge Discovery in Databases | 52 | A2 | Sep 18, 2017 - Sep 22, 2017 | Skopje, Macedonia | Apr 20, 2017 (Apr 13, 2017) | 2017091820170922 | 20170420
PLC | International Conference on Programming Languages and Compilers | 7 | B4 |  |  |  | z | z
PLDI | ACM SIGPLAN conference on Programming Language Design and Implementation | 98 | A1 | Jun 19, 2017 - Jun 23, 2017 | Barcelona, Spain | Nov 15, 2016 | 2017061920170623 | 20161115
PLoP | Pattern Languages of Programs Conference | 10 | B3 |  |  |  | z | z
PODC | ACM Symposium on Principles of Distributed Computing | 61 | A1 | Jul 25, 2017 - Jul 27, 2017 | Washington, D.C., USA | Feb 15, 2017 | 2017072520170727 | 20170215
PODS | ACM SIGACT-SIGMOD-SIGART Symposium on Principles of Database Systems | 69 | A1 | May 14, 2017 - May 19, 2017 | Raleigh, North Carolina, USA | Dec 18, 2016 (Dec 11, 2016) | 2017051420170519 | 20161218
PoEM | The IFIP WG8.1 Working Conference on the Practice of Enterprise Modelling | 7 | B4 | Nov 22, 2017 - Nov 24, 2017 | Leuven, Belgium | Jul 23, 2017 (Jul 16, 2017) | 2017112220171124 | 20170723
POLICY | IEEE International Workshop on Policies for Distributed Systems and Networks | 39 | A2 | Oct 21, 2017 - Oct 22, 2017 | Vienna, Austria | Jul 21, 2017 | 2017102120171022 | 20170721
POPL | ACM SIGPLAN-SIGACT Symposium on Principles of Programming Languages | 89 | A1 | Jan 18, 2017 - Jan 20, 2017 | Paris, France | Jul 6, 2016 (Jul 1, 2016) | 2017011820170120 | 20160706
PPDP | International ACM SIGPLAN Symposium on Principles and Practice of Declarative Programming | 32 | B1 |  |  |  | z | z
PPoPP | Principles and Practice of Parallel Programming | 53 | A2 | Feb 4, 2017 - Feb 8, 2017 | Austin, Texas, United States | Aug 1, 2016 | 2017020420170208 | 20160801
PPSC | SIAM Conference on Parallel Processing for Scientific Computing | 6 | B4 |  |  |  | z | z
PPSN | Parallel Problem Solving From Nature | 46 | A2 |  |  |  | z | z
PPSWR | Principles and Practice of Semantic Web Reasoning | 18 | B2 |  |  |  | z | z
PQCrypto | International Workshop on Post-Quantum Cryptography | 11 | B3 |  |  |  | z | z
PRDC | IEEE Pacific Rim International Symposium on Dependable Computing | 23 | B1 | Jan 22, 2017 - Jan 25, 2017 | Christchurch, New Zealand | Jun 30, 2016 | 2017012220170125 | 20160630
PRIS | International Workshop on Pattern Recognition in Information Systems | 13 | B3 |  |  |  | z | z
PRO-VE | IFIP Working Conference on Virtual Enterprises | 13 | B3 | Sep 18, 2017 - Sep 20, 2017 | Vicenza, Italy | Apr 2, 2017 (Mar 12, 2017) | 2017091820170920 | 20170402
PROFES | International Conference on Product Focused Software Process Improvement | 22 | B1 | Nov 29, 2017 - Dec 1, 2017 | Innsbruck, Austria | Jun 16, 2017 (Jun 9, 2017) | 2017112920171201 | 20170616
ProMAS | International Workshop on Programming Multi-Agent Systems | 11 | B3 |  |  |  | z | z
PROPOR | International Conference on Computational Processing of Portuguese | 15 | B3 |  |  |  | z | z
PSB | Pacific Symposium on Biocomputing | 57 | A1 |  |  |  | z | z
PSC | International Conference on Pervasive Systems and Computing | 8 | B4 |  |  |  | z | z
PSIVT | Pacific-Rim Symposium on Image and Video Technology | 13 | B3 |  |  |  | z | z
PST | Conference on Privacy Security and Trust | 18 | B2 | Aug 28, 2017 - Aug 30, 2017 | Calgary | May 15, 2017 | 2017082820170830 | 20170515
PWC | IFIP International Conference on Personal Wireless Communications | 24 | B1 |  |  |  | z | z
QEST | International Conference on Quantitative Evaluation of Systems | 27 | B1 | Sep 5, 2017 - Sep 7, 2017 | Berlin | Apr 9, 2017 | 2017090520170907 | 20170409
QoSA | International ACM Sigsoft Conference on the Quality of Software Architectures | 2 | B5 |  |  |  | z | z
QSHINE | International Conference on Quality of Service in Heterogeneous Wired/Wireless Networks | 17 | B2 |  |  |  | z | z
QSIC | International Quality Software Conference | 24 | B1 |  |  |  | z | z
Qsine | International Conference on Quality of Service in Heterogeneous Wired/Wireless Networks | 17 | B2 |  |  |  | z | z
QUATIC | International Conference on the Quality of Information and Communications Technology | 7 | B4 |  |  |  | z | z
RADECS | Conference on RADIATION EFFECTS on COMPONENTS and SYSTEMS | 12 | B3 | Oct 2, 2017 - Oct 6, 2017 | Geneva | Apr 24, 2017 (Dec 14, 2016) | 2017100220171006 | 20170424
RAID | International Symposium on Recent Advances in Intrusion Detection | 53 | A2 | Sep 18, 2017 - Sep 20, 2017 | Atlanta, Georgia | Mar 28, 2017 | 2017091820170920 | 20170328
RANDOM | International Workshop on Randomization and Computation | 24 | B1 |  |  |  | z | z
RANLP | Recent Advances in Natural Language Processing | 17 | B2 | Sep 2, 2017 - Sep 8, 2017 | Varna, Bulgaria | May 5, 2017 (Apr 28, 2017) | 2017090220170908 | 20170505
RAW | Reconfigurable Architectures Workshop | 5 | B3 |  |  |  | z | z
RCIS | International Conference on Research Challenges in Information Science | 10 | B4 | May 10, 2017 - May 12, 2017 | Brighton, UK | Feb 5, 2017 | 2017051020170512 | 20170205
RE | IEEE International Requirements Engineering Conference | 55 | A1 | Sep 4, 2017 - Sep 8, 2017 | Lisbon | Feb 17, 2017 (Feb 10, 2017) | 2017090420170908 | 20170217
RECOMB | Annual International Conference on Research in Computational Molecular Biology | 54 | A2 | Oct 4, 2017 - Oct 6, 2017 | Barcelona | Jun 11, 2017 | 2017100420171006 | 20170611
RECONFIG | International Conference on Reconfigurable Computing and FPGAs | 9 | B4 |  |  |  | z | z
ReCoSoC | International Workshop on Reconfigurable Communication-centric Systems-on-Chip | 9 | B4 | Jul 12, 2017 - Jul 14, 2017 | Madrid | Apr 7, 2017 (Mar 31, 2017) | 2017071220170714 | 20170407
RecSys | ACM Recommender Systems Conference | 29 | B1 | Aug 27, 2017 - Aug 31, 2017 | Como, Italy | Apr 3, 2017 (Mar 27, 2017) | 2017082720170831 | 20170403
REET | Requirements Engineering Education and Training | 3 | B5 |  |  |  | z | z
REFSQ | Working Conference on Requirements Engineering: Foundation for Software Quality | 15 | B3 | Feb 27, 2017 - Mar 2, 2017 | Essen, Germany | Sep 26, 2016 | 2017022720170302 | 20160926
RFID-TA | RFID-Technologies and Applications | 5 | B4 | Oct 8, 2017 - Oct 12, 2017 | Athens, Greece | Jun 20, 2017 | 2017100820171012 | 20170620
RIDE | International Workshop on Research Issues in Data Engineering | 24 | B1 |  |  |  | z | z
RO-MAN | IEEE International Symposium on Robot and Human Interactive Communication | 22 | B1 | Dec 21, 2017 - Dec 23, 2017 | Bhubaneswar | Aug 1, 2017 | 2017122120171223 | 20170801
ROBIO | IEEE International Conference on Robotics and Biomimetics | 22 | B1 |  |  |  | z | z
ROBOCOMM | IEEE International Conference on Robot Communication and Coordination | 10 | B4 |  |  |  | z | z
RR | International Conference on Web Reasoning and Rule Systems | 15 | B3 |  |  |  | z | z
RRS | Rational Robust and Secure Negotiation Mechanisms in Multi-Agent Systems | 4 | B4 |  |  |  | z | z
RSA | RSA Conference | 11 | B1 | Feb 14, 2017 - Feb 17, 2017 | RSA Conference USA, San Francisco | Sep 3, 2016 | 2017021420170217 | 20160903
RSP | International Workshop on Rapid System Prototyping | 20 | B2 |  |  |  | z | z
RSS | Robotics: Science and Systems Conference | 39 | A2 | Jul 12, 2017 - Jul 16, 2017 | Cambridge MA, USA | Jan 30, 2017 | 2017071220170716 | 20170130
RST | Reliable Software Technologies | 18 | B2 | Sep 4, 2017 - Sep 4, 2017 | Spain | Jun 2, 2017 | 2017090420170904 | 20170602
RST_A | Workshop A RST e os Estudos do Texto | 2 | B5 |  |  |  | z | z
RTA | International Conference on Rewriting Techniques and Applications | 33 | B1 |  |  |  | z | z
RTAS | IEEE Real-Time and Embedded Technology and Applications Symposium | 42 | A2 |  |  |  | z | z
RTCOMP | International Conference on Real-Time Computing Systems and Applications | 26 | B1 |  |  |  | z | z
RTCSA | International Conference on Real-Time and Embedded Computing Systems and Applications | 26 | B1 | Aug 16, 2017 - Aug 18, 2017 | Hsinchu, Taiwan | Apr 14, 2017 | 2017081620170818 | 20170414
RTSS | Real Time Systems Symposium | 57 | A1 | Dec 5, 2017 - Dec 8, 2017 | Paris, France | May 1, 2017 | 2017120520171208 | 20170501
RuleApps | East European Workshop on Rule-Based Applications | 3 | B5 |  |  |  | z | z
RULEML | International Conference on Rules and Rule Markup Languages for the Semantic Web | 22 | B1 |  |  |  | z | z
S&P | IEEE Symposium on Security and Privacy S&P) | 110 | A1 | May 17, 2017 - May 19, 2017 | Avignon, France | Feb 15, 2017 | 2017051720170519 | 20170215
SAB | International Conference on Simulation of Adaptive Behavior | 16 | B2 |  |  |  | z | z
SAC | ACM Symposium on Applied Computing | 61 | A1 | Apr 3, 2017 - Apr 7, 2017 | Marrakesh, Morocco | Sep 15, 2016 | 2017040320170407 | 20160915
SACI | International Symposium on Applied Computational Intelligence and Informatics | 6 | B4 |  |  |  | z | z
SACMAT | ACM Symposium on Access Control Models and Technologies | 55 | A1 |  |  |  | z | z
SAC_A | Selected Areas in Cryptography | 21 | B2 |  |  |  | z | z
SAFECOMP | International Conference on Computer Safety: Reliability and Security | 22 | B1 | Sep 12, 2017 - Sep 15, 2017 | Trento | Mar 10, 2017 | 2017091220170915 | 20170310
SAFEProcess | IFAC Symposioum on Fault Detection Supervision and Safety of Technical Processes | 14 | B3 |  |  |  | z | z
SAICSIT | Conference of the South African Institute of Computer Scientists and Information Technologists | 12 | B3 |  |  |  | z | z
SAINT | International Symposium on Applications and the Internet | 32 | B1 | Oct 25, 2017 - Oct 27, 2017 | Saint Pierre, Reunion Island, France | Jun 1, 2017 | 2017102520171027 | 20170601
SAM | International Conference on Security and Management | 12 | B3 |  |  |  | z | z
SAME | Semantic Ambient Media Experiences | 3 | B5 |  |  |  | z | z
SAMOS | International Conference on Embedded Computer Systems: Architectures Modeling and Simulation | 16 | B2 |  |  |  | z | z
SAMT | Semantics and Digital Media Technologies | 11 | B3 |  |  |  | z | z
Sandbox | ACM SIGGRAPH Sandbox Symposium on Videogames | 15 | B3 |  |  |  | z | z
SAPIR | International Workshop on Service Assurance with Partial and Intermittent Resources | 4 | B4 |  |  |  | z | z
SARNOFF | IEEE Sarnoff Symposium | 15 | B3 | Sep 18, 2017 - Sep 20, 2017 | New Jersey | May 31, 2017 | 2017091820170920 | 20170531
SAS | International Static Analysis Symposium | 48 | A2 | Aug 30, 2017 - Sep 1, 2017 | New York City, USA | Apr 20, 2017 (Apr 14, 2017) | 2017083020170901 | 20170420
SASO | IEEE International Conference on Self-Adaptive and Self-Organizing Systems | 15 | B3 | Sep 18, 2017 - Sep 22, 2017 | University of Arizona, Tucson, AZ | May 10, 2017 (May 1, 2017) | 2017091820170922 | 20170510
SASP | Symposium on Application Specific Processors | 8 | B4 |  |  |  | z | z
SAST | Brazilian Workshop on Systematic and Automated Software Testing | 3 | B5 |  |  |  | z | z
SAT | International Conference on Theory and Applications of Satisfiability Testing | 32 | B1 | Aug 28, 2017 - Sep 1, 2017 | Melbourne, Australia | May 2, 2017 (Apr 26, 2017) | 2017082820170901 | 20170502
SAVCBS | Workshop on Specification and Verification of Component-Based Systems | 7 | B4 |  |  |  | z | z
SBAC-PAD | International Symposium on Computer Architecture and High Performance | 18 | B2 | Oct 17, 2017 - Oct 20, 2017 | Campinas - Brazil | Jun 17, 2017 (Jun 2, 2017) | 2017101720171020 | 20170617
SBAI | Simpósio Brasileiro de Automação Inteligente | 7 | B4 |  |  |  | z | z
SBCARS | Simpósio Brasileiro de Componentes Arquiteturas e Reutilização de Software | 6 | B4 |  |  |  | z | z
SBCCI | Symposium on Integrated Circuits and Systems Design | 24 | B1 |  |  |  | z | z
SBCM | Simpósio Brasileiro de Computação Musical | 5 | B4 | Sep 3, 2017 - Sep 6, 2017 | São Paulo, SP, Brazil | May 30, 2017 | 2017090320170906 | 20170530
SBCUP | Simpósio Brasileiro de Computação Ubíqua e Pervasiva | 2 | B5 |  |  |  | z | z
SBES | Simpósio Brasileiro de Engenharia de Software | 16 | B2 |  |  |  | z | z
SBESC | Simpósio Brasileiro de Engenharia de Sistemas Computacionais | 6 | B4 | Nov 7, 2017 - Nov 10, 2017 | Curitiba, Paraná, Brazil | Jul 7, 2017 | 2017110720171110 | 20170707
SBGames | Simpósio Brasileiro de Jogos e Entretenimento Digital | 7 | B4 |  |  |  | z | z
SBIA | Brazilian Symposium on Artificial Intelligence | 19 | B2 |  |  |  | z | z
SBIE | Simpósio Brasileiro de Informática na Educação | 18 | B2 |  |  |  | z | z
SBLP | Simpósio Brasileiro de Linguagens de Programação | 13 | B3 |  |  |  | z | z
SBM | Eurographics Workshop on Sketch-Based Interfaces and Modeling | 14 | B3 |  |  |  | z | z
SBMF | Simpósio Brasileiro de Métodos Formais | 7 | B4 | Nov 27, 2017 - Dec 1, 2017 | Recife, Pernambuco, Brazil | Jul 14, 2017 (Jul 7, 2017) | 2017112720171201 | 20170714
SBPO | Simpósio Brasileiro de Pesquisa Operacional | 9 | B4 |  |  |  | z | z
SBQS | Simpósio Brasileiro de Qualidade de Software | 11 | B3 |  |  |  | z | z
SBRC | Simpósio Brasileiro de Redes de Computadores e Sistemas Distribuídos | 16 | B2 | May 15, 2017 - May 19, 2017 | Belem, Brazil | Dec 12, 2016 (Dec 5, 2016) | 2017051520170519 | 20161212
SBRN | Simpósio Brasileiro de Redes Neurais | 14 | B3 |  |  |  | z | z
SBRT | Simpósio Brasileiro de Telecomunicações | 6 | B4 |  |  |  | z | z
SBSC | Simpósio Brasileiro de Sistemas Colaborativos | 5 | B4 |  |  |  | z | z
SBSE | Simpósio Brasileiro de Sistemas Elétricos | 3 | B5 | Jul 15, 2017 - Jul 19, 2017 | Berlin, Germany | Feb 6, 2017 (Jan 30, 2017) | 2017071520170719 | 20170206
SBSEG | Simpósio de Segurança da Informação e Sistemas Computacionais | 8 | B4 |  |  |  | z | z
SBSI | Simpósio Brasileiro de Sistemas de Informação | 7 | B4 |  |  |  | z | z
SC | ACM/IEEE Conference on High Performance Networking and Computing | 76 | A1 | Nov 12, 2017 - Nov 17, 2017 | Denver, CO | Apr 3, 2017 (Mar 20, 2017) | 2017111220171117 | 20170403
SCA | ACM SIGGRAPH / Eurographics Symposium on Computer Animation | 39 | A2 | Jul 28, 2017 - Jul 30, 2017 | UCLA Campus, Los Angeles | Apr 14, 2017 | 2017072820170730 | 20170414
SCAM | IEEE International Workshop on Source Code Analysis and Manipulation | 15 | B3 | Sep 17, 2017 - Sep 18, 2017 | China | Jun 19, 2017 (Jun 15, 2017) | 2017091720170918 | 20170619
SCAN | GAMM / IMACS International Symposion on Scientific Computing Computer Arithmetic and Validated Numerics | 7 | B4 | May 1, 2017 - May 1, 2017 | Atlanta, Georgia, USA | Jan 17, 2017 | 2017050120170501 | 20170117
SCC | IEEE International Conference on Services Computing | 33 | B1 | Jun 25, 2017 - Jun 30, 2017 | Honolulu, HI, USA | Jan 19, 2017 | 2017062520170630 | 20170119
SCCC | International Conference of the Chilean Computer Science Society | 13 | B3 | Oct 16, 2017 - Oct 20, 2017 | Arica, Chile | Aug 7, 2017 | 2017101620171020 | 20170807
SCCI | IEEE Symp. Series on Computational Intelligence | 3 | B5 |  |  |  | z | z
SCN | International Conference on Security in Communication Networks | 16 | B2 | N/A | N/A | Oct 27, 2017 | z | 20171027
SCOPES | International Workshop on Software and Compilers for Embedded Systems | 16 | B2 |  |  |  | z | z
SCSC | Summer Computer Simulation Conference | 15 | B3 | Jul 9, 2017 - Jul 12, 2017 | Seattle | Apr 17, 2017 | 2017070920170712 | 20170417
SCW | IEEE Workshop on Speech Coding | 18 | B2 |  |  |  | z | z
SC_A | International Symposium on Software Composition | 21 | B2 |  |  |  | z | z
SDKB | International Workshops on Semantics in Data and Knowledge Bases | 6 | B4 |  |  |  | z | z
SDM | SIAM International Conference on Data Mining | 51 | A2 | Apr 27, 2017 - Apr 29, 2017 | Houston, Texas, USA | Oct 15, 2016 (Oct 8, 2016) | 2017042720170429 | 20161015
SEA | Symposium on Experimental Algorithms | 26 | B1 | Jun 21, 2017 - Jun 23, 2017 | London | Feb 13, 2017 (Feb 6, 2017) | 2017062120170623 | 20170213
SEAA | Euromicro Conference on Software Engineering and Advanced Applications | 31 | B1 | Aug 30, 2017 - Sep 1, 2017 | Vienna | Apr 3, 2017 | 2017083020170901 | 20170403
SEAL | International Conference on Simulated Evolution and Learning | 11 | B3 | Nov 10, 2017 - Nov 13, 2017 | Shenzhen | May 20, 2017 | 2017111020171113 | 20170520
SEAMS | International Symposium on Software engineering for adaptive and self-managing systems | 12 | B3 | May 22, 2017 - May 23, 2017 | Buenos Aires, Argentina | Jan 13, 2017 | 2017052220170523 | 20170113
SEAS | Workshop on Software Engineering for Agent-Oriented Systems | 5 | B4 |  |  |  | z | z
SEA_A | Software Engineering and Applications | 7 | B4 |  |  |  | z | z
SEBIZ | Applications and Business Aspects of the Semantic Web | 2 | B5 |  |  |  | z | z
SEC | IFIP International Information Security Conference | 28 | B1 | Mar 27, 2017 - Mar 29, 2017 | Taipei, Taiwan | Nov 15, 2016 (Oct 31, 2016) | 2017032720170329 | 20161115
SECIII | Social Ethical and Cognitive Issues on Informatics and ICT | 2 | B5 |  |  |  | z | z
SECON | IEEE Conference on Sensor Mesh and Ad Hoc Communications and Networks | 30 | B1 | Jun 11, 2017 - Jun 13, 2017 | San Diego, CA, USA | Dec 22, 2016 (Dec 15, 2016) | 2017061120170613 | 20161222
SECPERU | International Workshop on Security Privacy and Trust in Pervasive and Ubiquitous Computing | 10 | B4 |  |  |  | z | z
SECRYPT | International Conference on Information Security and Cryptology | 9 | B4 | Jul 26, 2017 - Jul 28, 2017 | Madrid, Spain | Mar 2, 2017 | 2017072620170728 | 20170302
SECURECOMM | Security and Privacy in Communication Networks | 25 | B1 | Oct 22, 2017 - Oct 24, 2017 | Niagara Falls, Canada | Jun 21, 2017 | 2017102220171024 | 20170621
SECURITY | Usenix Security Symposium | 81 | A1 | N/A | N/A | Sep 1, 2017 | z | 20170901
SECURWARE | International Conference on Emerging Security Information Systems and Technologies | 11 | B3 | Sep 10, 2017 - Sep 14, 2017 | Rome, Italy | May 25, 2017 | 2017091020170914 | 20170525
SEDE | Software Engineering and Data Engineering | 5 | B4 | Oct 2, 2017 - Oct 4, 2017 | San Diego, CA, USA | Jun 30, 2017 | 2017100220171004 | 20170630
SEFM | IEEE International Conference on Software Engineering and Formal Methods | 26 | B1 | Sep 4, 2017 - Sep 8, 2017 | Trento, Italy | Apr 13, 2017 (Apr 6, 2017) | 2017090420170908 | 20170413
SEKE | International Conference on Software Engineering and Knowledge Engineering | 35 | B1 | Jul 5, 2017 - Jul 7, 2017 | Pittsburgh, PA 15238 USA | Mar 1, 2017 | 2017070520170707 | 20170301
SELMAS | International Workshop on Software Engineering for Large-Scale Multi- Agent Systems | 11 | B3 |  |  |  | z | z
SEMAPRO | International Conference on Advances in Semantic Processing | 5 | B4 | Nov 12, 2017 - Nov 16, 2017 | Barcelona, Spain | Jun 25, 2017 | 2017111220171116 | 20170625
SemDial | Workshop on the Semantics and Pragmatics of Dialogue | 3 | B5 | Aug 15, 2017 - Aug 17, 2017 | Saarbrücken | May 7, 2017 | 2017081520170817 | 20170507
SemEval | International Workshop on Semantic Evaluations | 6 | B4 | Aug 3, 2017 - Aug 4, 2017 | Vancouver, Canada | Feb 27, 2017 (Jan 9, 2017) | 2017080320170804 | 20170227
SEMISH | Seminário Integrado de Software e Hardware | 9 | B4 |  |  |  | z | z
SemWiki | Semantic Wiki Workshop | 14 | B3 |  |  |  | z | z
SENSYS | ACM Conference on Embedded Networked Sensor Systems | 86 | A1 | Jun 11, 2017 - Aug 11, 2017 | Delft, The Netherlands | Oct 4, 2017 (Mar 4, 2017) | 2017061120170811 | 20171004
SERA | ACIS International Conference on Software Engineering Research Management and Applications | 14 | B3 |  |  |  | z | z
SERP | International Conference on Software Engineering Research & Practice | 17 | B2 |  |  |  | z | z
SERVICES | IEEE Congress on Services | 16 | B2 | Nov 12, 2017 - Nov 16, 2017 | Barcelona, Spain | Jun 25, 2017 | 2017111220171116 | 20170625
ServiceWave | ServiceWave | 9 | B4 |  |  |  | z | z
SESS | Software Engineering for Secure Systems | 7 | B4 |  |  |  | z | z
SEW | Annual Software Engineering Workshop | 20 | B2 | Sep 3, 2017 - Sep 6, 2017 | Prague, Czech Republic | May 10, 2017 | 2017090320170906 | 20170510
SForum | Microelectronics Students Forum | 1 | B5 |  |  |  | z | z
SGP | Eurographics Symposium on Geometry Processing | 7 | B4 | Jul 1, 2017 - Jul 5, 2017 | London, UK | Apr 10, 2017 (Apr 5, 2017) | 2017070120170705 | 20170410
SIBGRAPI | Conference on Graphics Patterns and Images | 28 | B1 | Oct 17, 2017 - Oct 20, 2017 | Brazil | Mar 27, 2017 | 2017101720171020 | 20170327
SIES | International Symposium on Industrial Embedded Systems | 13 | B3 |  |  |  | z | z
SIGCOMM | ACM Special Interest Group on Data Communications Conference | 145 | A1 | Aug 21, 2017 - Aug 25, 2017 |  Los Angeles, CA, USA | Jan 27, 2017 (Jan 20, 2017) | 2017082120170825 | 20170127
SIGCSE | ACM Technical Symposium on Computer Science Education | 53 | A2 | Mar 8, 2017 - Mar 11, 2017 | Seattle, Washington, USA | Aug 26, 2016 | 2017030820170311 | 20160826
SIGdial | Workshop on Discourse and Dialogue | 15 | B2 | Aug 15, 2017 - Aug 17, 2017 | Saarbrücken, Germany | Apr 18, 2017 | 2017081520170817 | 20170418
SIGDOC | ACM International Conference on Design of Communication | 24 | B1 |  |  |  | z | z
SIGIR | Annual International ACM SIGIR Conference on Research & Development in Information Retrieval | 122 | A1 | Aug 7, 2017 - Aug 11, 2017 | Shinjuku, Tokyo, Japan. | Jan 24, 2017 (Jan 17, 2017) | 2017080720170811 | 20170124
SIGITE | Information Technology Education | 15 | B3 | Oct 4, 2017 - Oct 7, 2017 | Rochester, New York | May 15, 2017 | 2017100420171007 | 20170515
SIGMETRICS | ACM Conference on Measurement and Modelling of Computing Systems | 73 | A1 |  |  |  | z | z
SIGMOD | ACM SIGMOD International Conference on Management of Data Conference | 151 | A1 | May 14, 2017 - May 19, 2017 | Raleigh | Jul 22, 2016 (Jul 15, 2016) | 2017051420170519 | 20160722
SIGSEM | ACL Workshop on Prepositions | 2 | B4 |  |  |  | z | z
SIIE | Simpósio Internacional de Informática Educativa | 6 | B4 |  |  |  | z | z
SIIT | International Conference on Standardization and Innovation in Information Technology | 10 | B4 |  |  |  | z | z
SIN | International Conference on Security of Information and Networks | 7 | B4 | Oct 13, 2017 - Oct 15, 2017 | Jaipur, India | Jul 17, 2017 | 2017101320171015 | 20170717
SInT | IEEE International Symposium on Information Theory | 60 | A1 |  |  |  | z | z
SIPS | IEEE Workshop on Signal Processing Systems Design and Implementation | 15 | B3 |  |  |  | z | z
SIROCCO | Colloquium on Structural Information and Communication Complexity | 21 | B2 | Jun 19, 2017 - Jun 22, 2017 | Porquerolles, France | Apr 1, 2017 | 2017061920170622 | 20170401
SITE | Society for Information Technology and Teacher Education Conference | 10 | B4 | Mar 13, 2017 - Mar 14, 2017 | San Francisco, CA | Dec 7, 2016 | 2017031320170314 | 20161207
SLE | International Conference on Software Language Engineering | 3 | B5 | Oct 23, 2017 - Oct 24, 2017 | Vancouver, Canada | Jun 16, 2017 (Jun 9, 2017) | 2017102320171024 | 20170616
SLS | Engineering Stochastic Local Search Algorithms | 9 | B4 |  |  |  | z | z
SLT | IEEE Spoken Language Technology Workshop | 16 | B2 |  |  |  | z | z
SMAP | International Workshop on Semantic and Social Media Adaptation and Personalization | 9 | B4 | Jul 9, 2017 - Jul 10, 2017 | Bratislava, Slovakia | Mar 17, 2017 (Mar 12, 2017) | 2017070920170710 | 20170317
SMC | IEEE International Conference on Systems Man and Cybernetics | 19 | B2 | Jul 5, 2017 - Jul 8, 2017 | Espoo, Finland | Feb 24, 2017 (Feb 17, 2017) | 2017070520170708 | 20170224
SMC_A | Sound and Music Computing Conference | 16 | B2 |  |  |  | z | z
SMI | IEEE International Conference on Shape Modeling and Applications | 38 | A2 | Jun 21, 2017 - Jun 23, 2017 | Berkeley, California, USA | Apr 3, 2017 | 2017062120170623 | 20170403
SOAS | Self-Organization and Autonomous Systems in Computing and Communications | 8 | B4 |  |  |  | z | z
SOC | International Symposium on System-on-Chip | 10 | B4 | Sep 18, 2017 - Sep 20, 2017 | Korea University, Seoul, Korea | May 20, 2017 | 2017091820170920 | 20170520
SOCASE | International Workshop on Service-Oriented Computing: Agents Semantics and Engineering | 7 | B4 |  |  |  | z | z
SOCC | IEEE International SOC Conference | 20 | B2 | Sep 25, 2017 - Sep 27, 2017 | Santa Clara, CA | May 5, 2017 (Apr 28, 2017) | 2017092520170927 | 20170505
SoCG | Annual ACM Symposium on Computational Geometry | 49 | A2 | Jul 4, 2017 - Jul 7, 2017 | Brisbane, Australia | Dec 5, 2016 (Nov 28, 2016) | 2017070420170707 | 20161205
SocialCom | International Conference on Social Computing and its Applications | 10 | B4 |  |  |  | z | z
SOCINFO | International Workshop on Social Informatics | 4 | B4 | Sep 13, 2017 - Sep 15, 2017 | Oxford, UK | May 31, 2017 | 2017091320170915 | 20170531
SOCO | International Conference on Soft Computing Models in Industrial and Environmental Applications | 4 | B4 |  |  |  | z | z
SODA | ACM-SIAM Symposium on Discrete Algorithms | 85 | A1 | Jan 16, 2017 - Jan 19, 2017 | Barcelona, Spain | Jul 13, 2016 (Jul 6, 2016) | 2017011620170119 | 20160713
SOFSEM | Conference on Current Trends in Theory and Practice of Computer Science | 22 | B1 | Jan 16, 2017 - Jan 20, 2017 | Limerick | TBD | 2017011620170120 | z
SOFTCOMM | International Conference on Software Telecommunications and Computer Networks | 8 | B4 |  |  |  | z | z
Softvis | Software Visualization | 28 | B1 |  |  |  | z | z
SoICT | Symposium on Information and Communication Technology | 3 | B5 | Dec 7, 2017 - Dec 8, 2017 | NhaTrang, Vietnam | Aug 14, 2017 | 2017120720171208 | 20170814
SoMeT | Software Methodologies Tools and Techniques | 10 | B4 | Sep 26, 2017 - Sep 28, 2017 | KitaKyushu, Japan | Mar 31, 2017 | 2017092620170928 | 20170331
SOSE | International Symposium on Service-Oriented System Engineering | 16 | B2 | Apr 6, 2017 - Apr 9, 2017 | San Francisco, USA | Nov 1, 2016 | 2017040620170409 | 20161101
SOSP | ACM Symposium on Operating Systems Principles | 71 | A1 | Oct 29, 2017 - Oct 31, 2017 | Shanghai | Apr 21, 2017 (Apr 14, 2017) | 2017102920171031 | 20170421
SOUPS | Symposium On Usable Privacy and Security | 29 | B1 | Jul 12, 2017 - Jul 14, 2017 | Santa Clara, CA | Mar 7, 2017 (Mar 1, 2017) | 2017071220170714 | 20170307
SPAA | ACM Symposium on Parallel Algorithms and Architectures | 46 | A2 | Jul 24, 2017 - Jul 27, 2017 | Washington D.C., USA | Feb 9, 2017 | 2017072420170727 | 20170209
SPECTS | International Symposium on Performance Evaluation of Computer and Telecommunication Systems | 8 | B4 | Jul 9, 2017 - Jul 12, 2017 | Seattle, WA, USA | Feb 20, 2017 | 2017070920170712 | 20170220
SPICE | International Software Process Improvement and Capability Determination Conference | 3 | B4 | Oct 4, 2017 - Oct 5, 2017 | Palma de Mallorca, Spain | Jun 2, 2017 | 2017100420171005 | 20170602
SPIRE | Symposium on String Processing and Information Retrieval | 33 | B1 |  |  |  | z | z
SPL | Southern Conference on Programmable Logic | 6 | B4 |  |  |  | z | z
SPLC | Software Product Line Conference | 43 | A2 | Sep 25, 2017 - Sep 29, 2017 | Sevilla, Spain | Mar 24, 2017 (Mar 17, 2017) | 2017092520170929 | 20170324
SPW | International Workshop on Security Protocols | 5 | B4 |  |  |  | z | z
SQC | Software and Systems Quality Conferences | 2 | B5 |  |  |  | z | z
SQM | International Conference on Software Quality Management | 7 | B4 |  |  |  | z | z
SRDS | IEEE International Symposium on Reliable Distributed Systems | 40 | A2 | Sep 26, 2017 - Sep 29, 2017 | Hong Kong, China | Apr 23, 2017 | 2017092620170929 | 20170423
SREIS | Symposium on Requirements Engineering for Information Security | 5 | B4 |  |  |  | z | z
SRUTI | Steps to Reducing Unwanted Traffic on the Internet Workshop | 8 | B4 |  |  |  | z | z
SSBSE | International Symposium on Search Based Software Engineering | 7 | B4 | Sep 9, 2017 - Sep 11, 2017 | Paderborn, Germany | Apr 7, 2017 (Mar 31, 2017) | 2017090920170911 | 20170407
SSC | IFAC Conference on System Structure and Control | 5 | B4 | Jun 26, 2017 - Jun 28, 2017 | New York, USA | Mar 15, 2017 | 2017062620170628 | 20170315
SSDBM | International Conference on Scientific and Statistical Database Management | 38 | A2 | Jun 27, 2017 - Jun 29, 2017 | Chicago, IL | Feb 10, 2017 (Feb 3, 2017) | 2017062720170629 | 20170210
SSI | Symposium on System and Information Security | 7 | B4 | Jun 29, 2017 - Jun 30, 2017 | Moscow, Russia | Apr 1, 2017 (May 30, 2017) | 2017062920170630 | 20170401
SSIRI | Secure System Integration and Reliability Improvement | 7 | B4 |  |  |  | z | z
SSIRI-C | IEEE International Conference on Secure Software Integration and Reliability Improvement Companion | 6 | B4 | Jul 25, 2017 - Jul 27, 2017 | Washington, D.C., USA | Feb 15, 2017 | 2017072520170727 | 20170215
SSPR | International Workshop on Structural and Syntactic Pattern Recognition | 25 | B1 |  |  |  | z | z
SSR | ACM SIGSOFT Working Conference on Software Reusability | 3 | B4 | Jun 29, 2017 - Jun 30, 2017 | Moscow, Russia | Apr 1, 2017 (May 30, 2017) | 2017062920170630 | 20170401
SSRR | IEEE International Workshop on Safety Security & Rescue Robots | 6 | B4 |  |  |  | z | z
SSST | Workshop on Syntax Semantics and Structure in Statistical Translation | 1 | B5 |  |  |  | z | z
SSTD | International Symposium on Spatial and Temporal Databases | 33 | B1 |  |  |  | z | z
STACS | International Symposium on Theoretical Aspects of Computer Science | 44 | A2 | Mar 8, 2017 - Mar 11, 2017 | Hannover, Germany | Sep 25, 2016 | 2017030820170311 | 20160925
STAIRS | Starting Artificial Intelligence Research Symposium | 8 | B4 |  |  |  | z | z
STEP | IEEE International Workshop on Software Technology and Engineering Practice | 14 | B3 |  |  |  | z | z
STFSSD | Software Technologies for Future Dependable Distributed Systems | 4 | B4 |  |  |  | z | z
STIL | Brazilian Symposium in Information and Human Language Technology | 5 | B4 | Oct 2, 2017 - Oct 4, 2017 | Uberlândia/MG | May 26, 2017 | 2017100220171004 | 20170526
STOC | ACM Symposium on Theory of Computing | 100 | A1 | Jun 17, 2017 - Jun 23, 2017 | Montreal, Canada | Nov 2, 2016 | 2017061720170623 | 20161102
Storage | IEEE Symposium on Mass Storage Systems | 28 | B1 | Nov 12, 2017 - Nov 17, 2017 | Denver, CO | Apr 3, 2017 (Mar 20, 2017) | 2017111220171117 | 20170403
SugarLoafPlop | Latin American Conference on Pattern Languages of Programming | 3 | B5 |  |  |  | z | z
SUPER | ACM/IEEE Supercomputing Conference | 76 | A1 | Jul 21, 2017 - Jul 21, 2017 | Honolulu, Hawaii | Apr 24, 2017 | 2017072120170721 | 20170424
SVR | Symposium on Virtual and Augmented Reality | 9 | B4 |  |  |  | z | z
SWAP | Workshop on Semantic Web Applications and Perspectives | 13 | B3 |  |  |  | z | z
SWAT | Scandinavian Symposium and Workshops on Algorithm Theory | 26 | B1 |  |  |  | z | z
SWEL | Workshop on Ontologies and Social Semantic Web for Intelligent Educational Systems | 4 | B4 |  |  |  | z | z
SWWS | Semantic Web and Web Services | 38 | A2 |  |  |  | z | z
SYROCO | International IFAC Symposium on Robot Control | 11 | B3 |  |  |  | z | z
T4E | International Workshop on Technology for Education | 3 | B5 |  |  |  | z | z
TABLEAUX | International Conference on Theorem Proving with Analytic Tableaux and Related Methods | 24 | B1 | Sep 26, 2017 - Sep 29, 2017 | Brasília, Brazil | Apr 25, 2017 (Apr 18, 2017) | 2017092620170929 | 20170425
TAC | Text Analysis Conference | 20 | B2 | Sep 6, 2017 - Sep 8, 2017 | Marseille, France | May 1, 2017 | 2017090620170908 | 20170501
TACAS | International Conference on Tools and Algorithms for the Construction and Analysis of Systems | 68 | A1 |  |  |  | z | z
TAG+ | International Workhop on Tree Adjoining Grammar and Related Formalisms | 3 | B4 |  |  |  | z | z
TALN | Traitement Automatique des Langues Naturelles | 7 | B4 |  |  |  | z | z
TAP | Tests and Proofs | 12 | B3 | Jul 19, 2017 - Jul 20, 2017 | Marburg, Germany | Feb 24, 2017 (Feb 17, 2017) | 2017071920170720 | 20170224
TaPP | USENIX Workshop on the Theory and Practice of Provenance | 4 | B4 |  |  |  | z | z
TARK | Conference on Theoretical Aspects of Rationality and Knowledge | 19 | B2 | Jun 24, 2017 - Jun 26, 2017 | University of Liverpool, UK | Apr 3, 2017 | 2017062420170626 | 20170403
TASE | IEEE/IFIP International Symposium on Theoretical Aspects of Software Engineering | 11 | B3 | Sep 13, 2017 - Sep 15, 2017 | France | Mar 19, 2017 (Mar 12, 2017) | 2017091320170915 | 20170319
TAU | IEEE International Workshop on Timing Issues | 4 | B4 |  |  |  | z | z
TCC | Theory of Cryptography Conference | 47 | A2 |  |  |  | z | z
TCGOV | International Conference on E-Government | 9 | B4 |  |  |  | z | z
TCoB | International Workshop on Technologies for Collaborative Business Process Management | 2 | B5 |  |  |  | z | z
TCS | IFIP International Conference on Theoetical Computer Science | 25 | B1 |  |  |  | z | z
TEAA | International Conference on Trends in Enterprise Application Architecture | 5 | B4 |  |  |  | z | z
TEAR | Trends in Enterprise Architecture Research | 3 | B5 | Oct 10, 2017 - Oct 13, 2017 | Québec City, Canada | May 7, 2017 | 2017101020171013 | 20170507
TECHEDUCA | International Conference on Technology Enhanced Learning Quality of Teaching and Education Reform | 4 | B4 |  |  |  | z | z
TEI | Tangible and Embedded Interaction | 25 | B1 | Mar 20, 2017 - Mar 23, 2017 | Yokohama, Japan | Aug 2, 2016 | 2017032020170323 | 20160802
TEL | Technology for Education and Learning | 1 | B5 | Sep 12, 2017 - Sep 15, 2017 | Tallinn, Estonia | Apr 10, 2017 (Apr 3, 2017) | 2017091220170915 | 20170410
TES | International Workshop on Technologies for E-Services | 21 | B2 |  |  |  | z | z
TextGraphs | Graph-based Methods for Natural Language Processing | 10 | B4 | Aug 3, 2017 - Aug 4, 2017 | Vancouver, Canada  | Apr 21, 2017 | 2017080320170804 | 20170421
TFP | Trends in Functional Programming | 6 | B3 |  |  |  | z | z
TIDSE | International Conference on Technology for Interactive Digital Storytelling and Entertainment | 18 | B2 |  |  |  | z | z
TIME | International Symposium on Temporal Representation and Reasoning | 23 | B1 | Aug 16, 2017 - Aug 18, 2017 | Hsinchu, Taiwan | Apr 14, 2017 | 2017081620170818 | 20170414
TISE | Taller Internacional de Software Educativo | 4 | B4 |  |  |  | z | z
TLCA | Typed Lambda Calculus and Applications | 23 | B1 |  |  |  | z | z
TLT | International Workshop on Treebanks and Linguistic Theories | 7 | B4 | Jan 20, 2017 - Jan 21, 2017 | Bloomington, IN, USA | Oct 9, 2016 | 2017012020170121 | 20161009
TMRA | International Conference on Topic Maps Research and Applications | 1 | B5 |  |  |  | z | z
TOOLS | Technology of Object-Oriented Languages and Systems Conferences | 27 | B1 | Nov 6, 2017 - Nov 8, 2017 | Boston, MA, USA | Jul 15, 2017 | 2017110620171108 | 20170715
TPDL | International Conference on Theory and Practice of Digital Libraries | 34 | B1 | Sep 18, 2017 - Sep 21, 2017 | Thessaloniki | Apr 7, 2017 | 2017091820170921 | 20170407
TPHOLs | International Conference on Theorem Proving in Higher Order Logics | 35 | B1 |  |  |  | z | z
TREC | Text Retrieval Conference | 51 | A2 | Nov 13, 2017 - Nov 15, 2017 | Gaithersburg, MD | Apr 3, 2017 | 2017111320171115 | 20170403
TRIDENTCOM | International Conference on Testbeds and Research Infrastructure for the Development of Networks and Communities | 22 | B1 |  |  |  | z | z
TRUST | Trust in Agent Societies | 8 | B4 | Aug 1, 2017 - Aug 4, 2017 | Sydney, Australia | Mar 1, 2017 | 2017080120170804 | 20170301
TSD | International Conference on Text Speech and Dialogue | 23 | B1 | Aug 27, 2017 - Aug 31, 2017 | Prague | Mar 31, 2017 | 2017082720170831 | 20170331
UAI | Conference in Uncertainty in Artifical Intelligence | 70 | A1 | Aug 11, 2017 - Aug 15, 2017 | Sydney, Australia | Mar 1, 2017 | 2017081120170815 | 20170301
UBICOMM | International Conference on Mobile Ubiquitous Computing Systems Services and Technologies | 11 | B3 | Nov 12, 2017 - Nov 16, 2017 | Barcelona, Spain | Jun 25, 2017 | 2017111220171116 | 20170625
UbiComp | International Conference on Ubiquitous Computing | 70 | A1 | Sep 11, 2017 - Sep 15, 2017 | Maui, HI, USA | Apr 1, 2017 | 2017091120170915 | 20170401
UCAS | International Workshop on Unique Chips and Systems | 4 | B4 |  |  |  | z | z
UDMS | Urban Data Management Symposium | 8 | B4 |  |  |  | z | z
UIC | International Conference on Ubiquitous Intelligence and Computing | 17 | B2 | Aug 4, 2017 - Aug 8, 2017 | San Francisco, USA | Mar 10, 2017 (Jun 10, 2017) | 2017080420170808 | 20170310
UIST | ACM Symposium on USer Interface Software and Technology | 77 | A1 | Oct 22, 2017 - Oct 25, 2017 | Quebec City, Canada | TBD | 2017102220171025 | z
UMAP | International Conference on User Modeling Adaption and Personalization | 35 | B1 | Jul 9, 2017 - Jul 12, 2017 | Bratislava, Slovakia | Feb 17, 2017 | 2017070920170712 | 20170217
UNISCON | International United Information Systems Conference | 9 | B4 |  |  |  | z | z
URSW | Uncertainty Reasoning for the Semantic Web | 10 | B4 |  |  |  | z | z
USENIX | USENIX Annual Technical Conference | 67 | A1 | Aug 16, 2017 - Aug 18, 2017 | Vancouver, BC. | Feb 16, 2017 | 2017081620170818 | 20170216
USITS | USENIX Symposium on Internet Technologies and Systems | 31 | B1 |  |  |  | z | z
VAMOS | International Workshop on Variability Modelling of Software-intensive Systems | 14 | B3 | Feb 1, 2017 - Feb 3, 2017 | Eindhoven, The Netherlands | Nov 14, 2016 (Nov 10, 2016) | 2017020120170203 | 20161114
VAST | IEEE Symposium on Visual Analytics Science and Technology | 24 | B1 |  |  |  | z | z
VCIP | SPIE International Conference on Visual Communications and Image Processing | 31 | B1 | Dec 10, 2017 - Dec 13, 2017 | St. Petersburg, FL, USA | May 26, 2017 | 2017121020171213 | 20170526
VDEA | SPIE Conference on Visual Data Exploration and Analysis | 2 | B5 |  |  |  | z | z
VECIMS | IEEE International Conference on Virtual Environments Human-Computer Interfaces and Measurement Systems | 4 | B4 |  |  |  | z | z
VECPAR | International Meeting on High Performance Computing for Computational Science | 10 | B4 |  |  |  | z | z
VG | IEEE International Workshop on Volume Graphics | 10 | B4 |  |  |  | z | z
VIIP | International Conference on Visualization: Imaging and Image Processing | 11 | B3 |  |  |  | z | z
Virtual | International Workshop on Virtual Rehabilitation | 10 | B4 | Jun 12, 2017 - Jun 15, 2017 | Ugento (LE), Italy | Mar 11, 2017 | 2017061220170615 | 20170311
Vis | IEEE Visualization Conference | 4 | B4 |  |  |  | z | z
VISAPP | International Conference on Computer Vision Theory and Applications | 14 | B3 | Feb 27, 2017 - Mar 1, 2017 | Porto, Portugal | Oct 20, 2016 | 2017022720170301 | 20161020
VISUAL | International Conference on Visual Information Systems | 16 | B2 | Dec 10, 2017 - Dec 13, 2017 | St. Petersburg, FL, USA | May 26, 2017 | 2017121020171213 | 20170526
VIZSEC | IEEE Workshops on Visualization for Computer Security | 23 | B1 | Oct 2, 2017 - Oct 2, 2017 | Phoenix, AZ, USA | Jul 18, 2017 | 2017100220171002 | 20170718
VL/HCC | IEEE Symposium on Visual Languages and Human-Centric Computing | 25 | B1 | Oct 11, 2017 - Oct 14, 2017 | Raleigh, NC | Apr 13, 2017 (Apr 6, 2017) | 2017101120171014 | 20170413
VLFM | Symposium on Visual Languages and Formal Methods | 2 | B4 |  |  |  | z | z
VLSI-DAT | International Symposium on VLSI Design Automation and Test | 9 | B4 | Jul 3, 2017 - Jul 5, 2017 | Bochum | Mar 10, 2017 | 2017070320170705 | 20170310
VLSI-SOC | IFIP International Conference on Very Large Scale Integration | 15 | B3 | Oct 23, 2017 - Oct 25, 2017 | Abu Dhabi | May 12, 2017 (May 1, 2017) | 2017102320171025 | 20170512
VLSID | International Conference on VLSI Design | 40 | A2 |  |  |  | z | z
VMCAI | Verification Model Checking and Abstract Interpretation | 40 | A2 | Jan 15, 2017 - Jan 17, 2017 |  Paris, France | Sep 21, 2016 (Sep 18, 2016) | 2017011520170117 | 20160921
VMIL | Workshop on Virtual Machines and Intermediate Languages | 7 | B4 |  |  |  | z | z
VNC | IEEE Vehicular Networking Conference  | 6 | B4 |  |  |  | z | z
VR | IEEE Virtual Reality Conference | 40 | A2 | Jun 28, 2017 - Jun 30, 2017 | Nara, Japan | Apr 16, 2017 (May 30, 2017) | 2017062820170630 | 20170416
VRCAI | International Conference on Virtual Reality Continuum and its Applications in Industry | 17 | B2 |  |  |  | z | z
VRCIA | ACM SIGGRAPH International Conference on Virtual-Reality Continuum and its Applications in Industry | 18 | B2 |  |  |  | z | z
VRST | ACM Virtual Reality Software and Technology | 32 | B1 | Nov 8, 2017 - Nov 10, 2017 | Gothenburg, Sweden | Jun 30, 2017 | 2017110820171110 | 20170630
VS-Games | IEEE Conference in Serious Games and Virtual Worlds | 7 | B4 | N/A | N/A | Oct 10, 2017 | z | 20171010
VSMM | International Conference on Virtual Systems and MultiMedia | 9 | B3 | Oct 31, 2017 - Nov 2, 2017 | Dublin, Ireland | Jun 2, 2017 | 2017103120171102 | 20170602
VSSN | ACM International Workshop on Video Surveillance and Sensor Networks | 20 | B2 |  |  |  | z | z
VTC | IEEE Vehicular Technology Conference | 98 | A1 | Sep 24, 2017 - Sep 27, 2017 | Toronto, Canada | Mar 13, 2017 | 2017092420170927 | 20170313
VTS | IEEE VLSI Test Symposium | 47 | A2 |  |  |  | z | z
W2GIS | International Workshop on Web and Wireless Geographical Information Systems | 11 | B3 | May 8, 2017 - May 9, 2017 | Shanghai , China | Dec 10, 2016 | 2017050820170509 | 20161210
W4A | International Cross-Disciplinary Conference on Web Accessibility | 26 | B1 |  |  |  | z | z
WAAMD | Workshop em Algoritmos e Aplicações de Mineração de Dados | 4 | B4 |  |  |  | z | z
WABI | Workshop on Algorithms in Bioinformatics | 34 | B1 |  |  |  | z | z
WAC | Web as Corpus Workshop | 6 | B4 | Aug 21, 2017 - Aug 23, 2017 | London, UK | Apr 5, 2017 (Mar 29, 2017) | 2017082120170823 | 20170405
WACC | WSEAS Applied Computing Conference | 3 | B5 | May 14, 2017 - May 17, 2017 | Madrid, Spain | Jan 25, 2017 | 2017051420170517 | 20170125
WACERTS | Workshop on Architectures for Cooperative Embedded Real-Time Systems | 2 | B5 |  |  |  | z | z
WACV | IEEE Workshop on Applications of Computer Vision | 33 | B1 | Mar 24, 2017 - Mar 31, 2017 | Santa Rosa, California | Oct 3, 2017 | 2017032420170331 | 20171003
WADS | Workshop on Algorithms and Data Structures | 25 | B1 | Jul 31, 2017 - Aug 2, 2017 | St. John’s, Canada | Feb 20, 2017 | 2017073120170802 | 20170220
WADS_A | Workshop on Architecting Dependable Systems | 24 | B1 |  |  |  | z | z
WAFR | Workshop on the Algorithmic Foundations of Robotics | 5 | B4 |  |  |  | z | z
WAIM | Interational Conference on Web Age Information Management | 21 | B2 | Jul 7, 2017 - Jul 9, 2017 | Beijing | Feb 17, 2017 (Feb 10, 2017) | 2017070720170709 | 20170217
WAIS | International Workshop on Artificial Intelligence and Statistics | 10 | B3 |  |  |  | z | z
WAMCA | Workshop on Architecture for Multi-Core Applications | 1 | B5 |  |  |  | z | z
WAMPS | Workshop anual do MPS.BR pessoal enviou meta-dados separadamente) | 2 | B5 |  |  |  | z | z
WAOA | Workshop on Approximation and Online Algorithms | 4 | B4 | Sep 7, 2017 - Sep 8, 2017 | Vienna, Austria | Jun 27, 2017 | 2017090720170908 | 20170627
WASA | International Conference on Wireless Algorithms Systems and Applications | 9 | B4 | Apr 3, 2017 - Apr 3, 2017 | Gothenburg, Sweden | Feb 23, 2017 | 2017040320170403 | 20170223
WASSA | Workshop on Computational Approaches to Subjectivity and Sentiment Analysis | 1 | B5 |  |  |  | z | z
WBC | Web Based Communities | 6 | B4 |  |  |  | z | z
WBE | International Symposium on Web-based Education | 1 | B5 |  |  |  | z | z
WBPM | Workshop Brasileiro de Gestão de Processos de Negócio | 1 | B5 |  |  |  | z | z
WCAE | Workshop on Computer Architecture Education | 6 | B4 |  |  |  | z | z
WCAMA | Workshop de Computação Aplicada à Gestão do Meio Ambiente e Recursos Naturais | 1 | B5 | Jul 3, 2017 - Jul 6, 2017 | São Paulo, Brazil | Mar 12, 2017 | 2017070320170706 | 20170312
WCCE | IFIP World Conference on Computers in Education | 8 | B4 |  |  |  | z | z
WCCI | IEEE World Congress on Computational Intelligence | 8 | B4 |  |  |  | z | z
WCCM | World Congress on Computational Mechanics | 11 | B3 |  |  |  | z | z
WCGA | Workshop de Computação em Grade e Aplicações | 4 | B4 |  |  |  | z | z
WCGE | Workshop de Computação Aplicada em Governo Eletrônico | 1 | B5 |  |  |  | z | z
WCNC | IEEE Wireless and Communications and Networking Conference | 34 | A2 | Mar 19, 2017 - Mar 22, 2017 | San Francisco, CA, USA | Sep 30, 2016 | 2017031920170322 | 20160930
WCOLDE | ICDE World Conference on Open Learning and Distance Education | 4 | B4 |  |  |  | z | z
WCRE | IEEE Working Conference on Reverse Engineering | 43 | A2 |  |  |  | z | z
WCSMO | World Congress on Structural and Multidisciplinary Optimization | 53 | A2 |  |  |  | z | z
WCW | Web Caching Workshop | 9 | B3 |  |  |  | z | z
WD | IFIP Wireless Days | 8 | B4 |  |  |  | z | z
WDBC | Worshop Brasileiro de Desenvolvimento Baseado em Componentes | 2 | B5 |  |  |  | z | z
WDDD | Workshop on Duplicating Deconstructing and Debunking | 10 | B4 |  |  |  | z | z
WDDDM | Workshop on Dependable Distributed Data Management | 7 | B4 |  |  |  | z | z
WDL | Workshop de Bibliotecas Digitais | 1 | B5 |  |  |  | z | z
WEAC | Workshop sobre Educação em Arquitetura de Computadores | 3 | B5 |  |  |  | z | z
Web3D | International Conference on 3D Web Technology | 31 | B1 | Jun 5, 2017 - Jun 7, 2017 | Brisbane, Australia | Feb 27, 2017 | 2017060520170607 | 20170227
WebDB | International Workshop on the Web and Databases | 34 | B1 | May 14, 2017 - May 14, 2017 | Chicago, IL, USA | Feb 24, 2017 | 2017051420170514 | 20170224
WEBIST | International Conference on Web Information Systems and Technologies | 11 | B3 | Apr 25, 2017 - Apr 27, 2017 | Porto, Portugal | Nov 23, 2016 | 2017042520170427 | 20161123
WebKDD | Web Mining and Web Usage Analysis | 4 | B4 |  |  |  | z | z
WEBMEDIA | Brazilian Symposium on Multimedia and the Web | 12 | B3 |  |  |  | z | z
WebNet | World Conference on the WWW and Internet | 16 | B2 |  |  |  | z | z
WebPRES | Web Personalization and Recommender Systems | 2 | B5 |  |  |  | z | z
WEED | Workshop on Energy-Efficient Design | 1 | B5 |  |  |  | z | z
WEI | Workshop sobre Educação em Informática | 9 | B4 |  |  |  | z | z
WEIS | Workshop on the Economics of Information Security | 26 | B1 |  |  |  | z | z
WER | Workshop de Engenharia de Requisitos | 14 | B3 |  |  |  | z | z
WES | International Workshop on Web Services E-Business and the Semantic Web | 16 | B2 |  |  |  | z | z
WESAAC | Workshop-Escola de Sistemas de Agentes seus Ambientes e Aplicações | 1 | B5 |  |  |  | z | z
WETICE | IEEE International Workshops on Enabling Technologies: Infrastructure for Collaborative Enterprises | 33 | B1 | Jun 21, 2017 - Jun 23, 2017 | Poznan, Poland | Feb 26, 2017 | 2017062120170623 | 20170226
WFLP | International Workshop on Functional and Constraint) Logic Programming | 7 | B4 |  |  |  | z | z
WG | International Workshop on Graph-Theoretic Concepts in Computer Science | 25 | B1 | Jul 19, 2017 - Jul 21, 2017 | Philadelphia, PA, USA | Mar 6, 2017 | 2017071920170721 | 20170306
WGRS | Workshop de Gerência e Operação de Redes e Serviços | 3 | B5 |  |  |  | z | z
WI | IEEE/WIC/ACM International Conference on Web Intelligence | 39 | A2 | Aug 23, 2017 - Aug 26, 2017 | Leipzig, Germany | Mar 12, 2017 | 2017082320170826 | 20170312
WICON | International Conference on Wireless Internet | 13 | B3 |  |  |  | z | z
WICSA | Working IEEE/IFIP Conference on Software Architecture | 32 | B1 |  |  |  | z | z
WIDM | ACM International Workshop on Web Information and Data Management | 32 | B1 |  |  |  | z | z
WIE | Workshop sobre Informática na Escola | 11 | B3 |  |  |  | z | z
WIM | Workshop de Informática Médica | 4 | B4 |  |  |  | z | z
WiMOB | IEEE International Conference on Wireless and Mobile Computing Networking and Communications | 23 | B1 | Oct 9, 2017 - Oct 11, 2017 | Rome, Italy | May 20, 2017 | 2017100920171011 | 20170520
WiOpt | International Symposium on Modeling and Optimization in Mobile Ad Hoc and Wireless Networks | 24 | B1 | May 15, 2017 - May 19, 2017 | Paris | Dec 22, 2016 (Dec 15, 2016) | 2017051520170519 | 20161222
WIS | International Workshop on Wireless Information Systems | 7 | B4 |  |  |  | z | z
WISA | Web Information Systems and Applications Conference | 9 | B4 | Jun 21, 2017 - Jun 24, 2017 | Lisbon, Portugal | Apr 15, 2017 | 2017062120170624 | 20170415
WISE | International Conference on Web Information Systems Engineering | 35 | B1 | Oct 7, 2017 - Oct 11, 2017 | Moscow, Russia | May 16, 2017 (May 9, 2017) | 2017100720171011 | 20170516
WISEC | Wireless Network Security | 22 | B1 | Jul 18, 2017 - Jul 20, 2017 | Boston, USA | Mar 20, 2017 (Mar 13, 2017) | 2017071820170720 | 20170320
WISM | Web Information Systems Modeling | 6 | B4 |  |  |  | z | z
WISP | IEEE International Workshop on Intelligent Signal Processing | 13 | B3 |  |  |  | z | z
WISTDCS | WOMBAT Workshop on Information Security Threats Data Collection and Sharing | 4 | B4 |  |  |  | z | z
WITS | Workshop on Information Technologies and Systems | 11 | B3 |  |  |  | z | z
WKDD | International Workshop on Knowledge Discovery and Data Mining | 10 | B4 |  |  |  | z | z
WMIS | Workshop on Mobile Information Systems | 12 | B3 |  |  |  | z | z
WMT | Workshop on Statistical Machine Translation | 3 | B5 |  |  |  | z | z
WMUTE | IEEE Wireless Mobile and Ubiquitous Technologies in Education | 14 | B3 |  |  |  | z | z
WOCC | Annual Wireless and Optical Communications Conference | 20 | B2 |  |  |  | z | z
WOCN | IFIP International Conference on Wireless and Optical Communications Networks | 19 | B2 |  |  |  | z | z
WoLLIC | Workshop on Logic Language Information and Computation | 11 | B3 |  |  |  | z | z
WOMSDE | Workshop on Ontologies and Metamodels in Software Engineering WOMSDE) | 5 | B4 |  |  |  | z | z
WONS | International Conference on Wireless on Demand Network Systems and Service | 9 | B4 |  |  |  | z | z
WORDS | IEEE International Workshop on Object-Oriented Real-Time Dependable Systems | 24 | B1 | Nov 10, 2016 - Nov 11, 2016 | Bucharest | Sep 18, 2016 | 2016111020161111 | 20160918
WORKS | Workshop on Workflows in Support of Large-Scale Science | 9 | B4 | Nov 13, 2017 - Nov 13, 2017 | Denver, Colorado, USA | Jul 30, 2017 | 2017111320171113 | 20170730
WorldHAPTICS | Joint EUROHAPTICS Conference and Symposium on Haptics Interfaces for Virtual Environment and Teleoperator Systems | 26 | B1 |  |  |  | z | z
WORM | ACM Workshop on Recurring Malcode | 36 | B1 |  |  |  | z | z
WoSAR | International Workshop on Software Aging and Rejuvenation | 4 | B4 | Oct 23, 2017 - Oct 26, 2017 | Toulouse, France | Jul 21, 2017 | 2017102320171026 | 20170721
WoSIDA | Workshop on Autonomic Distributed Systems | 1 | B5 | May 15, 2017 - May 15, 2017 | Belém of Pará, Brazil | Apr 17, 2017 | 2017051520170515 | 20170417
WoWMoM | IEEE International Symposium on a World of Wireless Mobile and Multimedia Networks | 6 | B3 | Jun 12, 2017 - Jun 15, 2017 |  Macao, China | Dec 14, 2016 (Dec 7, 2016) | 2017061220170615 | 20161214
WP2P | Workshop de Peer-to-Peer | 3 | B5 |  |  |  | z | z
WPERFORMANCE | Workshop em Desempenho de Sistemas Computacionais e de Comunicação | 3 | B5 |  |  |  | z | z
WRC | Workshop on Reconfigurable Computing | 4 | B4 |  |  |  | z | z
WRS | International Workshop on Reduction Strategies in Rewriting and Programming | 1 | B5 |  |  |  | z | z
WRT | International Workshop on Refactoring Tools | 6 | B4 |  |  |  | z | z
WRVA | Workshop de Realidade Virtual e Aumentada  | 2 | B5 |  |  |  | z | z
WS-REST | International Workshop on RESTful Design | 6 | B4 | Aug 28, 2017 - Aug 28, 2017 | Lisbon | Jun 30, 2017 | 2017082820170828 | 20170630
WSC | Winter Simulation Conference | 56 | A1 | Sep 3, 2017 - Sep 6, 2017 | Prague, Czech Republic | May 10, 2017 | 2017090320170906 | 20170510
WSCAD-SSC | Simpósio em Sistemas Computacionais | 4 | B4 | Jun 26, 2017 - Jun 28, 2017 | New York, USA | Mar 15, 2017 | 2017062620170628 | 20170315
WSCG | International Conferences in Central Europe on  Computer Graphics Visualization and Computer Vision | 30 | B1 | May 29, 2017 - Jun 2, 2017 | Plzen, Czech Republic | Jan 20, 2017 | 2017052920170602 | 20170120
WSDM | International Conference on Web Search and Data Mining | 35 | B1 | Feb 6, 2017 - Feb 10, 2017 | Cambridge, UK | Aug 7, 2016 (Jul 31, 2016) | 2017020620170210 | 20160807
WSE | IEEE International Symposium on Web Systems Evolution | 20 | B2 |  |  |  | z | z
WSKS | World Summit on the Knowledge Society | 10 | B4 |  |  |  | z | z
WSNA | ACM International Conference on Wireless Sensor Networks and Application | 29 | B1 |  |  |  | z | z
WSOM | Workshop on Self-Organizing Maps | 9 | B4 |  |  |  | z | z
WSPI | International Workshop on Philosophy and Informatics | 5 | B4 |  |  |  | z | z
WSWED | Brazilian Workshop on Semantic Web and Education | 1 | B5 |  |  |  | z | z
WTF | Workshop de Testes e Tolerância a Falhas | 4 | B4 |  |  |  | z | z
WTI | International Workshop on Web and Text Intelligence | 3 | B5 |  |  |  | z | z
WTS | Wireless Telecommunications Symposium  | 14 | B3 | Apr 26, 2017 - Apr 28, 2017 | Chicago | Jan 15, 2017 | 2017042620170428 | 20170115
WUWNET | International Conference on Underwater Networks | 16 | B2 |  |  |  | z | z
WVC | Workshop de Visão computacional | 3 | B5 |  |  |  | z | z
WWIC | Wired/Wireless Internet Communications | 15 | B3 | Jun 21, 2017 - Jun 23, 2017 | St. Petersburg, Russia | Mar 15, 2017 | 2017062120170623 | 20170315
WWW | International World Wide Web Conference | 145 | A1 | Apr 3, 2017 - Apr 7, 2017 | Perth, Australia | Oct 24, 2016 (Oct 19, 2016) | 2017040320170407 | 20161024
X-Meeting | International Conference of the Brazilian Association for Bioinformatics and Computational Biology | 8 | B4 | Jul 21, 2017 - Jul 23, 2017 | Guangzhou | Mar 20, 2017 (Jul 20, 2017) | 2017072120170723 | 20170320
XP | International Conference on Agile Processes in Software Engineering and eXtreme Programming | 20 | B2 | May 22, 2017 - May 26, 2017 | Cologne, Germany, Pullman Hotel | Dec 23, 2016 | 2017052220170526 | 20161223
XSym | International XML Database Symposium | 15 | B3 |  |  |  | z | z
