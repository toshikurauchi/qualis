#!/usr/bin/python
# -*- coding: UTF-8 -*-

import re
import datetime
from tqdm import tqdm

from carrega import *
from cfp import get_info
from cache import Cache


def gera_metadados(titulo):
    metadados = '''+++
date = "{data}"
draft = false

tags = ["misc", "qualis"]
title = "{titulo}"
math = true
summary = ""

highlight = true

+++
''' 
    data_str = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
    return metadados.format(data=data_str, titulo=titulo)
    

def gera_cabecalho(nomes):
	return ' | '.join(nomes) + '\n' + '|'.join(['---' for _ in nomes]) + '\n'
    

def gera_corpo(elementos, campos):
	return '\n'.join([' | '.join([elemento.get(campo, '') for campo in campos]) for elemento in elementos]) + '\n'


def gera_md(arquivo_markdown, titulo, texto, elementos, campos):
    f = open(arquivo_markdown, 'w')
    f.write(gera_metadados(titulo) + '\n\n' + texto + '\n\n' + gera_cabecalho(campos) + gera_corpo(elementos, campos))
    f.close()


def gera_md_periodico():
    texto = '''A tabela a seguir apresenta o Qualis de periódicos do quadriênio de 2013 a 2016. Os dados foram 
    obtidos na página da [Capes](https://sucupira.capes.gov.br/sucupira/public/consultas/coleta/veiculoPublicacaoQualis/listaConsultaGeralPeriodicos.jsf) no dia 
    26 de junho de 2017.'''
    gera_md('out/qualis-periodicos.md', 'Qualis Periódicos', texto, *carrega_periodicos())
    

def gera_md_conferencia():
    texto = '''A tabela a seguir apresenta o Qualis de conferências de 2012. Os dados foram 
    obtidos na página do [Instituto de Computação da Unicamp](http://ic.unicamp.br/pos/qualis). As datas e locais foram extraídos
    da página [WikiCFP](http://wikicfp.com/cfp/) [última atualização em %s].''' % datetime.datetime.now().strftime('%d-%m-%Y')
    conferencias, campos = carrega_conferencias()
    
    sigla = 'Sigla'
    data = 'Data'
    local = 'Local'
    deadline = 'Deadline'
    data_sort = 'data_sort'
    data_limite_sort = 'data_limite_sort'
    campos = (sigla, 'Nome', 'Índice-H', 'Estrato', data, local, deadline, data_sort, data_limite_sort)

    with Cache() as cache:
        for conferencia in tqdm(conferencias):
            s = conferencia[sigla]
            if s in cache:
                info = cache[s]
            else:
                info = get_info(s)
            conferencia[data], conferencia[local], conferencia[deadline], conferencia[data_sort], conferencia[data_limite_sort] = info
            cache[s] = info
    
    gera_md('out/qualis-conferencias.md', 'Qualis Conferências', texto, conferencias, campos)


if __name__=='__main__':
    gera_md_periodico()
    gera_md_conferencia()
