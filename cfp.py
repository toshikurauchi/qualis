import http.client
from html.parser import HTMLParser
from datetime import datetime


meses = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

# create a subclass and override the handler methods
class CfpHtmlParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.found_div = False
        self.count_table = 0
        self.count_tr = 0
        self.get_data = False
        self.do_when = True
        self.do_where = False
        self.do_deadline = False
        self.when = ''
        self.where = ''
        self.deadline = ''
        self.when_sort = 'z'
        self.deadline_sort = 'z'
        
    def handle_starttag(self, tag, attrs):
        if tag == "div":
            for attr in attrs:
                if(attr[0] == "class") and attr[1] == "contsec":
                    self.found_div = True
        elif self.found_div:
            if tag == "table" and self.count_table < 2:
                self.count_table += 1
            elif tag == "tr" and self.count_table == 2 and self.count_tr < 3:
                self.count_tr += 1
            elif tag == "td" and self.count_tr == 3:
                self.get_data = True
                
    def handle_endtag(self, tag):
        if (tag == "tr" and self.count_tr == 3) or (tag == "table" and self.count_table == 1) or (tag == "div" and self.found_div):
            self.found_div = False
            self.count_table = 0
            self.count_tr = 0
        if tag == "td":
            self.get_data = False
            
    def handle_data(self, data):
        if self.get_data:
            if self.do_when:
                self.do_when = False
                self.do_where = True
                self.when = data
                data_split = self.when.split(' ')
                if len(data_split) >= 7:
                    self.when_sort = "%04d%02d%02d%04d%02d%02d" % (int(data_split[2]), meses.index(data_split[0]), int(data_split[1][0:-1]), \
                                                                   int(data_split[6]), meses.index(data_split[4]), int(data_split[5][0:-1]))
            elif self.do_where:
                self.do_where = False
                self.do_deadline = True
                self.where = data
            elif self.do_deadline:
                self.do_deadline = False
                self.deadline = data
                data_split = self.deadline.split(' ')
                if len(data_split) >= 3:
                    self.deadline_sort = "%04d%02d%02d" % (int(data_split[2]), meses.index(data_split[0]), int(data_split[1][0:-1]))

def get_info(conferencia, ano = None):
    if ano is None:
        ano = datetime.now().year

    url = "www.wikicfp.com"
    query = "/cfp/servlet/tool.search?q={0}&year={1}".format(conferencia, ano)
    conn = http.client.HTTPConnection(url)
    conn.request("GET", query)
    res = conn.getresponse()
    if res.status == 200:
        data = res.read()
        parser = CfpHtmlParser()
        parser.feed(str(data, 'utf-8'))
        return parser.when, parser.where, parser.deadline, parser.when_sort, parser.deadline_sort
    return '', '', '', 'z', 'z'


if __name__ == '__main__':
    print('WHEN: %s\nWHERE: %s\nDEADLINE: %s\nWHEN_SORT: %s\nDEADLINE_SORT: %s' % get_info('CHI'))
